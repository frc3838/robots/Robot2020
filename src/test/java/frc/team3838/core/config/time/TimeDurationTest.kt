
package frc.team3838.core.config.time


import frc.team3838.core.config.time.TimeDuration.Companion.ofSum
import org.joda.time.DateTime
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.RepeatedTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertAll
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.EnumSource
import org.junit.jupiter.params.provider.MethodSource
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeUnit.*
import java.util.stream.Stream


internal class TimeDurationTest
{
    @ParameterizedTest
    @MethodSource("javaDurationAndTimeDuration")
    fun javaDurationToTimeDuration(expected: TimeDuration, duration: java.time.Duration) = assertEquals(expected, duration.toTimeDuration())

    @ParameterizedTest
    @MethodSource("javaDurationAndTimeDuration")
    fun asJavaDuration(timeDuration: TimeDuration, expected: java.time.Duration) = assertEquals(expected, timeDuration.asJavaDuration())


    @ParameterizedTest
    @MethodSource("jodaDurationAndTimeDuration")
    fun jodaDurationToTimeDuration(expected: TimeDuration, duration: org.joda.time.Duration) = assertEquals(expected, duration.toTimeDuration())

    @ParameterizedTest
    @MethodSource("jodaDurationAndTimeDuration")
    fun asJodaDuration(timeDuration: TimeDuration, expected: org.joda.time.Duration) = assertEquals(expected, timeDuration.asJodaDuration())


    @ParameterizedTest
    @MethodSource
    fun jodaIntervalToTimeDuration(expected: TimeDuration, interval: org.joda.time.Interval) = assertEquals(expected, interval.toTimeDuration())

    @ParameterizedTest
    @EnumSource(TimeUnit::class)
    fun timeUnitToTimeDuration(timeUnit: TimeUnit) = assertEquals(TimeDuration.of(5, timeUnit), timeUnit.toTimeDuration(5))


    @Test
    fun ofNanos() = assertEquals(TimeDuration.of(5, NANOSECONDS), TimeDuration.ofNanos(5))


    @Test
    fun ofMicro() = assertEquals(TimeDuration.of(5, MICROSECONDS), TimeDuration.ofMicros(5))


    @Test
    fun ofMillis() = assertEquals(TimeDuration.of(5, MILLISECONDS), TimeDuration.ofMillis(5))


    @Test
    fun ofSeconds() = assertEquals(TimeDuration.of(5, SECONDS), TimeDuration.ofSeconds(5))

    @Test
    fun ofMinutes() = assertEquals(TimeDuration.of(5, MINUTES), TimeDuration.ofMinutes(5))


    @Test
    fun ofHours() = assertEquals(TimeDuration.of(5, HOURS), TimeDuration.ofHours(5))


    @Test
    fun ofDays() = assertEquals(TimeDuration.of(5, DAYS), TimeDuration.ofDays(5))

    @ParameterizedTest
    @MethodSource("ofParts")
    @DisplayName("Test construction and conversion to coarsest time unit")
    fun construction(expectedDuration: Long, expectedTimeUnit: TimeUnit, inputDuration: Long, inputTimeUnit: TimeUnit)
    {
        val actual = TimeDuration.of(inputDuration, inputTimeUnit)
        assertAll("Test basic construction",
                  { assertEquals(expectedDuration, actual.duration, "Wrong Duration") },
                  { assertEquals(expectedTimeUnit, actual.timeUnit, "Wrong TimeUnit") }
                 )
    }


    @Test
    fun cloneTest()
    {
        val original = TimeDuration.of(100, MILLISECONDS)
        val clone = original.clone()
        assertAll(
                { assertEquals(original, clone, "Original and clone are not equal in value") },
                { assertNotSame(original, clone, "Original and clone are the same object and should not be") },
                { assertNotEquals(Integer.toHexString(System.identityHashCode(original)), Integer.toHexString(System.identityHashCode(clone)), "Original and clone have the same identity hashcode and they not ") }
                 )
    }


    @RepeatedTest(5)
    fun compareTo()
    {
        val expected = listOf(TimeDuration.of(1, NANOSECONDS),
                              TimeDuration.of(2, NANOSECONDS),
                              TimeDuration.of(3, NANOSECONDS),
                              TimeDuration.of(10, MICROSECONDS),
                              TimeDuration.of(20, MICROSECONDS),
                              TimeDuration.of(30, MICROSECONDS),
                              TimeDuration.of(100, MILLISECONDS),
                              TimeDuration.of(200, MILLISECONDS),
                              TimeDuration.of(300, MILLISECONDS),
                              TimeDuration.of(1, SECONDS),
                              TimeDuration.of(2, SECONDS),
                              TimeDuration.of(3, SECONDS),
                              TimeDuration.of(10, MINUTES),
                              TimeDuration.of(20, MINUTES),
                              TimeDuration.of(30, MINUTES),
                              TimeDuration.of(1, HOURS),
                              TimeDuration.of(2, HOURS),
                              TimeDuration.of(3, HOURS),
                              TimeDuration.of(24, HOURS),
                              TimeDuration.of(1, DAYS),
                              TimeDuration.of(2, DAYS),
                              TimeDuration.of(3, DAYS)
                             )
        val actual = expected.toMutableList()
        actual.shuffle()
        actual.sort()
        assertEquals(expected, actual, "List was not sorted properly using the compareTo method")
    }

    @Test
    fun comparisonOperators()
    {
        assertAll(
                { kotlin.test.assertTrue("2 Days > 1 Hour") { (TimeDuration.of(2, HOURS) > TimeDuration.of(1, HOURS)) } },
                { kotlin.test.assertTrue("1 Hour < 1 Day") { (TimeDuration.of(1, HOURS) < TimeDuration.of(1, DAYS)) } },
                { kotlin.test.assertTrue("1 Second > 999 Milliseconds") { (TimeDuration.of(1, SECONDS) > TimeDuration.of(999, MILLISECONDS)) } },
                { kotlin.test.assertTrue("999 Milliseconds < 1 Second ") { (TimeDuration.of(999, MILLISECONDS) < TimeDuration.of(1, SECONDS)) } },
                { kotlin.test.assertTrue("1 Second == 1000 Milliseconds") { (TimeDuration.of(1, SECONDS) == TimeDuration.of(1000, MILLISECONDS)) } }
                 )
    }


    @ParameterizedTest
    @MethodSource
    fun add(expected: TimeDuration, input: List<TimeDuration>)
    {
        assertAll({ assertEquals(expected, input[0].plus(input[1]), "Wrong sum for $input using plus method directly") },
                  { assertEquals(expected, (input[0] + input[1]), "Wrong sum for $input using '+' overload") })
    }


    @ParameterizedTest
    @MethodSource
    fun subtract(expected: TimeDuration, input: List<TimeDuration>)
    {
        assertAll({ assertEquals(expected, input[0].minus(input[1]), "Wrong difference for $input using minus method directly") },
                  { assertEquals(expected, (input[0] - input[1]), "Wrong difference for $input using '-' overload") })
    }


    @ParameterizedTest
    @MethodSource
    fun sumOfTest(expected: TimeDuration, input: List<TimeDuration>) = assertEquals(expected, ofSum(input))


    @ParameterizedTest
    @MethodSource
    fun stringParsing(expected: TimeDuration, text: String)
    {
        assertAll(
                { assertEquals(expected, TimeDuration.of(text), "Wrong result for original text of '$text'") },
                { assertEquals(expected, TimeDuration.of(text.toLowerCase()), "Wrong result for all lowercase text of '${text.toLowerCase()}'") },
                { assertEquals(expected, TimeDuration.of(text.toUpperCase()), "Wrong result for all uppercase text of '${text.toUpperCase()}'") }
                 )
    }

    @ParameterizedTest
    @MethodSource
    fun ofParts(expectedDuration: Long, expectedTimeUnit: TimeUnit, inputDuration: Long, inputTimeUnit: TimeUnit)
    {
        val actual = TimeDuration.of(inputDuration, inputTimeUnit)
        assertAll("Test of() factory with 1 set of args",
                  { assertEquals(expectedDuration, actual.duration, "Wrong Duration") },
                  { assertEquals(expectedTimeUnit, actual.timeUnit, "Wrong TimeUnit") },
                  { assertEquals(TimeDuration.of(expectedDuration, expectedTimeUnit), actual, "Wrong TimeDuration Object") }
                 )
    }


    @ParameterizedTest
    @MethodSource
    fun of2Groups(expected: TimeDuration, duration1: Long, timeUnit1: TimeUnit, duration2: Long, timeunit2: TimeUnit)
    {
        val actual = TimeDuration.of(duration1, timeUnit1, duration2, timeunit2)
        assertAll("Test of() factory with 2 set of args",
                  { assertEquals(expected.duration, actual.duration, "Wrong Duration") },
                  { assertEquals(expected.timeUnit, actual.timeUnit, "Wrong TimeUnit") },
                  { assertEquals(expected, actual, "Wrong TimeDuration Object") }
                 )
    }

    @ParameterizedTest
    @MethodSource
    fun of3Groups(expected: TimeDuration, duration1: Long, timeUnit1: TimeUnit, duration2: Long, timeunit2: TimeUnit, duration3: Long, timeunit3: TimeUnit)
    {
        val actual = TimeDuration.of(duration1, timeUnit1, duration2, timeunit2, duration3, timeunit3)
        assertAll("Test of() factory with 3 set of args",
                  { assertEquals(expected.duration, actual.duration, "Wrong Duration") },
                  { assertEquals(expected.timeUnit, actual.timeUnit, "Wrong TimeUnit") },
                  { assertEquals(expected, actual, "Wrong TimeDuration Object") }
                 )
    }

    @ParameterizedTest
    @MethodSource
    fun of4Groups(expected: TimeDuration, duration1: Long, timeUnit1: TimeUnit, duration2: Long, timeunit2: TimeUnit, duration3: Long, timeunit3: TimeUnit, duration4: Long, timeunit4: TimeUnit)
    {
        val actual = TimeDuration.of(duration1, timeUnit1, duration2, timeunit2, duration3, timeunit3, duration4, timeunit4)
        assertAll("Test of() factory with 4 set of args",
                  { assertEquals(expected.duration, actual.duration, "Wrong Duration") },
                  { assertEquals(expected.timeUnit, actual.timeUnit, "Wrong TimeUnit") },
                  { assertEquals(expected, actual, "Wrong TimeDuration Object") }
                 )
    }

    @Suppress("unused")
    companion object
    {
        @JvmStatic
        fun javaDurationAndTimeDuration(): Stream<Arguments>
        {
            return Stream.of(
                    Arguments.of(TimeDuration.of(5, NANOSECONDS), java.time.Duration.ofNanos(5)),
                    Arguments.of(TimeDuration.of(5, MICROSECONDS), java.time.Duration.ofNanos(5000)),
                    Arguments.of(TimeDuration.of(5, MILLISECONDS), java.time.Duration.ofMillis(5)),
                    Arguments.of(TimeDuration.of(5, SECONDS), java.time.Duration.ofSeconds(5)),
                    Arguments.of(TimeDuration.of(5, MINUTES), java.time.Duration.ofMinutes(5)),
                    Arguments.of(TimeDuration.of(5, HOURS), java.time.Duration.ofHours(5)),
                    Arguments.of(TimeDuration.of("1 Hour, 5 Minutes, 2 Seconds, and 300 milliseconds"), java.time.Duration.parse("P0DT1H5M2.3S"))
                            )
        }

        @JvmStatic
        fun jodaDurationAndTimeDuration(): Stream<Arguments>
        {
            return Stream.of(
                    Arguments.of(TimeDuration.of(5, MILLISECONDS), org.joda.time.Duration.millis(5)),
                    Arguments.of(TimeDuration.of(5, SECONDS), org.joda.time.Duration.standardSeconds(5)),
                    Arguments.of(TimeDuration.of(5, MINUTES), org.joda.time.Duration.standardMinutes(5)),
                    Arguments.of(TimeDuration.of(5, HOURS), org.joda.time.Duration.standardHours(5)),
                    Arguments.of(TimeDuration.of("1 Day, 2 Hours, and 5 minutes"),
                                 org.joda.time.Duration(DateTime(2000, 1, 1, 15, 0),
                                                        DateTime(2000, 1, 2, 17, 5)))
                            )
        }

        @JvmStatic
        fun sumOfTest(): Stream<Arguments>
        {
            return Stream.of(
                    Arguments.of(TimeDuration.of(2, DAYS), listOf(TimeDuration.of(1, DAYS), TimeDuration.of(24, HOURS))),
                    Arguments.of(TimeDuration.of(3, HOURS), listOf(TimeDuration.of(1, HOURS), TimeDuration.of(2, HOURS))),
                    Arguments.of(TimeDuration.of(3, HOURS), listOf(TimeDuration.of(1, HOURS), TimeDuration.of(120, MINUTES))),
                    Arguments.of(TimeDuration.of(1, DAYS), listOf(TimeDuration.of(20, HOURS), TimeDuration.of(4, HOURS))),
                    Arguments.of(TimeDuration.of(3, DAYS), listOf(TimeDuration.of(1, DAYS), TimeDuration.of(24, HOURS), TimeDuration.of(1, DAYS))),
                    Arguments.of(TimeDuration.of(3, HOURS), listOf(TimeDuration.of(1, HOURS), TimeDuration.of(59, MINUTES), TimeDuration.of(60, SECONDS), TimeDuration.of(30, MINUTES), TimeDuration.of(30, MINUTES)))
                            )
        }

        @JvmStatic
        fun stringParsing(): Stream<Arguments>
        {
            return Stream.of(
                    Arguments.of(TimeDuration.of(1, DAYS), "1 Day"),
                    Arguments.of(TimeDuration.of(2, DAYS), "2 Days"),
                    Arguments.of(TimeDuration.of(3, DAYS), "3  Day "),
                    Arguments.of(TimeDuration.of(4, DAYS), "  4  DayS"),
                    Arguments.of(TimeDuration.of(5, DAYS), "  5 Days "),
                    Arguments.of(TimeDuration.of(1, HOURS), "1 Hour "),
                    Arguments.of(TimeDuration.of(2, HOURS), "2 Hours"),
                    Arguments.of(TimeDuration.of(9, HOURS), "9 Hours and 0 Minutes"),
                    Arguments.of(TimeDuration.of(159, HOURS), "159 Hours"),
                    Arguments.of(TimeDuration.of(2, DAYS), "1 Day, and 24 Hours"),
                    Arguments.of(TimeDuration.of(73, HOURS), "2 Days 24 Hours 60 Minutes"),
                    Arguments.of(TimeDuration.of(73, HOURS), "2 Days, 24 Hours, 60 Minutes"),
                    Arguments.of(TimeDuration.of(73, HOURS), "2 Days, 24 Hours, and 60 Minutes"),
                    Arguments.of(TimeDuration.of(73, HOURS), "2 Days, 24 Hours, & 60 Minutes"),
                    Arguments.of(TimeDuration.of(73, HOURS), "2 Days and 24 Hours and 60 Minutes"),
                    Arguments.of(TimeDuration.of(73, HOURS), "2 Days, and 24 Hours, and 60 Minutes"),
                    Arguments.of(TimeDuration.of(73, HOURS), "2 Days & 24 Hours & 60 Minutes"),
                    Arguments.of(TimeDuration.of(73, HOURS), "2 Days + 24 Hours + 60 Minutes"),
                    Arguments.of(TimeDuration.of(150, SECONDS), " 2 Minutes & 30 seconds  "),
                    /* No Spaces*/
                    Arguments.of(TimeDuration.of(1, DAYS), "1DAY"),
                    Arguments.of(TimeDuration.of(1, DAYS), "1DAYS"),
                    Arguments.of(TimeDuration.of(2, DAYS), "2days"),
                    Arguments.of(TimeDuration.of(2, HOURS), "2Hours"),
                    Arguments.of(TimeDuration.of(2, HOURS), "2Hour"),
                    // Make sure we test all time units 
                    Arguments.of(TimeDuration.of(90061001001001, NANOSECONDS), "1 Day, 1 Hour, 1 Minute, 1 Second, 1 Millisecond, 1 Microsecond and 1 Nanosecond"),
                    Arguments.of(TimeDuration.of(180122002002002, NANOSECONDS), "2 Days, 2 Hours, 2 Minutes, 2 Seconds, 2 Milliseconds, 2 Microsecond and 2 Nanoseconds")
                            )
        }

        @JvmStatic
        fun ofParts(): Stream<Arguments>
        {
            // expectedDuration: Long, expectedTimeUnit: TimeUnit, inputDuration: Long, inputTimeUnit: TimeUnit
            return Stream.of(
                    Arguments.of(500, MILLISECONDS, 500, MILLISECONDS),
                    Arguments.of(4, MINUTES, 4, MINUTES),
                    Arguments.of(1, HOURS, 60, MINUTES),
                    Arguments.of(10, HOURS, 36_000_000, MILLISECONDS),
                    Arguments.of(2, DAYS, 48, HOURS),
                    Arguments.of(23, HOURS, 23, HOURS),
                    Arguments.of(1, DAYS, 24, HOURS),
                    Arguments.of(25, HOURS, 25, HOURS))
        }

        @JvmStatic
        fun of2Groups(): Stream<Arguments>
        {
            return Stream.of(
                    Arguments.of(TimeDuration.of(1, DAYS), 12, HOURS, 12, HOURS),
                    Arguments.of(TimeDuration.of(2, HOURS), 30, MINUTES, 90, MINUTES),
                    Arguments.of(TimeDuration.of(90, SECONDS), 1, MINUTES, 30, SECONDS),
                    Arguments.of(TimeDuration.of(90, SECONDS), 60, SECONDS, 30, SECONDS),
                    Arguments.of(TimeDuration.of(100, MILLISECONDS), 20, MILLISECONDS, 80, MILLISECONDS),
                    Arguments.of(TimeDuration.of(100, MILLISECONDS), 100, MILLISECONDS, 0, MILLISECONDS),
                    Arguments.of(TimeDuration.of(0, NANOSECONDS), 0, MINUTES, 0, MILLISECONDS))
        }

        @JvmStatic
        fun of3Groups(): Stream<Arguments>
        {
            return Stream.of(
                    Arguments.of(TimeDuration.of(2, DAYS), 12, HOURS, 12, HOURS, 24, HOURS),
                    Arguments.of(TimeDuration.of(2, HOURS), 30, MINUTES, 60, MINUTES, 30, MINUTES),
                    Arguments.of(TimeDuration.of(90, SECONDS), 1, MINUTES, 15, SECONDS, 15000, MILLISECONDS),
                    Arguments.of(TimeDuration.of(90, SECONDS), 35, SECONDS, 25, SECONDS, 30, SECONDS),
                    Arguments.of(TimeDuration.of(100, MILLISECONDS), 20, MILLISECONDS, 70, MILLISECONDS, 10, MILLISECONDS),
                    Arguments.of(TimeDuration.of(100, MILLISECONDS), 100, MILLISECONDS, 0, MILLISECONDS, 0, MILLISECONDS),
                    Arguments.of(TimeDuration.of(0, NANOSECONDS), 0, MINUTES, 0, MILLISECONDS, 0, DAYS))
        }

        @JvmStatic
        fun of4Groups(): Stream<Arguments>
        {
            return Stream.of(
                    Arguments.of(TimeDuration.of(3, DAYS), 12, HOURS, 12, HOURS, 24, HOURS, 1, DAYS),
                    Arguments.of(TimeDuration.of(2, HOURS), 30, MINUTES, 60, MINUTES, 15, MINUTES, 15, MINUTES),
                    Arguments.of(TimeDuration.of(90, SECONDS), 1, MINUTES, 10, SECONDS, 5, SECONDS, 15000, MILLISECONDS),
                    Arguments.of(TimeDuration.of(90, SECONDS), 35, SECONDS, 25, SECONDS, 10, SECONDS, 20, SECONDS),
                    Arguments.of(TimeDuration.of(100, MILLISECONDS), 20, MILLISECONDS, 70, MILLISECONDS, 9, MILLISECONDS, 1000, MICROSECONDS),
                    Arguments.of(TimeDuration.of(100, MILLISECONDS), 100, MILLISECONDS, 0, MILLISECONDS, 0, MILLISECONDS, 0, NANOSECONDS),
                    Arguments.of(TimeDuration.of(0, NANOSECONDS), 0, MINUTES, 0, MILLISECONDS, 0, DAYS, 0, DAYS))
        }

        @JvmStatic
        fun ofVarArg(): Stream<Arguments>
        {
            return Stream.of(
                    Arguments.of(TimeDuration.of(4, DAYS), 12, HOURS, 12, HOURS, 24, HOURS, 1, DAYS),
                    Arguments.of(TimeDuration.of(2, HOURS), 30, MINUTES, 60, MINUTES, 15, MINUTES, 15, MINUTES),
                    Arguments.of(TimeDuration.of(90, SECONDS), 1, MINUTES, 10, SECONDS, 5, SECONDS, 15000, MILLISECONDS),
                    Arguments.of(TimeDuration.of(90, SECONDS), 35, SECONDS, 25, SECONDS, 10, SECONDS, 20, SECONDS),
                    Arguments.of(TimeDuration.of(100, MILLISECONDS), 20, MILLISECONDS, 70, MILLISECONDS, 9, MILLISECONDS, 1000, MICROSECONDS),
                    Arguments.of(TimeDuration.of(100, MILLISECONDS), 100, MILLISECONDS, 0, MILLISECONDS, 0, MILLISECONDS, 0, NANOSECONDS),
                    Arguments.of(TimeDuration.of(0, NANOSECONDS), 0, MINUTES, 0, MILLISECONDS, 0, DAYS, 0, DAYS))
        }


        @JvmStatic
        fun add(): Stream<Arguments>
        {
            return Stream.of(
                    Arguments.of(TimeDuration.of(2, DAYS), listOf(TimeDuration.of(1, DAYS), TimeDuration.of(24, HOURS))),
                    Arguments.of(TimeDuration.of(3, HOURS), listOf(TimeDuration.of(1, HOURS), TimeDuration.of(2, HOURS))),
                    Arguments.of(TimeDuration.of(3, HOURS), listOf(TimeDuration.of(1, HOURS), TimeDuration.of(120, MINUTES))),
                    Arguments.of(TimeDuration.of(1, DAYS), listOf(TimeDuration.of(20, HOURS), TimeDuration.of(4, HOURS)))
                            )
        }

        @JvmStatic
        fun subtract(): Stream<Arguments>
        {
            return Stream.of(
                    Arguments.of(TimeDuration.of(2, DAYS), listOf(TimeDuration.of(3, DAYS), TimeDuration.of(24, HOURS))),
                    Arguments.of(TimeDuration.of(1, HOURS), listOf(TimeDuration.of(3, HOURS), TimeDuration.of(2, HOURS))),
                    Arguments.of(TimeDuration.of(1, HOURS), listOf(TimeDuration.of(3, HOURS), TimeDuration.of(120, MINUTES))),
                    Arguments.of(TimeDuration.of(20, HOURS), listOf(TimeDuration.of(1, DAYS), TimeDuration.of(4, HOURS)))
                            )
        }

        @JvmStatic
        fun jodaIntervalToTimeDuration(): Stream<Arguments>
        {
            return Stream.of(
                    Arguments.of(TimeDuration.of("1 Day, 2 Hours, and 5 minutes"),
                                 org.joda.time.Interval(DateTime(2000, 1, 1, 15, 0),
                                                        DateTime(2000, 1, 2, 17, 5))),
                    Arguments.of(TimeDuration.of(1, DAYS),
                                 org.joda.time.Interval(DateTime(2000, 1, 1, 15, 0),
                                                        DateTime(2000, 1, 2, 15, 0))),
                    Arguments.of(TimeDuration.of(10, MINUTES),
                                 org.joda.time.Interval(DateTime(2000, 1, 1, 15, 0),
                                                        DateTime(2000, 1, 1, 15, 10))))
        }

    }

}