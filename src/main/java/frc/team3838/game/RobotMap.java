package frc.team3838.game;

import java.util.concurrent.TimeUnit;
import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;
import com.google.common.collect.ImmutableMap;

import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.XboxController.Axis;
import edu.wpi.first.wpilibj.buttons.Button;
import edu.wpi.first.wpilibj.buttons.JoystickButton;
import edu.wpi.first.wpilibj.command.Command;
import frc.team3838.core.config.AbstractIOAssignments;
import frc.team3838.core.controls.AndButton;
import frc.team3838.core.controls.AxisAsButton;
import frc.team3838.core.controls.ButtonAction;
import frc.team3838.core.controls.GamePadFullRangeAxisFromThrottles;
import frc.team3838.core.controls.GamepadSide;
import frc.team3838.core.controls.HidBasedAxisPair;
import frc.team3838.core.controls.SimpleAxis;
import frc.team3838.core.dashboard.SortedSendableChooser;
import frc.team3838.core.dashboard.SortedSendableChooser.Order;
import frc.team3838.core.meta.API;
import frc.team3838.core.subsystems.I3838Subsystem;
import frc.team3838.core.subsystems.NavxSubsystem;
import frc.team3838.core.subsystems.Subsystems;
import frc.team3838.core.subsystems.drive.DriveControlMode;
import frc.team3838.game.commands.BallAcquisitionStartSequenceCommand;
import frc.team3838.game.commands.BallAcquisitionStopSequenceCommand;
import frc.team3838.game.commands.ElevatorLowerCommand;
import frc.team3838.game.commands.ElevatorRaiseCommand;
import frc.team3838.game.commands.GotToColorPositionControlCommand;
import frc.team3838.game.commands.IndexerStartCommand;
import frc.team3838.game.commands.IndexerStopCommand;
import frc.team3838.game.commands.ShooterStartHighSpeed;
import frc.team3838.game.commands.ShooterStartLowSpeed;
import frc.team3838.game.commands.ShooterStartMediumSpeed;
import frc.team3838.game.commands.Spin3TimesRotationControlCommand;
import frc.team3838.game.commands.StopShooterMotorsCommand;
import frc.team3838.game.commands.TransportReverseCommand;
import frc.team3838.game.commands.TransportStartCommand;
import frc.team3838.game.commands.TransportStopCommand;
import frc.team3838.game.commands.WheelSpinnerLowerCommand;
import frc.team3838.game.commands.WheelSpinnerRaiseCommand;
import frc.team3838.game.commands.WheelSpinnerStopMotorCommand;
import frc.team3838.game.commands.WinchClimbCommand;
import frc.team3838.game.commands.WinchStopCommand;
import frc.team3838.game.subsystems.BallSensorSubsystem;
import frc.team3838.game.subsystems.BallSubsystem;
import frc.team3838.game.subsystems.CompressorSubsystem;
import frc.team3838.game.subsystems.DriveTrainSubsystem;
import frc.team3838.game.subsystems.DriverControlConfig2020;
import frc.team3838.game.subsystems.ShooterSubsystem;
import frc.team3838.game.subsystems.UsbCameraSubsystem;
import frc.team3838.game.subsystems.WheelSpinnerSubsystem;
import frc.team3838.game.subsystems.WinchSubsystem;



/**
 * The RobotMap is a mapping from the ports sensors and actuators are wired into
 * to a variable name. This provides flexibility changing wiring, makes checking
 * the wiring easier and significantly reduces the number of magic numbers
 * floating around.
 */
public class RobotMap
{
    /**
     * Factor added to AIO & DIO port and PWM Channel numbers on the MXP (myRIO Expansion Port) 'More Board'.
     * Thus DIO port 2 on the MXP More board is DIO 12 (2 + ADD_FACTOR) to the roboRIO.
     * And  AIO port 5 on the MXP More board is DIO 15 (5 + ADD_FACTOR) to the roboRIO.
     * <br/>
     * See <a href='www.revrobotics.com/product/more-board/'>www.revrobotics.com/product/more-board/</a> <br/>
     * See <a href='http://wpilib.screenstepslive.com/s/4485/m/13809/l/145309-java-conventions-for-objects-methods-and-variables#MXPIONumbering'>http://wpilib.screenstepslive.com/s/4485/m/13809/l/145309-java-conventions-for-objects-methods-and-variables#MXPIONumbering</a><br/>
     */
    @SuppressWarnings("unused")
    public static final int MXP_ADD_FACTOR = 10;

    @SuppressWarnings("WeakerAccess")
    @Nonnull
    public static final ImmutableMap<Class<? extends I3838Subsystem>, Boolean> enabledSubsystemsMap = initSubsystemsMap();

    public static final boolean isDriveTrainEnabled = true;

    private static ImmutableMap<Class<? extends I3838Subsystem>, Boolean> initSubsystemsMap()
    {
        final ImmutableMap.Builder<Class<? extends I3838Subsystem>, Boolean> map = ImmutableMap.builder();

        map.put(DriveTrainSubsystem.class, isDriveTrainEnabled);
        map.put(NavxSubsystem.class, true);
        map.put(CompressorSubsystem.class, true);
        map.put(UsbCameraSubsystem.class, false);
    
        map.put(ShooterSubsystem.class, true);
        map.put(BallSubsystem.class, true);
        map.put(BallSensorSubsystem.class, true);
        map.put(WheelSpinnerSubsystem.class, true);
        map.put(WinchSubsystem.class, true);
        
        // Testing Subsystems
        //map.put(DashboardTrialSubsystem.class, false);
        return map.build();
    }
    
    
    @API
    public static class DIOs extends AbstractIOAssignments
    {
        // Keep assignments in numerical order please
        // NOTE: DUPLICATE ASSIGNMENTS WILL RESULT IN THE ROBOT FAILING TO STARTUP (i.e. ERROR'S OUT) 
        //       EVEN IF THE VARIABLE IS NOT USED ANYWHERE. ENSURE THERE ARE NO DUPLICATES :)
        //       See AbstractIOAssignments class level Javadoc for more details
    
        // TODO: Need to set/verify DriveTrain encoder assignments for 2020 robot
        public static final int DRIVE_TRAIN_LEFT_ENCODER_CH_A = 0;
        public static final int DRIVE_TRAIN_LEFT_ENCODER_CH_B = 1;
        public static final int DRIVE_TRAIN_RIGHT_ENCODER_CH_A = 2;
        public static final int DRIVE_TRAIN_RIGHT_ENCODER_CH_B = 3;

        public static final int SHOOTER_ANGLE_LIMIT_FLAG = 4;
        
        public static final int CLIMB_BAR_GRAB_INDICATOR = 5;
        
        /*
            == NAVX DIO ADDRESSING
            navX-MXP  MXP Pin   RoboRIO 
            Port      Number    Channel Address
            0         DIO0      10
            1         DIO1      11
            2         DIO2      12
            3         DIO3      13
            -         -         14-17 reserved for internal navX use
            4         DIO8      18
            5         DIO9      19
            6         DIO10     20
            7         DIO11     21
            8         DIO12     22
            9         DIO13     23   
         */
    
        public static final int BALL_INTAKE_COUNTER_0 = 10;
        public static final int BALL_INTAKE_COUNTER_1 = 11;
        public static final int BALL_INTAKE_COUNTER_2 = 12;
        public static final int BALL_INTAKE_COUNTER_3 = 13;
        
        @SuppressWarnings("unused")
        public static final int RESERVED_FOR_INTERNAL_USE_BY_THE_NAVx_BOARD_14 = 14;
        @SuppressWarnings("unused")
        public static final int RESERVED_FOR_INTERNAL_USE_BY_THE_NAVx_BOARD_15 = 15;
        @SuppressWarnings("unused")
        public static final int RESERVED_FOR_INTERNAL_USE_BY_THE_NAVx_BOARD_16 = 16;
        @SuppressWarnings("unused")
        public static final int RESERVED_FOR_INTERNAL_USE_BY_THE_NAVx_BOARD_17 = 17;

        // Keep assignments in numerical order please
    
        
        private DIOs() {/* Static use only. */}
    }
    
    @API
    public static class AIOs extends AbstractIOAssignments
    {
        // Keep assignments in numerical order please
        // NOTE: DUPLICATE ASSIGNMENTS WILL RESULT IN THE ROBOT FAILING TO STARTUP (i.e. ERROR'S OUT) 
        //       EVEN IF THE VARIABLE IS NOT USED ANYWHERE. ENSURE THERE ARE NO DUPLICATES :)
        //       See AbstractIOAssignments class level Javadoc for more details
    
    
        private AIOs() {/* Static use only. */}
    }

    @API
    public static class PWMs extends AbstractIOAssignments
    {
        // Keep assignments in numerical order please
        // NOTE: DUPLICATE ASSIGNMENTS WILL RESULT IN THE ROBOT FAILING TO STARTUP (i.e. ERROR'S OUT) 
        //       EVEN IF THE VARIABLE IS NOT USED ANYWHERE. ENSURE THERE ARE NO DUPLICATES :)
        //       See AbstractIOAssignments class level Javadoc for more details
    
        /*
            === PWM Output Addressing ===   
            navX-MXP        MXP Pin         RoboRIO
            Port            Number          Channel Address
            0               PWM0            10
            1               PWM1            11
            2               PWM2            12
            3               PWM3            13
            4               PWM4            14
            5               PWM5            15
            6               PWM6            16
            7               PWM7            17
            8               PWM8            18
            9               PWM9            19
         */
    
        private PWMs() {/* Static use only. */}
    }

    /**
     * Port assignments for the Pneumatic Control Module (PCM).
     * [Not to be confused with the PWMs (Pulse Width Modulation) channel assignment class.]
     */
    @API
    public static class PCM extends AbstractIOAssignments
    {
        // Keep assignments in numerical order please 
        // NOTE: DUPLICATE ASSIGNMENTS WILL RESULT IN THE ROBOT FAILING TO STARTUP (i.e. ERROR'S OUT) 
        //       EVEN IF THE VARIABLE IS NOT USED ANYWHERE. ENSURE THERE ARE NO DUPLICATES :)
        //       See AbstractIOAssignments class level Javadoc for more details
        
        public static final int BALL_ACQUISITION_SolenoidForwardChannel = 1;
        public static final int BALL_ACQUISITION_SolenoidReverseChannel = 0;        
        
        public static final int ELEVATOR_SolenoidForwardChannel = 2;
        public static final int ELEVATOR_SolenoidReverseChannel = 3;
    
        public static final int WOF_SolenoidForwardChannel = 4;
        public static final int WOF_SolenoidReverseChannel = 5;
    
    
        private PCM() {/* Static use only. */}
    }
    
    @API
    public static class Relays extends AbstractIOAssignments
    {
        // Keep assignments in numerical order please
    
        public static final int RING_LIGHT = 0;
    
        private Relays() {/* Static use only. */}
    }

    public static class CANs extends AbstractIOAssignments
    {
        // CAN IDs are assigned via the "Phoenix Tuner"
        // By default (out of the box) devices always have a device ID of 0 by default. 
        // So we keep that ID unused.

        // Keep assignments in numerical order please
        // NOTE: DUPLICATE ASSIGNMENTS WILL RESULT IN THE ROBOT FAILING TO STARTUP (i.e. ERROR'S OUT) 
        //       EVEN IF THE VARIABLE IS NOT USED ANYWHERE. ENSURE THERE ARE NO DUPLICATES :)
        //       See AbstractIOAssignments class level Javadoc for more details
        
        
        @SuppressWarnings("unused")
        public static final int DEFAULT_AND_THEREFORE_RESERVED_ID = 0;

        public static final int SHOOTER_UPPER_MOTOR = 1;    //TalonSRX 
        public static final int SHOOTER_LOWER_MOTOR = 2;    //TalonSRX 
        
        public static final int SHOOTER_ANGLE_MOTOR = 3;    //VictorSPX 
        public static final int BALL_INDEX_MOTOR = 4;   //VictorSPX
        
        // ALl Drivetrain motor controllers are VictorSPX
        public static final int DRIVE_RIGHT_SIDE_FRONT = 5;     // a.k.a. Passenger's side
        public static final int DRIVE_RIGHT_SIDE_REAR = 6;     
        public static final int DRIVE_LEFT_SIDE_FRONT = 7;      // a.k.a. Driver's side
        public static final int DRIVE_LEFT_SIDE_REAR = 8;      
        
        /**
         * The CAN Bus ID for the Power Distribution Panel (PDP). We usually do not need to access
         * this at all. But we do document its CAN ID here to prevent conflicts.
         */
        @SuppressWarnings("unused")
        public static final int PDP = 9;
    
        public static final int WINCH_MOTOR = 10; //TalonSRX 
        
        public static final int BALL_ACQUISITION_MOTOR = 11;    //VictorSPX
        public static final int BALL_TRANSPORT_MOTOR = 12;      //VictorSPX

        /** The CAN ID for the Pneumatic Control Module (i.e. the 'Compressor'). */
        public static final int PCM = 13;

        public static final int WHEEL_OF_FORTUNE_SPINNER_MOTOR = 14; //TalonSRX 
    
        private CANs() {/* Static use only. */}
    }


    
    
    
    
    
    
    // TODO Needs some cleanup and possible refactoring
    @API
    public static class UI
    {
        @SuppressWarnings("unused")
        private static final Logger logger = LoggerFactory.getLogger(UI.class);

        @API
        public static final ImmutableList<DriverControlConfig2020> configs;
        private static final SortedSendableChooser<DriverControlConfig2020> configChooser;
        private static final DriverControlConfig2020 joystickArcade;
        private static final SimpleAxis shooterAngleControl;
        private static final Button shooterAngleSafetyOverrideButton;
    
        public static DriverControlConfig2020 getDriveTrainControlConfiguration()
        {
            // If we ever change back to using the config chooser, we'll also need to uncomment the code that puts the chooser on the dashboard
            //return configChooser.getSelected();
            return joystickArcade;
        }

        public static SimpleAxis getShooterAngleControl()
        {
            return shooterAngleControl;
        }
    
    
        public static Button getShooterAngleSafetyOverrideButton() { return shooterAngleSafetyOverrideButton; }
    
    
        static
        {
            int loopCount = 0;
            while (!Subsystems.haveSubsystemsBeenInitialized() && (loopCount < 20))
            {
                loopCount++;
                try {TimeUnit.MILLISECONDS.sleep(100);} catch (InterruptedException ignore) {}
            }

            final Builder<DriverControlConfig2020> listBuilder = ImmutableList.builder();
            final Logger logger = LoggerFactory.getLogger(UI.class);

            logger.info("Creating Joystick DriveConfigurations");

            final Joystick primaryStick = new Joystick(0);
    
            final Button trigger = new JoystickButton(primaryStick, 1);
            trigger.whenPressed(new IndexerStartCommand());
            trigger.whenReleased(new IndexerStopCommand());
            
            final Button shooterOff = new JoystickButton(primaryStick, 2);
            shooterOff.whenPressed(new StopShooterMotorsCommand());

            final Button shooterOnLow = new JoystickButton(primaryStick, 4);
            shooterOnLow.whenPressed(new ShooterStartLowSpeed());

            final Button shooterOnMedium = new JoystickButton(primaryStick, 3);
            shooterOnMedium.whenPressed(new ShooterStartMediumSpeed());

            final Button shooterOnHigh = new JoystickButton(primaryStick, 5);
            shooterOnHigh.whenPressed(new ShooterStartHighSpeed());

            final Button reverseModeButton = new JoystickButton(primaryStick, 7);
            final Button fineControlButton = new JoystickButton(primaryStick, 6);
            //final Button reverseModeComboButton = new OrButton(reverseModeButtonA, reverseModeButtonB);
            
            final Button lowerBallArmAndRunMotors = new JoystickButton(primaryStick, 8);
            assignButton(lowerBallArmAndRunMotors, ButtonAction.Button_WhenPressed, new BallAcquisitionStartSequenceCommand());
            final Button raiseBallArmAndStopMotors = new JoystickButton(primaryStick, 9);
            assignButton(raiseBallArmAndStopMotors, ButtonAction.Button_WhenPressed, new BallAcquisitionStopSequenceCommand());
    
            final Button ballTransportButton = new JoystickButton(primaryStick, 10);
            assignButton(ballTransportButton, ButtonAction.Button_WhenPressed, new TransportStartCommand());
            assignButton(ballTransportButton, ButtonAction.Button_WhenReleased, new TransportStopCommand());
            
            // private static final DriverControlConfig2020 joystickArcade =
            joystickArcade = new DriverControlConfig2020("Joystick Arcade Control on port 0",
                                        DriveControlMode.Arcade,
                                        new HidBasedAxisPair(primaryStick),
                                        reverseModeButton,
                                                         fineControlButton
            );
            

            listBuilder.add(joystickArcade);

            configs = listBuilder.build();
            configChooser = new SortedSendableChooser<>(Order.Insertion);

            if (configs.isEmpty())
            {
                logger.error("No joystick or gamepad is connected. Cannot create a UI configuration.");
                final DriverControlConfig2020 noOpConfig = DriverControlConfig2020.getDriverControlConfigNoOpInstance();
                configChooser.setDefaultOption("No operation - no joystick/gamepad is connected.", noOpConfig);
            }
            else if (configs.size() == 1)
            {
                final DriverControlConfig2020 config = configs.get(0);
                configChooser.setDefaultOption(config.getConfigurationName(), config);
            }
            else
            {
                for (int i = 0; i < configs.size(); i++)
                {
                    final DriverControlConfig2020 config = configs.get(i);
                    final String prefix = "# " + (i + 1) + ") ";
                    if (i == 0)
                    {
                        configChooser.setDefaultOption(prefix + config.getConfigurationName(), config);
                    }
                    else
                    {
                        configChooser.addOption(prefix + config.getConfigurationName(), config);
                    }
                }
            }
    
            final XboxController coDriverGamePad = new XboxController(1);
            
            shooterAngleControl = new GamePadFullRangeAxisFromThrottles(coDriverGamePad, GamepadSide.Right);
            final JoystickButton leftBumperButton = new JoystickButton(coDriverGamePad, XboxController.Button.kBumperLeft.value);
            final JoystickButton rightBumperButton = new JoystickButton(coDriverGamePad, XboxController.Button.kBumperRight.value);
            shooterAngleSafetyOverrideButton = new AndButton(leftBumperButton, rightBumperButton);
            
            final JoystickButton bButton = new JoystickButton(coDriverGamePad, XboxController.Button.kB.value);
            assignButton(bButton, ButtonAction.Button_WhenPressed, new TransportStartCommand());
            assignButton(bButton, ButtonAction.Button_WhenReleased, new TransportStopCommand());
            
            final JoystickButton xButton = new JoystickButton(coDriverGamePad, XboxController.Button.kX.value);
            assignButton(xButton, ButtonAction.Button_WhenPressed, new TransportReverseCommand());
            assignButton(xButton, ButtonAction.Button_WhenReleased, new TransportStopCommand());
    
            final JoystickButton yButton = new JoystickButton(coDriverGamePad, XboxController.Button.kY.value);
            assignButton(yButton, ButtonAction.Button_WhenPressed, new ElevatorRaiseCommand());
            
            final JoystickButton aButton = new JoystickButton(coDriverGamePad, XboxController.Button.kA.value);
            assignButton(aButton, ButtonAction.Button_WhenPressed, new ElevatorLowerCommand());
            
            final JoystickButton startButton = new JoystickButton(coDriverGamePad, XboxController.Button.kStart.value);
            assignButton(startButton, ButtonAction.Button_WhenPressed, new WinchClimbCommand());
            assignButton(startButton, ButtonAction.Button_WhenReleased, new WinchStopCommand());
            
            final AxisAsButton spin3TimesButton = new AxisAsButton(coDriverGamePad, Axis.kLeftX, 0.2); // i.e. push right
            assignButton(spin3TimesButton, ButtonAction.Button_WhenPressed, new Spin3TimesRotationControlCommand());
    
            final AxisAsButton gotoColorButton = new AxisAsButton(coDriverGamePad, Axis.kLeftX, -0.2); // i.e. push left
            assignButton(gotoColorButton, ButtonAction.Button_WhenPressed, new GotToColorPositionControlCommand());
    
    
            final AxisAsButton raiseWheelArm = new AxisAsButton(coDriverGamePad, Axis.kLeftY, -0.2);
            assignButton(raiseWheelArm, ButtonAction.Button_WhenPressed, new WheelSpinnerRaiseCommand());
            
            final AxisAsButton lowerWheelArm = new AxisAsButton(coDriverGamePad, Axis.kLeftY, 0.2);
            assignButton(lowerWheelArm, ButtonAction.Button_WhenPressed, new WheelSpinnerLowerCommand());
    
            final JoystickButton stopWheelSpinner = new JoystickButton(coDriverGamePad, XboxController.Button.kStickLeft.value);
            assignButton(stopWheelSpinner, ButtonAction.Button_WhenPressed, new WheelSpinnerStopMotorCommand());
            /*
                ___                              _
               / __|__ _ _ __  ___ _ __  __ _ __| |
              | (_ / _` | '  \/ -_) '_ \/ _` / _` |
               \___\__,_|_|_|_\___| .__/\__,_\__,_|
                                  |_|
               */
            
            
            

            //SmartDashboard.putData("==UI Configuration Chooser==", configChooser);
        }

//        public static void previousStaticInitializer()
//        {
//
//            final Builder<DriverControlConfig2020> listBuilder = ImmutableList.builder();
//            final Logger logger = LoggerFactory.getLogger(UI.class);
//
//
//
//
//
//                /*
//                  ___                              _
//                 / __|__ _ _ __  ___ _ __  __ _ __| |
//                | (_ / _` | '  \/ -_) '_ \/ _` / _` |
//                 \___\__,_|_|_|_\___| .__/\__,_\__,_|
//                                    |_|
//                 */
//            final int gamePadReverseModeButton = 5;
//            final int gamePadFineControlButtonNum = 6;
//
////            if (!HidUtils.isHidConnected(0) || !HidUtils.isGamepad(0))
////            {
////                logger.info("No gamepad detected on port 0. Will not create gamepad drive configuration options");
////            }
////            else
//            {
//                logger.info("Creating Gamepad DriveConfigurations");
//
//
//
//                final XboxController gamepad = new XboxController(0);
//
//                final Button reverseModeTrigger = new JoystickButton(gamepad, gamePadReverseModeButton);
//
//                final Button fineControlTrigger = new JoystickButton(gamepad, gamePadFineControlButtonNum);
//                //final Trigger fineControlTrigger =  new MultiPovButton(gamepad, PovDirection.N_UP, PovDirection.NW, PovDirection.NE);
//
//                final Trigger allowReverseClimbTrigger = new NoOpTrigger();//new JoystickButton(gamepad, 7);
//
//
//
//
//                final DriverControlConfig2020 gamepadArcade = new DriverControlConfig2020("Gamepad Arcade Control",
//                                                                                          DriveControlMode.Arcade,
//                                                                                          new HidBasedAxisPair(gamepad, 4, 5),
//                                                                                          reverseModeTrigger,
//                                                                                          fineControlTrigger
//                );
//
//                listBuilder.add(gamepadArcade);
//
//
//                final DriverControlConfig2020 gamepadTank = new DriverControlConfig2020("Gamepad Tank Control",
//                                                                                        DriveControlMode.Tank,
//                                                                                        new GamepadTankAxisPair(gamepad),
//                                                                                        reverseModeTrigger,
//                                                                                        fineControlTrigger
//                );
//                listBuilder.add(gamepadTank);
//
//            }
//
//
//
//
//
//
//            /*
//                _             _   _    _
//             _ | |___ _  _ __| |_(_)__| |__ ___
//            | || / _ \ || (_-<  _| / _| / /(_-<
//             \__/\___/\_, /__/\__|_\__|_\_\/__/
//                      |__/
//             */
//
//            final int joystickReverseModeButtonNumber = 3;
//            final int joystickFineControlButtonNumber = 1;
//
//            final int joystickAllowReverseClimbNumberA = 8;
//            final int joystickAllowReverseClimbNumberB = 9;
//
////            if (!HidUtils.isHidConnected(1))
////            {
////                logger.info("No joystick detected on port 1. Will not create joystick drive configuration options");
////            }
////            else
//            {
//
//                logger.info("Creating Joystick DriveConfigurations");
//
//                final Joystick primaryStick = new Joystick(1);
//
//                final Button arcadeReverseModeTrigger = new JoystickButton(primaryStick, joystickReverseModeButtonNumber);
//                final Trigger arcadeFineControlTrigger = new JoystickButton(primaryStick, joystickFineControlButtonNumber);
//
//                final Trigger arcadeAllowReverseClimbTrigger = new DualComboButton(new JoystickButton(primaryStick, joystickAllowReverseClimbNumberA),
//                                                                                   new JoystickButton(primaryStick, joystickAllowReverseClimbNumberB));
//
//
//
//
//                final DriverControlConfig2020 joystickArcade =
//                    new DriverControlConfig2020("Joystick Arcade Control",
//                                                DriveControlMode.Arcade,
//                                                new HidBasedAxisPair(primaryStick),
//                                                arcadeReverseModeTrigger,
//                                                arcadeFineControlTrigger
//                    );
//                listBuilder.add(joystickArcade);
//
//
//            }
//
//            // HAD TO COMMENT OUT IN THIS PLACEHOLDER METHOD SINCE WE CANNOT ASSIGN TO A FINAL HERE
////            configs = listBuilder.build();
////            configChooser = new SortedSendableChooser<>(Order.Insertion);
//
//
//            if (configs.isEmpty())
//            {
//                logger.error("No joystick or gamepad is connected. Cannot create a UI configuration.");
//                final DriverControlConfig2020 noOpConfig = DriverControlConfig2020.getDriverControlConfig2017NoOpInstance();
//                configChooser.setDefaultOption("No operation - no joystick/gamepad is connected.", noOpConfig);
//            }
//            else
//            {
//                for (int i = 0; i < configs.size(); i++)
//                {
//                    final DriverControlConfig2020 config = configs.get(i);
//                    final String prefix = "# " + (i + 1) + ") ";
//                    if (i == 0)
//                    {
//                        configChooser.setDefaultOption(prefix + config.getConfigurationName(), config);
//                    }
//                    else
//                    {
//                        configChooser.addOption(prefix + config.getConfigurationName(), config);
//                    }
//                }
//            }
//
//
//
//
//            SmartDashboard.putData("==UI Configuration Chooser==", configChooser);
//
//        }


        /*
            NOTE: Look into the newer Button API:
            
            final Trigger intakeTrigger = new JoystickButton(m_operatorController, XboxController.Button.kBumperRight.value).or(new JoystickButton(m_driverController, XboxController.Button.kBumperRight.value));
            intakeTrigger
                .whenActive(new InstantCommand(() -> m_intake.toggleIntakePosition(true))
                .andThen(new WaitUntilCommand(() -> !intakeTrigger.get())
                .andThen(new InstantCommand(() -> m_intake.toggleIntakeWheels(true))))
                .withTimeout(0.5));                
         */
        
        
        @API
        public static Button createAndAssignButton(final GenericHID hid,
                                                   final int buttonNumber,
                                                   final ButtonAction action,
                                                   final Command command) throws IllegalArgumentException
        {
            final JoystickButton button = new JoystickButton(hid, buttonNumber);
            return assignButton(button, action, command);
        }


        @API
        public static Button assignButton(Button button, ButtonAction action, Command command)
        {
            action.assign(button, command);
            return button;
        }
    
    
        private UI() {/* Static use only. */}
    }
}
