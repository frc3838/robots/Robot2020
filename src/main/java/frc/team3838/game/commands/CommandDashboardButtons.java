package frc.team3838.game.commands;

import java.util.Map;
import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.wpi.first.wpilibj.shuffleboard.BuiltInLayouts;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardLayout;
import frc.team3838.core.dashboard.DashboardManager;
import frc.team3838.game.subsystems.BallSubsystem;
import frc.team3838.game.subsystems.ShooterSubsystem;



public class CommandDashboardButtons
{
    private static final Logger logger = LoggerFactory.getLogger(CommandDashboardButtons.class);

    /*
        EXAMPLE FROM https://wpilib.screenstepslive.com/s/currentCS/m/shuffleboard/l/1021980-organizing-widgets
        ShuffleboardLayout elevatorCommands = Shuffleboard.getTab("Commands")
          .getLayout("Elevator", BuiltInLayouts.kList)
          .withSize(2, 2)
          .withProperties(Map.of("Label position", "HIDDEN")); // hide labels for commands
        elevatorCommands.add(new ElevatorDownCommand());
        elevatorCommands.add(new ElevatorUpCommand());

     */


    public static void addAllCommands()
    {
        CompressorCommandsKt.addAllCompressorCommandsToDashboard();
        WinchCommandsKt.addAllWinchCommandsToDashboard();
        BallSubsystem.INSTANCE.addAllBallHandlerCommands();
        ShooterSubsystem.INSTANCE.addAllShooterCommands();
        WheelSpinnerCommandsKt.putAllSpinnerCommandsOnDashboard();
    }
    
    


    @Nonnull
    private static ShuffleboardLayout createLayout(String title, int width, int height)
    {
        return DashboardManager.getCommandsTab().getLayout(title, BuiltInLayouts.kList)
                               .withSize(width, height)
                               .withProperties(Map.of("Label position", "HIDDEN"));  // hide labels for commands
    }
}
