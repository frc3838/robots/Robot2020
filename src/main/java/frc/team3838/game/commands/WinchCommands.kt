package frc.team3838.game.commands

import com.google.common.collect.ImmutableSet
import frc.team3838.core.commands.AbstractInstant3838Command
import frc.team3838.core.dashboard.commandsTab
import frc.team3838.core.subsystems.I3838Subsystem
import frc.team3838.core.utils.NO_OP
import frc.team3838.game.subsystems.WinchSubsystem


abstract class AbstractWinchCommand : AbstractInstant3838Command()
{
    override fun getRequiredSubsystems(): ImmutableSet<I3838Subsystem> = ImmutableSet.of(WinchSubsystem)
    override fun initializeImpl() = NO_OP()
}


class WinchClimbCommand: AbstractWinchCommand()
{
    override fun executeImpl()
    {
        WinchSubsystem.turnOnWinchMotorForClimb()
    }
}

class WinchLowerCommand: AbstractWinchCommand()
{
    override fun executeImpl() = WinchSubsystem.turnOnWinchMotorForDecent()
}

class WinchStopCommand: AbstractWinchCommand()
{
    override fun executeImpl() = WinchSubsystem.stopWinchMotor()
}

fun addAllWinchCommandsToDashboard()
{
    commandsTab.add(WinchClimbCommand())
    commandsTab.add(WinchLowerCommand())
    commandsTab.add(WinchStopCommand())
    
}