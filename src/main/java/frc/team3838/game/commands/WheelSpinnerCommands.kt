package frc.team3838.game.commands

import com.google.common.collect.ImmutableSet
import frc.team3838.core.commands.Abstract3838Command
import frc.team3838.core.commands.AbstractInstant3838Command
import frc.team3838.core.config.time.PartialSeconds
import frc.team3838.core.config.time.TimeDuration
import frc.team3838.core.logging.LaconicLogger
import frc.team3838.core.subsystems.I3838Subsystem
import frc.team3838.core.utils.NO_OP
import frc.team3838.game.subsystems.WheelColor
import frc.team3838.game.subsystems.WheelDirection
import frc.team3838.game.subsystems.WheelSpinnerSubsystem
import java.util.concurrent.TimeUnit


abstract class AbstractWheelSpinnerCommand : AbstractInstant3838Command()
{
    override fun getRequiredSubsystems(): ImmutableSet<I3838Subsystem> = ImmutableSet.of(WheelSpinnerSubsystem)
    override fun initializeImpl() = NO_OP()
}

class WheelSpinnerStartClockwiseCommand() : AbstractWheelSpinnerCommand()
{
    override fun executeImpl()
    {
        WheelSpinnerSubsystem.startSpinnerMotorClockWise()
    }
}

class WheelSpinnerStartCounterClockwiseCommand() : AbstractWheelSpinnerCommand()
{
    override fun executeImpl()
    {
        WheelSpinnerSubsystem.startSpinnerMotorCounterClockWise()
    }
}

class WheelSpinnerStopMotorCommand() : AbstractWheelSpinnerCommand()
{
    override fun executeImpl()
    {
        WheelSpinnerSubsystem.stopSpinnerMotor()
    }
}

class GotToColorPositionControlCommand(): Abstract3838Command(name = "Goto Color")
{
    private var startingColor: WheelColor? = null
    private var currentRobotColor: WheelColor? = null
    private var fieldTargetColor: WheelColor? = null
    private var robotTargetColor: WheelColor? = null
    private var preRobotTargetColor: WheelColor? = null
    private var direction: WheelDirection? = null
    private var isFullyInitialized = false
    private var allDone = false

    private val interval = TimeDuration.ofSeconds(PartialSeconds.HalfSecond)
    private var initLaconicLogger = LaconicLogger(logger, interval)
    private var laconicLogger1 = LaconicLogger(logger, interval)
    private var laconicLogger2 = LaconicLogger(logger, interval)
    private var laconicLogger3 = LaconicLogger(logger, interval)
    private var laconicLogger4 = LaconicLogger(logger, interval)
    private var laconicLogger5 = LaconicLogger(logger, interval)
    private var laconicLogger6 = LaconicLogger(logger, interval)
    private var laconicLogger7 = LaconicLogger(logger, interval)
    
    override fun getRequiredSubsystems(): ImmutableSet<I3838Subsystem> = ImmutableSet.of(WheelSpinnerSubsystem)
    
    override fun initializeImpl()
    {
        startingColor = null
        currentRobotColor  = null
        fieldTargetColor = null
        robotTargetColor = null
        preRobotTargetColor = null
        direction = null
        isFullyInitialized = false
        allDone = false
        initVariablesAsNeeded()

        WheelSpinnerSubsystem.startSpinnerMotor(direction ?: WheelDirection.Clockwise , WheelSpinnerSubsystem.MOD_SPEED)
        
    }

    private fun initVariablesAsNeeded()
    {
        logger.debug() { "Pre  Init:: fieldTargetColor: $fieldTargetColor  robotTargetColor: $robotTargetColor  startingColor: $startingColor  direction: $direction  isFullyInitialized: $isFullyInitialized" }
        val wheelReadingResult = WheelSpinnerSubsystem.readWheelsCurrentColor()
        currentRobotColor = wheelReadingResult.foundWheelColor
        if (startingColor == null) startingColor = wheelReadingResult.foundWheelColor
        
        if (fieldTargetColor == null)
        {
            fieldTargetColor = WheelSpinnerSubsystem.readTargetColorFromFMS()
            if (fieldTargetColor != null)
            {
                robotTargetColor = fieldTargetColor!!.getCorrespondingTargetColor()
                direction =
                        if (startingColor != null && robotTargetColor != null)
                            startingColor!!.getDirectionTo(robotTargetColor!!)
                        else
                            WheelDirection.Clockwise

                preRobotTargetColor = robotTargetColor!!.getNeighbor(direction!!.getOpposite())
            }
            else
            {
                robotTargetColor = null
            }
        }

        if (direction == null) direction = WheelDirection.Clockwise
        
        isFullyInitialized = startingColor != null && fieldTargetColor != null && robotTargetColor != null && direction != null

        logger.debug() { "Post Init:: fieldTargetColor: $fieldTargetColor  robotTargetColor: $robotTargetColor  startingColor: $startingColor  direction: $direction  isFullyInitialized: $isFullyInitialized" }
    }

    override fun executeImpl()
    {
        if (!isFullyInitialized) initVariablesAsNeeded()
        currentRobotColor = WheelSpinnerSubsystem.readWheelsCurrentColor().foundWheelColor
        laconicLogger1.debug() { "currentRobotColor: $currentRobotColor"}
        
        if (currentRobotColor == robotTargetColor)
        {
            laconicLogger2.debug() { "ON TARGET:  currentRobotColor: $currentRobotColor  robotTargetColor: $robotTargetColor" }
            allDone = true
            WheelSpinnerSubsystem.startSpinnerMotor(direction ?: WheelDirection.Clockwise, WheelSpinnerSubsystem.MIN_SPEED)
            try { TimeUnit.MILLISECONDS.sleep(40); } catch (ignore: InterruptedException) { }
            WheelSpinnerSubsystem.stopSpinnerMotor()

            WheelSpinnerSubsystem.startSpinnerMotor(direction?.getOpposite() ?: WheelDirection.CounterClockwise, WheelSpinnerSubsystem.MIN_SPEED)
            try { TimeUnit.MILLISECONDS.sleep(10); } catch (ignore: InterruptedException) { }
            WheelSpinnerSubsystem.stopSpinnerMotor()
            laconicLogger3.debug() { "DONE" }
        }
        else if (currentRobotColor == preRobotTargetColor)
        {
            WheelSpinnerSubsystem.startSpinnerMotor(direction ?: WheelDirection.Clockwise, WheelSpinnerSubsystem.MIN_SPEED)
            laconicLogger3.debug() { "At Pre-color currentRobotColor: $currentRobotColor  preRobotTargetColor: $preRobotTargetColor  robotTargetColor: $robotTargetColor" }
        }
        else
        {
            WheelSpinnerSubsystem.startSpinnerMotor(direction ?: WheelDirection.Clockwise, WheelSpinnerSubsystem.MOD_SPEED)
            laconicLogger4.debug() { "Regular Speed for: $currentRobotColor  preRobotTargetColor: $preRobotTargetColor  robotTargetColor: $robotTargetColor" }
        }
    }

    override fun isFinishedImpl(): Boolean = allDone

    override fun endImpl()
    {
        WheelSpinnerSubsystem.stopSpinnerMotor()
    }
}

class Spin3TimesRotationControlCommand() : Abstract3838Command(name = "Spin 3X")
{
    private val DIRECTION = WheelDirection.Clockwise
    private val OPPOSITE_DIRECTION = WheelDirection.CounterClockwise
    private var startingColor: WheelColor? = null
    private var endColor: WheelColor? = null
    private var isFullyInitialized = false

    private var previousColor: WheelColor? = null
    private var currentRobotColor: WheelColor? = null
    private var startingColorPasses = 0
    private var allDone = false
    
    private val requiredStartingColorPasses = 7

    private val interval = TimeDuration.ofSeconds(PartialSeconds.HalfSecond)
    private var initLaconicLogger = LaconicLogger(logger, interval)
    private var laconicLogger1 = LaconicLogger(logger, interval)
    private var laconicLogger2 = LaconicLogger(logger, interval)
    private var laconicLogger3 = LaconicLogger(logger, interval)
    private var laconicLogger4 = LaconicLogger(logger, interval)
    private var laconicLogger5 = LaconicLogger(logger, interval)
    private var laconicLogger6 = LaconicLogger(logger, interval)
    private var laconicLogger7 = LaconicLogger(logger, interval)
    
    

    override fun getRequiredSubsystems(): ImmutableSet<I3838Subsystem> = ImmutableSet.of(WheelSpinnerSubsystem)

    override fun initializeImpl()
    {
        startingColor = null
        endColor = null
        isFullyInitialized = false
        allDone = false
        
        previousColor = null
        currentRobotColor = null
        startingColorPasses = 0

        
        initVariablesAsNeeded()

    }

    private fun initVariablesAsNeeded()
    {
        if (startingColor == null)
        {
            val wheelReadingResult = WheelSpinnerSubsystem.readWheelsCurrentColor()
            startingColor = wheelReadingResult.foundWheelColor
            
        }
        
        if (endColor == null && startingColor != null)
        {
            endColor = startingColor!!.getNeighbor(DIRECTION).getNeighbor(DIRECTION)
        }
        
        if (previousColor == null) previousColor = startingColor
        
        isFullyInitialized = startingColor != null  && endColor != null

        initLaconicLogger.debug() { "startingColor: $startingColor  endColor: $endColor  previousColor: $previousColor  requiredStartingColorPasses: $requiredStartingColorPasses  isFullyInitialized: $isFullyInitialized" }
    }

    override fun executeImpl()
    {
        // We have to call this in execute to deal with the scenario of the current color not being readable
        // because we are on a seam, or the low chance the field system has not reported the target color yet
        if (!isFullyInitialized) initVariablesAsNeeded()


        currentRobotColor = WheelSpinnerSubsystem.readWheelsCurrentColor().foundWheelColor
        laconicLogger1.debug() {"currentRobotColor: $currentRobotColor"}
        
        if (currentRobotColor != null)
        {
            if (previousColor != currentRobotColor)
            {
                // the color has changed
                previousColor = currentRobotColor
                if (currentRobotColor == startingColor) startingColorPasses++
                laconicLogger2.debug() { "Color has changed: startingColorPasses: $startingColorPasses" }
            }
        }

        var speed: Double = WheelSpinnerSubsystem.MAX_SPEED
        
        
        if (startingColorPasses >= requiredStartingColorPasses)
        {
            // We've done our total passes, but we want to go past the starting color
            if (currentRobotColor == endColor)
            {
                allDone = true
                WheelSpinnerSubsystem.startSpinnerMotor(DIRECTION, WheelSpinnerSubsystem.MIN_SPEED)
                try { TimeUnit.MILLISECONDS.sleep(50); } catch (ignore: InterruptedException) { }
                WheelSpinnerSubsystem.stopSpinnerMotor()
                laconicLogger3.debug() { "ALL DONE. currentRobotColor: $currentRobotColor requiredStartingColorPasses: $requiredStartingColorPasses  startingColor: $startingColor  endColor: $endColor" }
            }
            else
            {
                laconicLogger4.debug(){ "passing starting color, waiting to get to next color, i.e. the end color.  currentRobotColor: $currentRobotColor requiredStartingColorPasses: $requiredStartingColorPasses  startingColor: $startingColor  endColor: $endColor"}
            }
            
        }
        else if (startingColorPasses >= (requiredStartingColorPasses - 1))
        {
            // Getting close, let's slow down
            WheelSpinnerSubsystem.startSpinnerMotor(DIRECTION, WheelSpinnerSubsystem.MOD_SPEED)
            laconicLogger5.debug() { "Getting close requiredStartingColorPasses: $requiredStartingColorPasses" }
        }
        else
        {
            // Not close yet, let's keep spinning baby
            WheelSpinnerSubsystem.startSpinnerMotor(DIRECTION, WheelSpinnerSubsystem.MAX_SPEED)
            laconicLogger6.debug() { "Still Spinning requiredStartingColorPasses: $requiredStartingColorPasses" }
        }
    }


    override fun isFinishedImpl(): Boolean = allDone
   

    override fun endImpl()
    {
        WheelSpinnerSubsystem.stopSpinnerMotor()
    }
}

fun putAllSpinnerCommandsOnDashboard()
{
    WheelSpinnerSubsystem.tab.add(WheelSpinnerStartClockwiseCommand())
    WheelSpinnerSubsystem.tab.add(WheelSpinnerStartCounterClockwiseCommand())
    WheelSpinnerSubsystem.tab.add(WheelSpinnerStopMotorCommand())
    WheelSpinnerSubsystem.tab.add(Spin3TimesRotationControlCommand())
    WheelSpinnerSubsystem.tab.add(GotToColorPositionControlCommand())
}

