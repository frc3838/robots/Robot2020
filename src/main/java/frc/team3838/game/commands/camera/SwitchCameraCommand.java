package frc.team3838.game.commands.camera;

import edu.wpi.first.wpilibj.buttons.Trigger;
import frc.team3838.core.commands.camera.AbstractSwitchCameraCommand;
import frc.team3838.core.controls.NoOpTrigger;
import frc.team3838.core.subsystems.Abstract3838TwoUsbCameraSubsystem;
import frc.team3838.game.RobotMap.UI;
import frc.team3838.game.subsystems.UsbCameraSubsystem;



public class SwitchCameraCommand extends AbstractSwitchCameraCommand
{
    private final Trigger trigger;


    public SwitchCameraCommand()
    {
        Trigger theTrigger = UI.getDriveTrainControlConfiguration().getReverseModeTrigger();
        if (theTrigger == null)
        {
            getLogger().warn("Trigger for camera switching was null in DriveTrainControlConfiguration. No trigger will be configured (i.e. No op) and thus camera switching will be disabled.");
        }
        trigger = (theTrigger != null) ? theTrigger : new NoOpTrigger();
    }


    @Override
    protected Abstract3838TwoUsbCameraSubsystem getCameraSubsystem()
    {
        return UsbCameraSubsystem.getInstance();
    }


    @Override
    protected Trigger getTrigger()
    {
        return trigger;
    }


    @Override
    protected boolean getInvert()
    {
       return false;
    }

}
