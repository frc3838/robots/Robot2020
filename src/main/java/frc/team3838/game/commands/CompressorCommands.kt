package frc.team3838.game.commands

import com.google.common.collect.ImmutableSet
import frc.team3838.core.commands.AbstractInstant3838Command
import frc.team3838.core.dashboard.commandsTab
import frc.team3838.core.subsystems.I3838Subsystem
import frc.team3838.core.utils.NO_OP
import frc.team3838.game.subsystems.CompressorSubsystem
import frc.team3838.game.subsystems.WheelSpinnerSubsystem


abstract class AbstractCompressorCommand : AbstractInstant3838Command()
{
    override fun getRequiredSubsystems(): ImmutableSet<I3838Subsystem> = ImmutableSet.of(CompressorSubsystem.getInstance())
    override fun initializeImpl() = NO_OP()
}

abstract class AbstractSpinnerAndCompressorCommand : AbstractInstant3838Command()
{
    override fun getRequiredSubsystems(): ImmutableSet<I3838Subsystem> = ImmutableSet.of(CompressorSubsystem.getInstance(), WheelSpinnerSubsystem)
    override fun initializeImpl() = NO_OP()
}

class BallAcquisitionArmLowerCommand : AbstractCompressorCommand()
{
    override fun executeImpl() = CompressorSubsystem.getInstance().lowerBallAcquisitionArm()
}

class BallAcquisitionArmRaiseCommand : AbstractCompressorCommand()
{
    override fun executeImpl() = CompressorSubsystem.getInstance().raiseBallAcquisitionArm()
}

class BallAcquisitionArmStopCommand : AbstractCompressorCommand()
{
    override fun executeImpl() = CompressorSubsystem.getInstance().ballAcquisitionArmOff()
}

class ElevatorLowerCommand : AbstractCompressorCommand()
{
    override fun executeImpl() = CompressorSubsystem.getInstance().lowerElevator()
}

class ElevatorRaiseCommand : AbstractCompressorCommand()
{
    override fun executeImpl() = CompressorSubsystem.getInstance().raiseElevator()
}

class ElevatorStopCommand : AbstractCompressorCommand()
{
    override fun executeImpl() = CompressorSubsystem.getInstance().elevatorOff()
}

class WheelSpinnerLowerCommand : AbstractSpinnerAndCompressorCommand()
{
    override fun executeImpl()
    {
        CompressorSubsystem.getInstance().lowerWheelSpinner()
        WheelSpinnerSubsystem.stopSpinnerMotor()
    }
}

class WheelSpinnerRaiseCommand : AbstractSpinnerAndCompressorCommand()
{
    override fun executeImpl()
    {
        CompressorSubsystem.getInstance().raiseWheelSpinner()
        WheelSpinnerSubsystem.stopSpinnerMotor()
    }
}

class WheelSpinnerArmUpAndDownStopCommand : AbstractCompressorCommand()
{
    override fun executeImpl() = CompressorSubsystem.getInstance().wheelSpinnerOff()
}

fun addAllCompressorCommandsToDashboard()
{
    commandsTab.add(ElevatorLowerCommand())
    commandsTab.add(ElevatorRaiseCommand())
    commandsTab.add(ElevatorStopCommand())
    commandsTab.add(WheelSpinnerLowerCommand())
    commandsTab.add(WheelSpinnerRaiseCommand())
    commandsTab.add(WheelSpinnerArmUpAndDownStopCommand())
    commandsTab.add(BallAcquisitionArmLowerCommand())
    commandsTab.add(BallAcquisitionArmRaiseCommand())
    commandsTab.add(BallAcquisitionArmStopCommand())
}