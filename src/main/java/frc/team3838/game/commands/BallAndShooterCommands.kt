package frc.team3838.game.commands

import com.google.common.collect.ImmutableSet
import frc.team3838.core.commands.AbstractInstant3838Command
import frc.team3838.core.subsystems.I3838Subsystem
import frc.team3838.core.utils.NO_OP
import frc.team3838.game.subsystems.BallSubsystem
import frc.team3838.game.subsystems.CompressorSubsystem
import frc.team3838.game.subsystems.ShooterSubsystem


abstract class AbstractShooterInstantCommand : AbstractInstant3838Command()
{
    override fun getRequiredSubsystems(): ImmutableSet<I3838Subsystem> = ImmutableSet.of(ShooterSubsystem)
    override fun initializeImpl() = NO_OP()
}

abstract class AbstractBallInstantCommand : AbstractInstant3838Command()
{
    override fun getRequiredSubsystems(): ImmutableSet<I3838Subsystem> = ImmutableSet.of(BallSubsystem)
    override fun initializeImpl() = NO_OP()
}

abstract class AbstractAcquisitionArmInstantCommand : AbstractInstant3838Command()
{
    override fun getRequiredSubsystems(): ImmutableSet<I3838Subsystem> = ImmutableSet.of(BallSubsystem, CompressorSubsystem.getInstance())
    override fun initializeImpl() = NO_OP()
}



class StopShooterMotorsCommand: AbstractShooterInstantCommand()
{
    override fun executeImpl() = ShooterSubsystem.stopMotors()
}

class ShooterCallSetMotorSpeedCommand: AbstractShooterInstantCommand()
{
    override fun executeImpl() = ShooterSubsystem.setMotorSpeed()
}

class ShooterStartHighSpeed(): AbstractShooterInstantCommand()
{
    override fun executeImpl() = ShooterSubsystem.startShooter(6000)
}

class ShooterStartMediumSpeed(): AbstractShooterInstantCommand()
{
    override fun executeImpl() = ShooterSubsystem.startShooter(4000)
}

class ShooterStartLowSpeed(): AbstractShooterInstantCommand()
{
    override fun executeImpl() = ShooterSubsystem.startShooter(2000)
}

class RingLightToggleCommand(): AbstractShooterInstantCommand()
{
    override fun executeImpl() = ShooterSubsystem.toggleRingLight()
}

class IndexerStartCommand(): AbstractBallInstantCommand()
{
    override fun executeImpl()
    {
        BallSubsystem.startIndexerMotor()
    }
}

class IndexerReverseCommand(): AbstractBallInstantCommand()
{
    override fun executeImpl()
    {
        BallSubsystem.reverseIndexerMotor()
    }
}

class IndexerStopCommand(): AbstractBallInstantCommand()
{
    override fun executeImpl()
    {
        BallSubsystem.stopIndexerMotor()
    }
}

class AcquisitionStartCommand(): AbstractBallInstantCommand()
{
    override fun executeImpl()
    {
        BallSubsystem.startAcquisitionMotor()
    }
}

class AcquisitionReverseCommand(): AbstractBallInstantCommand()
{
    override fun executeImpl()
    {
        BallSubsystem.reverseAcquisitionMotor()
    }
}

class AcquisitionStopCommand(): AbstractBallInstantCommand()
{
    override fun executeImpl()
    {
        BallSubsystem.stopAcquisitionMotor()
    }
}

class TransportStartCommand(): AbstractBallInstantCommand()
{
    override fun executeImpl()
    {
        BallSubsystem.startTransportMotor()
    }
}

class TransportReverseCommand(): AbstractBallInstantCommand()
{
    override fun executeImpl()
    {
        BallSubsystem.reverseTransportMotor()
    }
}

class TransportStopCommand(): AbstractBallInstantCommand()
{
    override fun executeImpl()
    {
        BallSubsystem.stopTransportMotor()
    }
}

class IndexerAndTransportCommand(): AbstractBallInstantCommand()
{
    override fun executeImpl()
    {
        BallSubsystem.startIndexerMotor()
        BallSubsystem.startTransportMotor()
    }
}

class IndexerAndTransportReverseCommand(): AbstractBallInstantCommand()
{
    override fun executeImpl()
    {
        BallSubsystem.reverseIndexerMotor()
        BallSubsystem.reverseTransportMotor()
    }
}

class IndexerAndTransportStopCommand() : AbstractBallInstantCommand()
{
    override fun executeImpl()
    {
        BallSubsystem.stopIndexerMotor()
        BallSubsystem.stopTransportMotor()
    }
}

class BallAcquisitionStartSequenceCommand: AbstractAcquisitionArmInstantCommand()
{
    override fun executeImpl()
    {
        CompressorSubsystem.getInstance().lowerBallAcquisitionArm()
        BallSubsystem.startAcquisitionMotor()
        BallSubsystem.startTransportMotor()
    }
}
class BallAcquisitionStopSequenceCommand: AbstractAcquisitionArmInstantCommand()
{
    override fun executeImpl()
    {
        CompressorSubsystem.getInstance().raiseBallAcquisitionArm()
        BallSubsystem.stopAcquisitionMotor()
        BallSubsystem.stopTransportMotor() // Do we want to stop the transport motor - may need to be smarter about this
    }
}
