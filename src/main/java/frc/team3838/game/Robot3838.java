package frc.team3838.game;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.google.common.collect.ImmutableMap;

import edu.wpi.first.wpilibj.command.Command;
import frc.team3838.core.Abstract3838Robot;
import frc.team3838.core.subsystems.I3838Subsystem;
import frc.team3838.core.subsystems.drive.Abstract3838DriveTrainSubsystem;
import frc.team3838.game.commands.CommandDashboardButtons;
import frc.team3838.game.commands.autonomous.AutonomousSelector;
import frc.team3838.game.subsystems.DriveTrainSubsystem;
import frc.team3838.game.subsystems.ShooterSubsystem;



/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the IterativeRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the manifest file in the resource
 * directory.
 */
// *IMPORTANT: If you rename this class or change its package, the corresponding
//             change needs to be made in the ${projectRoot}/build.properties
//             file
public class Robot3838 extends Abstract3838Robot
{

    // ***********************************************************************************************
    // ***********************************************************************************************
    // TODO: Need to change Abstract3838Robot to extend TimedRobot for 2019 (and definitely for 2020)
    // Alternatively, there is the IterativeRobotBase:
    //    "This is identical to TimedRobot, except the main robot loop is not run automatically - users are 
    //    required to implement it inside of the startCompetition() method. This gives more freedom for advanced 
    //    users to handle the loop timing in different ways, but is also less-convenient."
    //    https://docs.wpilib.org/en/latest/docs/software/wpilib-overview/creating-robot-program.html#iterativerobotbase
    // ***********************************************************************************************
    // ***********************************************************************************************
    
    
    public Robot3838()
    {
        // the loop timing affects Shuffleboard updates via Suppliers and Consumers
        super(/*0.25*/);
    }
    
    
    @Nullable
    @Override
    protected String getRobotInitVersionInfo()
    {
        return  "Code Version 2019.1.07";
    }






    @SuppressWarnings("RedundantThrows")
    @Override
    protected void initAutonomousSelectorImpl() throws Exception
    {
        AutonomousSelector.initializeChooser();
    }


    @SuppressWarnings("RedundantThrows")
    @Nullable
    @Override
    protected Command getSelectedAutonomousCommandImpl() throws Exception
    {
        return AutonomousSelector.getSelectedCommand();
    }


    @Override
    protected void initJoysticks()
    {
        
    }


    @Override
    protected void additionalRobotInitWork()
    {
        // DashboardManager.getElectricalTab().add(new PowerDistributionPanel(CANs.PDP));
        // DashboardManager.getElectricalTab().add(HatchEjectorSubsystem.getInstance().getCompressor());

        CommandDashboardButtons.addAllCommands();
    }
    
    
    /**
     * This method is called once each time the robot enters Disabled mode.
     * You can use it to reset any subsystem information you want to clear when
     * the robot is disabled.
     */
    @Override
    public void disableInitAdditional()
    {
        ShooterSubsystem.INSTANCE.turnRingLightOff();
    }
    
    
    @Nonnull
    @Override
    protected ImmutableMap<Class<? extends I3838Subsystem>, Boolean> getEnabledSubsystemsMap() { return RobotMap.enabledSubsystemsMap; }


    @Nonnull
    @Override
    protected Abstract3838DriveTrainSubsystem getDriveTrainSubsystem()
    {
        return DriveTrainSubsystem.getInstance();
    }

}
