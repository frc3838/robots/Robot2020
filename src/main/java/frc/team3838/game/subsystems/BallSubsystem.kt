package frc.team3838.game.subsystems

import com.ctre.phoenix.motorcontrol.NeutralMode
import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab
import frc.team3838.core.hardware.createWpiVictorSPX
import frc.team3838.core.subsystems.Abstract3838Subsystem
import frc.team3838.game.RobotMap
import frc.team3838.game.commands.AcquisitionReverseCommand
import frc.team3838.game.commands.AcquisitionStartCommand
import frc.team3838.game.commands.AcquisitionStopCommand
import frc.team3838.game.commands.BallAcquisitionStartSequenceCommand
import frc.team3838.game.commands.BallAcquisitionStopSequenceCommand
import frc.team3838.game.commands.IndexerAndTransportCommand
import frc.team3838.game.commands.IndexerAndTransportReverseCommand
import frc.team3838.game.commands.IndexerAndTransportStopCommand
import frc.team3838.game.commands.IndexerReverseCommand
import frc.team3838.game.commands.IndexerStartCommand
import frc.team3838.game.commands.IndexerStopCommand
import frc.team3838.game.commands.TransportReverseCommand
import frc.team3838.game.commands.TransportStartCommand
import frc.team3838.game.commands.TransportStopCommand


object BallSubsystem : Abstract3838Subsystem()
{

    private const val ACQUISITION_FORWARD_SPEED: Double = 0.4
    private const val ACQUISITION_REVERSE_SPEED: Double = -0.3
    private const val INDEX_FORWARD_SPEED: Double = 0.5
    private const val INDEX_REVERSE_SPEED: Double = -0.5
    private const val TRANSPORT_FORWARD_SPEED: Double = 1.0
    private const val TRANSPORT_REVERSE_SPEED: Double = -0.5
    val tab: ShuffleboardTab = Shuffleboard.getTab("Ball")
    
    private var acquisitionMotor : WPI_VictorSPX? = null
    private var indexerMotor : WPI_VictorSPX? = null
    private var transportMotor: WPI_VictorSPX? = null
            
    override fun initSubsystem()
    {
        acquisitionMotor = createWpiVictorSPX(RobotMap.CANs.BALL_ACQUISITION_MOTOR, false, NeutralMode.Brake)
        indexerMotor = createWpiVictorSPX(RobotMap.CANs.BALL_INDEX_MOTOR, true, NeutralMode.Brake)
        transportMotor = createWpiVictorSPX(RobotMap.CANs.BALL_TRANSPORT_MOTOR, true, NeutralMode.Brake)
    }
    
    override fun initDefaultCommandImpl()
    {
        
    }
    
    fun startIndexerMotor() = indexerMotor?.set(INDEX_FORWARD_SPEED)
    fun reverseIndexerMotor() = indexerMotor?.set(INDEX_REVERSE_SPEED)
    fun stopIndexerMotor() = indexerMotor?.set(0.0)
    
    fun startAcquisitionMotor() = acquisitionMotor?.set(ACQUISITION_FORWARD_SPEED)
    fun reverseAcquisitionMotor() = acquisitionMotor?.set(ACQUISITION_REVERSE_SPEED)
    fun stopAcquisitionMotor() = acquisitionMotor?.set(0.0)
    
    fun startTransportMotor() = transportMotor?.set(TRANSPORT_FORWARD_SPEED)
    fun reverseTransportMotor() = transportMotor?.set(TRANSPORT_REVERSE_SPEED)
    fun stopTransportMotor() = transportMotor?.set(0.0)
    
    fun addAllBallHandlerCommands()
    {
        tab.add(IndexerStartCommand())
        tab.add(IndexerReverseCommand())
        tab.add(IndexerStopCommand())
        tab.add(AcquisitionStartCommand())
        tab.add(AcquisitionReverseCommand())
        tab.add(AcquisitionStopCommand())
        tab.add(TransportStartCommand())
        tab.add(TransportReverseCommand())
        tab.add(TransportStopCommand())
        tab.add(IndexerAndTransportCommand())
        tab.add(IndexerAndTransportReverseCommand())
        tab.add(IndexerAndTransportStopCommand())
        tab.add(BallAcquisitionStartSequenceCommand())
        tab.add(BallAcquisitionStopSequenceCommand())
    }
    
}