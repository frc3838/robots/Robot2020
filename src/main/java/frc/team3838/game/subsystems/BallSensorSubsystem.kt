package frc.team3838.game.subsystems

import com.google.common.collect.ImmutableSet
import edu.wpi.first.wpilibj.SerialPort
import frc.team3838.core.commands.AbstractPollingCommand
import frc.team3838.core.config.Baud
import frc.team3838.core.config.time.TimeDuration
import frc.team3838.core.hardware.LimitSwitch
import frc.team3838.core.logging.LaconicLeveledLogger
import frc.team3838.core.logging.LaconicLogger
import frc.team3838.core.subsystems.Abstract3838Subsystem
import frc.team3838.core.subsystems.I3838Subsystem
import frc.team3838.game.RobotMap
import frc.team3838.loopCountHistogram
import frc.team3838.processTokenTimer
import frc.team3838.readTokenTimer
import frc.team3838.serialRead
import frc.team3838.serialRead2
import frc.team3838.writeTimer
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import mu.KotlinLogging
import org.slf4j.event.Level
import java.lang.Exception
import java.nio.charset.Charset
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicIntegerArray

private val charset: Charset = Charsets.US_ASCII
private const val delimiter = "\r\n"

internal val laconicLoggerInterval = TimeDuration.ofSeconds(3)

object BallSensorSubsystem: Abstract3838Subsystem()
{
    val requiredSubsystem: ImmutableSet<I3838Subsystem> = ImmutableSet.of(BallSensorSubsystem) 
    private val laconicReadLogger = LaconicLogger(logger, laconicLoggerInterval)
    private val laconicErrorLogger = LaconicLogger(logger, laconicLoggerInterval)
    private val laconicJobErrorLogger = LaconicLogger(logger, laconicLoggerInterval)
    
    
    private const val NO_TOKEN = "NO_TOKEN"
    
    private const val SETUP_COMPLETION_MESSAGE = "Setup Complete!"
    
    private val delimiterByte = '\n'.toByte()
    private val baud: Baud = Baud.BAUD_115200
    private val port: SerialPort.Port = SerialPort.Port.kUSB1

    private var serialPort: SerialPort? = null 
    
    private var ballSensor1: LimitSwitch? = null
    private var ballSensor2: LimitSwitch? = null
    private var ballSensor3: LimitSwitch? = null
    private var ballSensor0: LimitSwitch? = null

    private var job: Job? = null
    
    var currentShooterAngle: Double = -2.0
        private set

    var initializedOk = false
        private set
    
    
    override fun initSubsystem()
    {
        logger.debug { "Initializing ball sensors " }
        ballSensor0 = LimitSwitch.create(RobotMap.DIOs.BALL_INTAKE_COUNTER_0, false)
        ballSensor1 = LimitSwitch.create(RobotMap.DIOs.BALL_INTAKE_COUNTER_1, false)
        ballSensor2 = LimitSwitch.create(RobotMap.DIOs.BALL_INTAKE_COUNTER_2, false)
        ballSensor3 = LimitSwitch.create(RobotMap.DIOs.BALL_INTAKE_COUNTER_3, false)

        ShooterSubsystem.tab.addBoolean("Sensor 0") { ballSensor0?.isActivated == true }
        ShooterSubsystem.tab.addBoolean("Sensor 1") { ballSensor1?.isActivated == true }
        ShooterSubsystem.tab.addBoolean("Sensor 2") { ballSensor2?.isActivated == true }
        ShooterSubsystem.tab.addBoolean("Sensor 3") { ballSensor3?.isActivated == true }
        
        initializeVL53xx()
//        val acknowledged = enterContinuousStreamMode()
//        logger.info { "EnterContinuousMode acknowledged: $acknowledged" }
        
        job = GlobalScope.launch {
            while (true)
            {
                try
                {
                    queryForAndSetCurrentShooterAngle()
                    //executeQuery(MeasurementAll)
                    //executeQuery(BallCountAll)
                    //continuousRead()
                }
                catch (e: Throwable)
                {
                    laconicJobErrorLogger.warn(e){"An exception occurred in the BallSensorSubsystem background job: $e"}
                }
            }
        }
    }

    override fun initDefaultCommandImpl()
    {
        defaultCommand = object : AbstractPollingCommand()
        {
            val laconicLogger = LaconicLeveledLogger(logger, TimeDuration.ofSeconds(5), Level.DEBUG)
            
            override fun initializeImpl()
            {
                super.initializeImpl()
                
            }

            override fun doAction()
            {
                laconicLogger.log { "$counts   Sensor0: ${ballSensor0?.isActivated} Sensor1: ${ballSensor1?.isActivated} Sensor2: ${ballSensor2?.isActivated} Sensor3: ${ballSensor3?.isActivated}"  }
            }

            override fun getRequiredSubsystems(): ImmutableSet<I3838Subsystem> = requiredSubsystem

        }
    }

    private fun initializeVL53xx()
    {
        try
        {
            logger.info { "Initializing VL53xx Controller." }
            serialPort = SerialPort(baud.baudRate, port)
            serialPort?.setReadBufferSize(1)
            serialPort?.setTimeout(0.05)
            serialPort?.setFlowControl(SerialPort.FlowControl.kNone)
            serialPort?.setWriteBufferMode(SerialPort.WriteBufferMode.kFlushOnAccess)

            var finished = false
            val start = System.currentTimeMillis()
            while (!finished)
            {
                val token = readToken()
                logger.info { "VL53 Startup: $token" }
                finished = token.contains(SETUP_COMPLETION_MESSAGE) || System.currentTimeMillis() - start > TimeUnit.SECONDS.toMillis(4)
            }

            enterNormalMode()
            if (!initializedOk)
            {
                logger.info { "Failed to enter Normal Mode. Attempting again" }
                enterNormalMode()
                if (!initializedOk)
                {
                    logger.warn { "Failed to enter Normal Mode. Attempting a third and final time" }
                    enterNormalMode()
                }
            }

            logger.info { "VL53xx Controller Initialization Complete. initializedOk = $initializedOk" }
        }
        catch (e: Throwable)
        {
            logger.warn(e) {"ERROR: An Exception occurred when initializing the SerialPort: $e" }
        }
    }

    private fun enterNormalMode(timeoutInSeconds: Long = 4)
    {
        var token = executeQueryNoTimer(NormalMode)
        initializedOk = token.contains("OK")
        var finished = initializedOk
        val start = System.currentTimeMillis()
        while (!finished)
        {
            token = readToken()
            initializedOk = token.contains("OK")
            finished = initializedOk || System.currentTimeMillis() - start > TimeUnit.SECONDS.toMillis(timeoutInSeconds)
        }
    }

    private fun enterContinuousStreamMode() = enterMode(ContinuousMode, "OK MEAS")

    private fun enterMode(query: DirectQuery, ack: String, timeoutInSeconds: Long = 4): Boolean
    {
        var token = executeQueryNoTimer(query)
        var isAcknowledged = token.startsWith(ack)
        var finished = isAcknowledged
        val start = System.currentTimeMillis()
        while (!finished)
        {
            token = readToken()
            isAcknowledged = token.startsWith(ack)
            finished = isAcknowledged || System.currentTimeMillis() - start > TimeUnit.SECONDS.toMillis(timeoutInSeconds)
        }
        return isAcknowledged
    }

    fun queryForAndSetCurrentShooterAngle()
    {
        currentShooterAngle =  executeQuery(ShooterAngleQuery)
    }

    fun <T> executeQuery(query: Query<T>): T
    {
        // TODO need to wrap in if (initializedOk) - but need to deal what to return- add an invalidState response to the Query

        writeTimer.time().use {
            serialPort?.writeString(query.query)
            serialPort?.flush() // TODO test if we need the flush
        }

        val rawToken = readTokenTimer.time().use {
            readToken()
        }

        return processTokenTimer.time().use { query.processToken(rawToken) }
    }

    fun <T> executeQueryNoTimer(query: Query<T>): T
    {
        serialPort?.writeString(query.query)
        serialPort?.flush() // TODO test if we need the flush
        val rawToken = readToken()
        return query.processToken(rawToken)
    }

    private fun continuousRead()
    {
        try
        {
            val token = readTokenTimer.time().use { readToken() }
            MeasurementAll.processToken(token)
        }
        catch (e: Throwable)
        {
            laconicErrorLogger.warn() {"Continuous Read Error: $e"}
        }
    }

    private fun readToken(): String
    {
        return if (serialPort != null)
        {
            try
            {
                var bytes = serialRead.time().use {
                    serialPort!!.read(serialPort!!.bytesReceived)
                }
    
                var loopCount = 0
                while (bytes.isEmpty() || bytes.last() != delimiterByte)
                {
                    loopCount++
                    bytes += serialRead2.time().use { serialPort!!.read(serialPort!!.bytesReceived) }
                }
    
                loopCountHistogram.update(loopCount)
    
                bytes.toString(charset).removeSuffix(delimiter).trim()
            }
            catch (e: Throwable)
            {
                val message = "ERROR in read: $e"
                laconicReadLogger.debug { message }
                return message // TODO: all processors need to deal with this
            }
        }
        else
        {
            laconicReadLogger.warn() {"SerialPort is null. Cannot read bytes. Returning empty string"}
            ""
        }
    }

    override fun isEnabled(): Boolean = super.isEnabled() && initializedOk
}

abstract class Query<T>(baseQuery: String)
{
    private val terminator = "\n"
    val query = "${baseQuery}${terminator}"

    abstract fun processToken(token: String): T
}

open class DirectQuery(baseQuery: String) : Query<String>(baseQuery)
{
    override fun processToken(token: String): String = token
}

abstract class SensorQuery<T>(baseQuery: String, sensorNum: Int) : Query<T>("$baseQuery $sensorNum")

object NormalMode : DirectQuery("N")

object ContinuousMode : DirectQuery("C")

object ShooterAngleQuery : Query<Double>("I")
{
    private val logger = KotlinLogging.logger {}
    private val laconicLogger = LaconicLogger(logger, laconicLoggerInterval)
    override fun processToken(token: String): Double
    {
        return try
        {
            val normalizedToken = token.removeSuffix(delimiter).trim()
            val angle = (360.0 - ((normalizedToken.toInt() * 360.0) / 16383.0)) - 6 // 6 is offset
            laconicLogger.debug() { "ShooterAngleQuery calculated angle of $angle for normalizedToken: '$normalizedToken'" }
            return angle
        }
        catch (e:Exception)
        {
            laconicLogger.warn() { "ShooterAngleQuery: could NOT process token: '$token'" }
            -1.0
        }
    }
}


// TODO: Need to handle ALL differently
class MeasurementQuery(sensorNum: Int) : SensorQuery<Long>("M", sensorNum)
{
    override fun processToken(token: String): Long
    {
        val raw = token.substringAfter(',').trim()
        return raw.toLongOrNull() ?: -1
    }
}

object MeasurementMap : SensorQuery<Map<Int, Long>>("M", 255)
{
    override fun processToken(token: String): Map<Int, Long>
    {
        return token.splitToSequence(',')
            .mapIndexed { index, raw -> Pair(index, raw.toLongOrNull() ?: -1) }
            .associateBy({ it.first }, { it.second })
    }
}

object MeasurementAll : SensorQuery<Unit>("M", 255)
{
    private val logger = KotlinLogging.logger {}
    private val laconicExceptionLogger = LaconicLogger(logger, TimeDuration.ofSeconds(5))
    private val laconicGeneralLogger = LaconicLogger(logger, TimeDuration.ofSeconds(5))
    
    override fun processToken(token: String)
    {
        try
        {
            val lines = token.split(delimiter)
            val last = lines.last()
            last.splitToSequence(',')
                .forEachIndexed() { index: Int, raw: String ->
                    distances.set(index, raw.toIntOrNull() ?: -1)
                }
        }
        catch (e: Throwable)
        {
            laconicExceptionLogger.warn(e){ "An exception occurred when processing MeasurementAll token >>>\n${token}\n<<<. Exception: $e" }
        }
    }

}

class Status(sensorNum: Int) : SensorQuery<String>("S", sensorNum)
{
    override fun processToken(token: String): String
    {
        TODO("Function 'processToken' is not implemented for the Status query")
    }
}

object StatusAll : SensorQuery<Unit>("S", 255)
{
    override fun processToken(token: String)
    {
        TODO("Function 'processToken' is not implemented for the StatusAll query")
    }
}

class BallCount(sensorNum: Int) : SensorQuery<String>("B", sensorNum)
{
    override fun processToken(token: String): String
    {
        TODO("Function 'processToken' is not implemented for the BallCount query")
    }
}

object BallCountAll : SensorQuery<Unit>("B", 255)
{
    private val logger = KotlinLogging.logger {}
    private val laconicExceptionLogger = LaconicLogger(logger, TimeDuration.ofSeconds(5))
    override fun processToken(token: String)
    {
        try
        {
            val lines = token.split(delimiter)
            val last = lines.last()
            last.splitToSequence(',')
                .forEachIndexed() { index: Int, raw: String ->
                    counts.set(index, raw.toIntOrNull() ?: -1)
                }
        }
        catch (e: Throwable)
        {
            laconicExceptionLogger.warn(e) { "An exception occurred when processing BallCountAll token >>>\n$${token}\n<<<. Exception: $e" }
        }
    }
}

val distances = AtomicIntegerArray(10)
val counts = AtomicIntegerArray(10)