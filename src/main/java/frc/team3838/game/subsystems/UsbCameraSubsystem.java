package frc.team3838.game.subsystems;

import javax.annotation.Nonnull;

import frc.team3838.core.subsystems.Abstract3838TwoUsbCameraSubsystem;
import frc.team3838.core.subsystems.CameraConfig;
import frc.team3838.game.commands.camera.SwitchCameraCommand;



public class UsbCameraSubsystem extends Abstract3838TwoUsbCameraSubsystem
{

  
    @SuppressWarnings("RedundantThrows")
    @Override
    protected void initDefaultCommandImpl() throws Exception
    {
        setDefaultCommand(new SwitchCameraCommand());
    }
    
    
    /** The Singleton instance of this UsbCameraSubsystem. External classes should use the {@link #getInstance()} method to get the instance. */
    private static UsbCameraSubsystem INSTANCE;
    
    
    /**
     * Creates a new instance of this UsbCameraSubsystem.
     * This constructor is private since this class is a Singleton. External classes
     * should use the {@link #getInstance()} method to get the instance.
     * For example, instead of doing this:
     * <pre>
     *     UsbCameraSubsystem usbCameraSubsystem = new UsbCameraSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     UsbCameraSubsystem usbCameraSubsystem = UsbCameraSubsystem.getInstance();
     * </pre>
     */
    private UsbCameraSubsystem()
    {
        // The super constructor checks if this subsystem is enabled and if so it calls initSubsystem()
        super(new CameraConfig(0, "Front_Camera_0"), 
              new CameraConfig(1, "Rear_Camera_1"));
        // ** DO NOT PUT ANY CODE IN THIS CONSTRUCTOR **
        // ** Do all initialization work in the initSubsystem() method     
    }
    
    
    /**
     * Returns the Singleton instance of this UsbCameraSubsystem. This static method
     * should be used by external classes, rather than the constructor
     * to get the instance of this class.
     * <pre>
     *     UsbCameraSubsystem usbCameraSubsystem = new UsbCameraSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     UsbCameraSubsystem usbCameraSubsystem = UsbCameraSubsystem.getInstance();
     * </pre>
     *
     * @return the single instance of this subsystem.
     */
    @Nonnull
    public static UsbCameraSubsystem getInstance()
    {
        // "Double Checked Locking" implementation that provides quick access but with thread safe initialization,
        // and eliminates potential subtle initialization sequence bugs Eager initialization may cause
        // See Method 4 at https://www.geeksforgeeks.org/singleton-design-pattern/ 
        
        // Fast (non-synchronized) check to reduce overhead of acquiring a lock when it's not needed
        if (INSTANCE == null)
        {
            // Make thread safe 
            synchronized (UsbCameraSubsystem.class)
            {
                // Check nullness again as multiple threads can reach above null check
                if (INSTANCE == null)
                {
                    INSTANCE = new UsbCameraSubsystem();
                }
            }
        }
        return INSTANCE;
    }
}
