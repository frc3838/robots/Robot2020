package frc.team3838.game.subsystems

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard
import frc.team3838.core.config.time.TimeDuration
import frc.team3838.core.controls.JoystickMovement
import frc.team3838.core.controls.Throttle
import frc.team3838.core.hardware.createWpiTalonSRX
import frc.team3838.core.isInEndGame
import frc.team3838.core.isNotDuringCompOrPracticeMode
import frc.team3838.core.logging.LaconicLeveledLogger
import frc.team3838.core.subsystems.Abstract3838Subsystem
import frc.team3838.core.utils.MathUtils
import frc.team3838.game.RobotMap
import org.slf4j.event.Level


object WinchSubsystem: Abstract3838Subsystem()
{
    private val JOYSTICK_DIRECTION_TO_CLIMB = JoystickMovement.PUSH
    private const val JOYSTICK_DRIFT_ZONE_THRESHOLD:Double = 0.005
    private val MAX_CLIMB_SPEED:Double  = 1.0
    private val DEFAULT_CLIMB_SPEED:Double  =  0.5 // TODO: to be determined
    private var winchMotor: WPI_TalonSRX? = null
    
    val laconicLogger = LaconicLeveledLogger(logger, TimeDuration.ofSeconds(5), Level.INFO)

    override fun initSubsystem()
    { 
        winchMotor = createWpiTalonSRX(RobotMap.CANs.WINCH_MOTOR, invert = true)
        logger.info { "Adding WinchSpeed to SmartDashboard" }
        SmartDashboard.putNumber("WinchSpeed", SmartDashboard.getNumber("WinchSpeed", 0.0))

    }

    @Throws(Exception::class)
    override fun initDefaultCommandImpl()
    {
//        defaultCommand = object : AbstractPollingCommand() {
//            override fun doAction() = operate()
//            override fun getRequiredSubsystems(): ImmutableSet<I3838Subsystem> = requiredSubsystem
//        }
    }
    
    
    fun operate()
    {
        winchMotor?.set(0.0)
    }
    
    fun okToTurnOnWinch(): Boolean = isNotDuringCompOrPracticeMode() || isInEndGame()
    
    
    private fun getThrottleSpeed(throttle: Throttle): Double
    {
        var speed = throttle.get()
        // If we want PUSH to Climb, we need to invert the Joystick value since pushing is a negative number
        if (JOYSTICK_DIRECTION_TO_CLIMB === JoystickMovement.PUSH)
        {
            speed = -speed
        }
        
        val absoluteSpeed = Math.abs(speed)
        
        //compensate for dead zone on Joystick
        if (absoluteSpeed < JOYSTICK_DRIFT_ZONE_THRESHOLD)
        {
            speed = 0.0
        }
        if (absoluteSpeed > MAX_CLIMB_SPEED)
        {
            speed = MathUtils.matchSign(MAX_CLIMB_SPEED, speed)
        }
        return speed
    }
    
    fun turnOnWinchMotorForClimb()
    {
        val speed = DEFAULT_CLIMB_SPEED // SmartDashboard.getNumber("WinchSpeed", DEFAULT_CLIMB_SPEED)
        logger.debug { "Setting winch motor speed to $speed for climb" }
        winchMotor?.set(speed)
    }

    fun turnOnWinchMotorForDecent()
    {
        val speed = -DEFAULT_CLIMB_SPEED // -SmartDashboard.getNumber("WinchSpeed", DEFAULT_CLIMB_SPEED)
        logger.debug { "Setting winch motor speed to $speed for lowering" }
        winchMotor?.set(speed)
    }
    fun stopWinchMotor()
    {
        winchMotor?.set(0.0)
        logger.debug { "Stopping winch motor" }
    }

}