package frc.team3838.game.subsystems;


import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import frc.team3838.core.subsystems.Abstract3838CompressorSubsystem;
import frc.team3838.game.RobotMap.CANs;
import frc.team3838.game.RobotMap.PCM;



@SuppressWarnings("RedundantThrows")
public class CompressorSubsystem extends Abstract3838CompressorSubsystem
{
    private static final Logger logger = LoggerFactory.getLogger(CompressorSubsystem.class);
    
    private static final DoubleSolenoid.Value elevatorRaiseValue = Value.kReverse;
    private static final DoubleSolenoid.Value elevatorLowerValue = Value.kForward;
    private static final DoubleSolenoid.Value ballAcquisitionRaiseValue = Value.kForward;
    private static final DoubleSolenoid.Value ballAcquisitionLowerValue = Value.kReverse;
    private static final DoubleSolenoid.Value wheelSpinnerRaiseValue = Value.kReverse;
    private static final DoubleSolenoid.Value wheelSpinnerLowerValue = Value.kForward;
    
    private DoubleSolenoid elevatorRaiseLowerSolenoid;
    private DoubleSolenoid ballAcquisitionRaiseLowerSolenoid;
    private DoubleSolenoid wheelSpinnerRaiseLowerSolenoid;
    
    
    @Override
    protected void initSubsystem() throws Exception
    {
        super.initSubsystem();
        // NOTE: The Compressor is initialized in the super class.
        
        ballAcquisitionRaiseLowerSolenoid =
                new DoubleSolenoid(getPcmCanId(),
                                   PCM.BALL_ACQUISITION_SolenoidForwardChannel,
                                   PCM.BALL_ACQUISITION_SolenoidReverseChannel);
        
        elevatorRaiseLowerSolenoid =
                new DoubleSolenoid(getPcmCanId(),
                                   PCM.ELEVATOR_SolenoidForwardChannel,
                                   PCM.ELEVATOR_SolenoidReverseChannel);
    
    
        wheelSpinnerRaiseLowerSolenoid = new DoubleSolenoid(getPcmCanId(),
                                                            PCM.WOF_SolenoidForwardChannel,
                                                            PCM.WOF_SolenoidReverseChannel);
        
    }
    
    
    @Override
    public int getPcmCanId()
    {
        return CANs.PCM;
    }
    
    
    @Override
    protected void initDefaultCommandImpl() throws Exception
    {
        /* No Op */
    }
    
    
    public void raiseElevator()
    {
        elevatorRaiseLowerSolenoid.set(elevatorRaiseValue);
    }
    
    
    public void lowerElevator()
    {
        elevatorRaiseLowerSolenoid.set(elevatorLowerValue);
    }
    
    
    /**
     * Turns off the solenoid (used mostly for testing).
     */
    public void elevatorOff()
    {
        elevatorRaiseLowerSolenoid.set(Value.kOff);
    }
    
    
    public void raiseBallAcquisitionArm()
    {
        ballAcquisitionRaiseLowerSolenoid.set(ballAcquisitionRaiseValue);
    }
    
    
    public void lowerBallAcquisitionArm()
    {
        ballAcquisitionRaiseLowerSolenoid.set(ballAcquisitionLowerValue);
    }
    
    
    /**
     * Turns off the solenoid (used mostly for testing).
     */
    public void ballAcquisitionArmOff()
    {
        ballAcquisitionRaiseLowerSolenoid.set(Value.kOff);
    }
    
    
    public void raiseWheelSpinner()
    {
        wheelSpinnerRaiseLowerSolenoid.set(wheelSpinnerRaiseValue);
    }
    
    
    public void lowerWheelSpinner()
    {
        wheelSpinnerRaiseLowerSolenoid.set(wheelSpinnerLowerValue);
    }
    
    
    /**
     * Turns off the solenoid (used mostly for testing).
     */
    public void wheelSpinnerOff()
    {
        wheelSpinnerRaiseLowerSolenoid.set(Value.kOff);
    }
    
    
    /** The Singleton instance of this CompressorSubsystem. External classes should use the {@link #getInstance()} method to get the instance. */
    private static CompressorSubsystem INSTANCE;
    
    
    /**
     * Creates a new instance of this CompressorSubsystem.
     * This constructor is private since this class is a Singleton. External classes
     * should use the {@link #getInstance()} method to get the instance.
     * For example, instead of doing this:
     * <pre>
     *     CompressorSubsystem compressorSubsystem = new CompressorSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     CompressorSubsystem compressorSubsystem = CompressorSubsystem.getInstance();
     * </pre>
     */
    private CompressorSubsystem()
    {
        // The super constructor checks if this subsystem is enabled and if so it calls initSubsystem()
        super();
        // ** DO NOT PUT ANY CODE IN THIS CONSTRUCTOR **
        // ** Do all initialization work in the initSubsystem() method 
    }
    
    
    /**
     * Returns the Singleton instance of this CompressorSubsystem. This static method
     * should be used by external classes, rather than the constructor
     * to get the instance of this class.
     * <pre>
     *     CompressorSubsystem compressorSubsystem = new CompressorSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     CompressorSubsystem compressorSubsystem = CompressorSubsystem.getInstance();
     * </pre>
     *
     * @return the single instance of this subsystem.
     */
    @Nonnull
    public static CompressorSubsystem getInstance()
    {
        // "Double Checked Locking" implementation that provides quick access but with thread safe initialization,
        // and eliminates potential subtle initialization sequence bugs Eager initialization may cause
        // See Method 4 at https://www.geeksforgeeks.org/singleton-design-pattern/ 
        
        // Fast (non-synchronized) check to reduce overhead of acquiring a lock when it's not needed
        if (INSTANCE == null)
        {
            // Make thread safe 
            synchronized (CompressorSubsystem.class)
            {
                // Check nullness again as multiple threads can reach above null check
                if (INSTANCE == null)
                {
                    INSTANCE = new CompressorSubsystem();
                }
            }
        }
        return INSTANCE;
    }
}

