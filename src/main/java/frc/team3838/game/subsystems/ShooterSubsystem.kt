package frc.team3838.game.subsystems

import com.ctre.phoenix.motorcontrol.ControlMode
import com.ctre.phoenix.motorcontrol.FeedbackDevice
import com.ctre.phoenix.motorcontrol.NeutralMode
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX
import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX
import com.google.common.collect.ImmutableSet
import edu.wpi.first.wpilibj.Relay
import edu.wpi.first.wpilibj.buttons.Button
import edu.wpi.first.wpilibj.shuffleboard.BuiltInWidgets
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard
import edu.wpi.first.wpilibj.smartdashboard.SendableBuilder.BooleanConsumer
import frc.team3838.core.commands.AbstractPollingCommand
import frc.team3838.core.config.time.TimeDuration
import frc.team3838.core.controls.NoOpButton
import frc.team3838.core.controls.SimpleAxis
import frc.team3838.core.dashboard.addBooleanBox
import frc.team3838.core.dashboard.addDoubleSettingTextBox
import frc.team3838.core.dashboard.addIntSettingTextBox
import frc.team3838.core.dashboard.addToggleSwitch
import frc.team3838.core.dashboard.matchPlayTab
import frc.team3838.core.hardware.LimitSwitch
import frc.team3838.core.hardware.createWpiVictorSPX
import frc.team3838.core.logging.LaconicLeveledLogger
import frc.team3838.core.pid.DConsumer
import frc.team3838.core.pid.FConsumer
import frc.team3838.core.pid.IConsumer
import frc.team3838.core.pid.PConsumer
import frc.team3838.core.pid.PIDTuner
import frc.team3838.core.subsystems.Abstract3838Subsystem
import frc.team3838.core.subsystems.I3838Subsystem
import frc.team3838.core.utils.MathUtils
import frc.team3838.core.utils.PIDSetting
import frc.team3838.game.RobotMap
import frc.team3838.game.RobotMap.CANs
import frc.team3838.game.RobotMap.DIOs
import frc.team3838.game.commands.RingLightToggleCommand
import frc.team3838.game.commands.ShooterCallSetMotorSpeedCommand
import frc.team3838.game.commands.StopShooterMotorsCommand
import org.slf4j.event.Level
import java.util.function.BooleanSupplier
import java.util.function.DoubleConsumer
import java.util.function.IntConsumer
import kotlin.math.abs
import kotlin.math.max

enum class AngleMovementDirection { Raising, Lowering, Unknown}

object ShooterSubsystem : Abstract3838Subsystem()
{
    private const val DEFAULT_F = 1023.0 / 7200.0
    // Safe angles: 19.5 THROUGH 49.6
    private const val MIN_ANGLE = 20 
    private const val MAX_ANGLE = 49
    private const val ABSOLUTE_SPEED_LIMIT_FOR_NEARING_LIMIT = 0.4
    private const val NEAR_ANGLE_LIMIT_THRESHOLD = 4
    
    val requiredSubsystem: ImmutableSet<I3838Subsystem> by lazy { ImmutableSet.of<I3838Subsystem>(ShooterSubsystem) }

    @JvmField
    val tab = Shuffleboard.getTab("Shooter")
    private const val ANGLE_MOTOR_SPEED = 0.2

    // From the CTRE Manual
    private const val ENCODER_UNITS_PER_REV = 4096
    
    /*
     * The following factors are to convert between RPM and the sensor velocity which is represented in "Units per 100ms". 
     * When setting speed on the MotorController in Velocity mode, the value is set in "position change / 100ms".
     * 
     * The 600 is derived frm the fact that 1 sec is 1000ms so there are 600 "100 ms" units in a second. See the 
     * <a href="https://github.com/CrossTheRoadElec/Phoenix-Examples-Languages/tree/master/Java/VelocityClosedLoop">Velocity Closed Loop</a>
     * example code, specifically the "teleopPeriodic" method in the Robot class.
     */
    /**
     * Multiple the `getSelectedSensorVelocity()` value by this factor to get RPM.
     */
    private const val SensorVelocity_READING_TO_RPM_FACTOR = 600.0 / ENCODER_UNITS_PER_REV // 600 is the number of "100ms units" in a minute

    /**
     * Multiple the target value by this factor to get RPM.
     */
    private const val TARGET_RPM_TO_SensorVelocity_SETTING_FACTOR = 1.0 / SensorVelocity_READING_TO_RPM_FACTOR

    
    // Using the encoders:
    //    https://www.chiefdelphi.com/t/talonsrx-encoder-values-to-smart-dashboard-shuffleboard/343651/2
    private const val SDB_SHARED_SPEED = "SHARED Speed Setting"
    
    private val angleSettingLaconicLogger = LaconicLeveledLogger(logger, TimeDuration.ofSeconds(1), Level.DEBUG)

    //    public static final String SDB_TOP_ACTUAL_SPEED_SETTING = "Top Actual Speed Setting";
    //    public static final String SDB_BOT_ACTUAL_SPEED_SETTING = "Bottom Actual Speed Setting";
    private const val SDB_TARGET_RPM = "Target RPM"
    private const val SDB_UPPER_CALC_SPEED = "Upper Calc RPM"
    private const val SDB_LOWER_CALC_SPEED = "Lower Calc RPM"
    private const val SDB_UPPER_MAX_CALC_SPEED = "Upper Max Calc RPM"
    private const val SDB_LOWER_MAX_CALC_SPEED = "Lower Max Calc RPM"
    private const val SDB_UPPER_SPEED_VARIANCE = "Upper %"
    private const val SDB_LOWER_SPEED_VARIANCE = "Lower %"
    
    private val enablePidTuning = false
    var isUsePidLoop = true

    /** Whether to use the PID Control for setting the motor speed (true), or use percentage setting for speed (false).   */
    private var upperUsePidMode = true

    /** Whether to use the PID Control for setting the motor speed (true), or use percentage setting for speed (false).   */
    private var botUsePidMode = true
    private val upperMotorPidSetting = PIDSetting("Upper", 0.25, 0.001, 20.0, DEFAULT_F)
    private val lowerMotorPidSetting = PIDSetting("Lower", 0.25, 0.001, 20.0, DEFAULT_F)
    private var upperPidTuner: PIDTuner? = null
    private var lowerPidTuner: PIDTuner? = null
    private var angleFlagLimitSwitch: LimitSwitch = LimitSwitch.createNoOp(dummyValue = true)
    private var angleAxis: SimpleAxis? = null
    private var angleSafetyOverrideButton: Button = NoOpButton()
    private var lastMovementDirection: AngleMovementDirection = AngleMovementDirection.Unknown
    
    
    private var angleMotor: WPI_VictorSPX? = null
    private var upperMotor: WPI_TalonSRX? = null
    private var lowerMotor: WPI_TalonSRX? = null
    private var upperVelocityReading = 0
    private var lowerVelocityReading = 0
    
    var sharedSpeedSetting = 0.5
        private set
    var targetRpms = 5500
    var upperCalcRpm = 0.0
        private set
    var lowerCalcRpm = 0.0
        private set
    
    private var ringLight: Relay? = null

    // We want bottom spin, so we set lower motor to spin a bit faster than target RPM and the upper motor a bit slower
    private var upperPercentVariance = 0.95
    private var lowerPercentVariance = 1.20
    var maxUpperCalcRpm = 0.0
        private set
    var maxLowerCalcRpm = 0.0
        private set
    @Throws(Exception::class)
    override fun initSubsystem()
    {
        ringLight = Relay(RobotMap.Relays.RING_LIGHT, Relay.Direction.kForward)
        ringLight?.set(Relay.Value.kOff)
        
        
        angleMotor = createWpiVictorSPX(CANs.SHOOTER_ANGLE_MOTOR, true, NeutralMode.Brake)
        angleAxis = RobotMap.UI.getShooterAngleControl()
        angleSafetyOverrideButton = RobotMap.UI.getShooterAngleSafetyOverrideButton()
        // We're configured so when activated, we know we are only safe to move in one direction
        angleFlagLimitSwitch = LimitSwitch.create(DIOs.SHOOTER_ANGLE_LIMIT_FLAG, true)
        
        matchPlayTab.addBooleanBox("Angle Flag", BooleanSupplier { angleFlagLimitSwitch.isActivated})
        matchPlayTab.addBooleanBox("AtUpperLimit", BooleanSupplier { isAtUpperLimit()})
        matchPlayTab.addBooleanBox("AtLowerLimit", BooleanSupplier { isAtLowerLimit()})
        matchPlayTab.addString("Current Angle") { getCurrentShooterAngleAsString() }



        upperMotor = createTalon(CANs.SHOOTER_UPPER_MOTOR, upperMotorPidSetting, false)
        lowerMotor = createTalon(CANs.SHOOTER_LOWER_MOTOR, lowerMotorPidSetting, true)
        if (enablePidTuning)
        {
            upperPidTuner = PIDTuner(upperMotorPidSetting,
                                     BooleanConsumer { value: Boolean -> upperUsePidMode = value },
                                     PConsumer(upperMotor),
                                     IConsumer(upperMotor),
                                     DConsumer(upperMotor),
                                     FConsumer(upperMotor))
            lowerPidTuner = PIDTuner(lowerMotorPidSetting,
                                     BooleanConsumer { value: Boolean -> botUsePidMode = value },
                                     PConsumer(lowerMotor),
                                     IConsumer(lowerMotor),
                                     DConsumer(lowerMotor),
                                     FConsumer(lowerMotor))
            tab.addNumber("Upper Graph") { upperCalcRpm }
                .withWidget(BuiltInWidgets.kGraph)
                .withProperties(mapOf<String, Any>("Visible time" to 90))
            tab.addNumber("Lower Graph") { lowerCalcRpm }
                .withWidget(BuiltInWidgets.kGraph)
                .withProperties(mapOf<String, Any>("Visible time" to 90))
        }
        
        tab.addNumber(SDB_UPPER_CALC_SPEED) { upperCalcRpm }
        tab.addNumber(SDB_UPPER_MAX_CALC_SPEED) { maxUpperCalcRpm }
        tab.addNumber(SDB_LOWER_CALC_SPEED) { lowerCalcRpm }
        tab.addNumber(SDB_LOWER_MAX_CALC_SPEED) { maxLowerCalcRpm }
        logger.debug("Adding Shooter Widgets to Shooter Tab")
        
        @Suppress("UNUSED_VARIABLE") 
        val shardedSpeedWidget = tab.addDoubleSettingTextBox(SDB_SHARED_SPEED, sharedSpeedSetting, DoubleConsumer { value: Double ->
            sharedSpeedSetting = MathUtils.constrainBetweenNegOneAndOne(
                    value)
        })

        @Suppress("UNUSED_VARIABLE")
        val targetRpmWidget = tab.addIntSettingTextBox(SDB_TARGET_RPM, targetRpms, IntConsumer { targetRpms: Int -> this.targetRpms = targetRpms })

        @Suppress("UNUSED_VARIABLE")
        val lowerVarianceWidget = tab.addDoubleSettingTextBox(SDB_LOWER_SPEED_VARIANCE, lowerPercentVariance, DoubleConsumer { lowerPercentVariance: Double -> setLowerPercentVariance(lowerPercentVariance) })

        @Suppress("UNUSED_VARIABLE")
        val upperVarianceWidget = tab.addDoubleSettingTextBox(SDB_UPPER_SPEED_VARIANCE, upperPercentVariance, DoubleConsumer { upperPercentVariance: Double -> setUpperPercentVariance(upperPercentVariance) })
        tab.addToggleSwitch("Use PID", isUsePidLoop, BooleanConsumer { usePidLoop: Boolean -> isUsePidLoop = usePidLoop })
    }

    private fun createTalon(deviceNumber: Int, pidSetting: PIDSetting, inverse: Boolean): WPI_TalonSRX
    {
        val talon = WPI_TalonSRX(deviceNumber)
        // Factory Default all hardware to prevent unexpected behaviour [From VelocityClosedLoop Example]
        talon.configFactoryDefault()
        talon.setNeutralMode(NeutralMode.Brake)

        // Config sensor used for Primary PID [Velocity]  [From VelocityClosedLoop Example]
        talon.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Relative,
                                           Constants.PID_LOOP_INDEX,
                                           Constants.TIMEOUT_MS)

        // Phase sensor accordingly. Positive Sensor Reading should match Green (blinking) LEDs on Talon  [From VelocityClosedLoop Example]
        talon.setSensorPhase(true)

        // Config the peak and nominal outputs  [From VelocityClosedLoop Example]
        talon.configNominalOutputForward(0.0, Constants.TIMEOUT_MS)
        talon.configNominalOutputReverse(0.0, Constants.TIMEOUT_MS)
        talon.configPeakOutputForward(1.0, Constants.TIMEOUT_MS)
        talon.configPeakOutputReverse(-1.0, Constants.TIMEOUT_MS)
        talon.inverted = inverse
        configurePidValues(talon, pidSetting)
        return talon
    }

    private fun configurePidValues(talon: WPI_TalonSRX?, pidSetting: PIDSetting)
    {
        // Config the Velocity closed loop gains in slot0
        talon!!.config_kP(Constants.PID_LOOP_INDEX, pidSetting.p, Constants.TIMEOUT_MS)
        talon.config_kI(Constants.PID_LOOP_INDEX, pidSetting.i, Constants.TIMEOUT_MS)
        talon.config_kD(Constants.PID_LOOP_INDEX, pidSetting.d, Constants.TIMEOUT_MS)
        talon.config_kF(Constants.PID_LOOP_INDEX, pidSetting.f, Constants.TIMEOUT_MS)
    }

    override fun initDefaultCommandImpl()
    {
        defaultCommand = object : AbstractPollingCommand()
        {
            override fun doAction()
            {
                upperVelocityReading = upperMotor!!.selectedSensorVelocity
                upperCalcRpm = upperVelocityReading * SensorVelocity_READING_TO_RPM_FACTOR
                maxUpperCalcRpm = max(upperCalcRpm, maxUpperCalcRpm)
                lowerVelocityReading = lowerMotor!!.selectedSensorVelocity
                lowerCalcRpm = lowerVelocityReading * SensorVelocity_READING_TO_RPM_FACTOR
                maxLowerCalcRpm = max(lowerCalcRpm, maxLowerCalcRpm)

                if (angleAxis != null)
                {
                    val speed = angleAxis?.get() ?: -1.0
                    val absoluteSpeed = abs(speed)
                    // Create a dead zone
                    if (absoluteSpeed < 0.01)
                    {
                        angleSettingLaconicLogger.debug() { "Setting angle speed to 0.0  (for read value of $speed)" }
                        angleMotor?.set(0.0)
                    }
                    else
                    {
                        val requestedDirection = if (MathUtils.isPositive(speed)) AngleMovementDirection.Raising else AngleMovementDirection.Lowering
                        
                        
                        if (requestedDirection == AngleMovementDirection.Raising)
                        {
                            if (isAtUpperLimit())
                            {
                                angleSettingLaconicLogger.debug() { "Cannot raise shooter angle as it is at its MAXIMUM angle. Setting angle motor speed to zero" }
                                angleMotor?.set(0.0)
                            }
                            else if (isNearUpperLimit())
                            {
                                val constrainedSpeed = MathUtils.constrainAbsoluteMaxValue(speed, ABSOLUTE_SPEED_LIMIT_FOR_NEARING_LIMIT)
                                angleSettingLaconicLogger.debug() { "Nearing UPPER limit using speed of $constrainedSpeed for requested speed of $speed " }
                                angleMotor?.set(constrainedSpeed)
                            }
                            else
                            {
                                angleSettingLaconicLogger.debug() { "Raising angle with speed of $speed" }
                                angleMotor?.set(speed)
                            }
                        }
                        else if (requestedDirection == AngleMovementDirection.Lowering)
                        {
                            if (isAtLowerLimit())
                            {
                                angleSettingLaconicLogger.debug() { "Cannot lower shooter angle as it is at its MINIMUM angle. Setting angle motor speed to zero" }
                                angleMotor?.set(0.0)
                            }
                            else if (isNearLowerLimit())
                            {
                                val constrainedSpeed = MathUtils.constrainAbsoluteMaxValue(speed, ABSOLUTE_SPEED_LIMIT_FOR_NEARING_LIMIT)
                                angleSettingLaconicLogger.debug() { "Nearing LOWER limit using speed of $constrainedSpeed for requested speed of $speed " }
                                angleMotor?.set(constrainedSpeed)
                            }
                            else
                            {
                                angleSettingLaconicLogger.debug() { "Lowering angle with speed of $speed" }
                                angleMotor?.set(speed)
                            }
                        }
                        else
                        {
                            // In theory, we should never get here.... but just in case
                            angleSettingLaconicLogger.debug() { "Fail Safe Clause: Setting angle speed to 0.0  (for read value of $speed)" }
                            angleMotor?.set(0.0)
                        }
                    }
//                    if (abs(speed) > 0.01)
//                    {
//                        val requestedDirection = if (MathUtils.isPositive(speed)) AngleMovementDirection.Raising else AngleMovementDirection.Lowering
//                        val inSafeZone = angleFlagLimitSwitch.isNotActivated
//                                      
//                            
//                        if (inSafeZone)
//                        {
//                            // We don't want to toggle this until we are back in the safe zone so we hold the 
//                            // unsafe direction to move
//                            lastMovementDirection = requestedDirection  
//                        }
//                        
//                        angleMotor?.set(speed)
//                        angleSettingLaconicLogger.debug() { "Setting angle speed to $speed" }
//                        
//                    }
//                    else
//                    {
//                        angleSettingLaconicLogger.debug() {"Setting angle speed to 0.0  (for read value of $speed)"}
//                        angleMotor?.set(0.0)
//                    }
                    
                }
                else
                {
                    angleSettingLaconicLogger.warn("angleControl is null, can't move shooter angle.");
                }
            }

            // Because of init/construction sequence, we can NOT make this a field of the inner class and return the field
            override fun getRequiredSubsystems(): ImmutableSet<I3838Subsystem> = requiredSubsystem

        }
    }
    
    fun isAtUpperLimit(): Boolean = getCurrentShooterAngle() >= MAX_ANGLE //|| angleFlagLimitSwitch.isActivated && lastMovementDirection == AngleMovementDirection.Raising
    fun isAtLowerLimit(): Boolean = getCurrentShooterAngle() <= MIN_ANGLE //|| angleFlagLimitSwitch.isActivated && lastMovementDirection == AngleMovementDirection.Lowering
    fun isNearUpperLimit(): Boolean = getCurrentShooterAngle() >= MAX_ANGLE - NEAR_ANGLE_LIMIT_THRESHOLD
    fun isNearLowerLimit(): Boolean = getCurrentShooterAngle() <= MIN_ANGLE + NEAR_ANGLE_LIMIT_THRESHOLD
    
    fun increaseAngle()
    {
        angleMotor!!.set(ANGLE_MOTOR_SPEED)
    }

    fun decreaseAngle()
    {
        angleMotor!!.set(-ANGLE_MOTOR_SPEED)
    }

    fun stopAngleMotor()
    {
        angleMotor!!.set(0.0)
    }

    fun startShooter(theTargetRpm: Int)
    {
        val targetVelocity_UnitsPer100ms = theTargetRpm * TARGET_RPM_TO_SensorVelocity_SETTING_FACTOR
        val upperTargetVelocity_UnitsPer100ms = targetVelocity_UnitsPer100ms * upperPercentVariance
        val lowerTargetVelocity_UnitsPer100ms = targetVelocity_UnitsPer100ms * lowerPercentVariance
        upperMotor!![ControlMode.Velocity] = upperTargetVelocity_UnitsPer100ms
        lowerMotor!![ControlMode.Velocity] = lowerTargetVelocity_UnitsPer100ms
    }

    fun setMotorSpeed()
    {
        if (isUsePidLoop)
        {
            logger.debug("setMotorSpeed Running via closed loop using Velocity")
            configurePidValues(upperMotor, upperMotorPidSetting)
            configurePidValues(lowerMotor, lowerMotorPidSetting)
            val targetVelocity_UnitsPer100ms = targetRpms * TARGET_RPM_TO_SensorVelocity_SETTING_FACTOR
            val upperTargetVelocity_UnitsPer100ms = targetVelocity_UnitsPer100ms * upperPercentVariance
            val lowerTargetVelocity_UnitsPer100ms = targetVelocity_UnitsPer100ms * lowerPercentVariance
            upperMotor!![ControlMode.Velocity] = upperTargetVelocity_UnitsPer100ms
            lowerMotor!![ControlMode.Velocity] = lowerTargetVelocity_UnitsPer100ms
        }
        else
        {
            logger.debug("setMotorSpeed running using PercentOutput")
            val upperSpeedSetting = sharedSpeedSetting * upperPercentVariance
            val lowerSpeedSetting = sharedSpeedSetting * lowerPercentVariance
            upperMotor!![ControlMode.PercentOutput] = upperSpeedSetting
            lowerMotor!![ControlMode.PercentOutput] = lowerSpeedSetting
        }
    }

    fun stopMotors()
    {
        upperMotor!!.set(0.0)
        lowerMotor!!.set(0.0)
    }

    fun getCurrentShooterAngle() = BallSensorSubsystem.currentShooterAngle
    fun getCurrentShooterAngleAsString() = MathUtils.formatNumber(BallSensorSubsystem.currentShooterAngle, 1)
    
    
    fun getUpperVelocityReading(): Double
    {
        return upperVelocityReading.toDouble()
    }

    fun getLowerVelocityReading(): Double
    {
        return lowerVelocityReading.toDouble()
    }

    fun setUpperPercentVariance(upperPercentVariance: Double)
    {
        this.upperPercentVariance = Math.abs(upperPercentVariance)
    }

    fun getUpperPercentVariance(): Double
    {
        return upperPercentVariance
    }

    fun setLowerPercentVariance(lowerPercentVariance: Double)
    {
        this.lowerPercentVariance = Math.abs(lowerPercentVariance)
    }

    fun getLowerPercentVariance(): Double
    {
        return lowerPercentVariance
    }

    val isLowerMotorInverted: Boolean
        get() = lowerMotor != null && lowerMotor!!.inverted

    val isUpperMotorInverted: Boolean
        get() = upperMotor != null && upperMotor!!.inverted

    
    
    fun toggleRingLight()
    {
        if (ringLight?.get() == Relay.Value.kOff)
        {
            turnRingLightOn()
        }
        else
        {
            turnRingLightOff()
        }
    }
    
    fun turnRingLightOff()
    {
        logger.debug { "Turning RingLight OFF" }
        ringLight?.set(Relay.Value.kOff)
    }
    
    fun turnRingLightOn()
    {
        logger.debug { "Turning RingLight ON" }
        ringLight?.set(Relay.Value.kOn)
    }

    fun addAllShooterCommands()
    {
        tab.add(ShooterCallSetMotorSpeedCommand())
        tab.add(StopShooterMotorsCommand())
        tab.add(RingLightToggleCommand())
    }
    
}

internal object Constants
{
    /**
     * Which PID slot to pull gains from. Starting 2018, you can choose from 0,1,2 or 3.
     * Only the first two (0,1) are visible in web-based configuration.
     */
    const val SLOT_INDEX = 0

    /**
     * Talon SRX/ Victor SPX will supported multiple (cascaded) PID loops. For
     * now we just want the primary one.
     */
    const val PID_LOOP_INDEX = 0

    /**
     * Set to zero to skip waiting for confirmation, set to nonzero to wait and
     * report to DS if action fails.
     */
    const val TIMEOUT_MS = 30 //    /**
    //     * PID Gains may have to be adjusted based on the responsiveness of control loop.
    //     * kF: 1023 represents output value to Talon at 100%, 7200 represents Velocity units at 100% output
    //     *
    //     * kP   kI   kD   kF          Iz    PeakOut
    //     */
    //    public final static Gains GAINS_VELOCITY = new Gains(0.25, 0.001, 20, 1023.0 / 7200.0, 300, 1.00);
}