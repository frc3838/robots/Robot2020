package frc.team3838.game.subsystems

import com.ctre.phoenix.motorcontrol.NeutralMode
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX
import com.google.common.collect.ImmutableSet
import com.revrobotics.ColorMatch
import com.revrobotics.ColorSensorV3
import edu.wpi.first.wpilibj.DriverStation
import edu.wpi.first.wpilibj.I2C
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab
import edu.wpi.first.wpilibj.util.Color
import frc.team3838.core.commands.AbstractPollingCommand
import frc.team3838.core.config.time.TimeDuration
import frc.team3838.core.dashboard.matchPlayTab
import frc.team3838.core.hardware.createWpiTalonSRX
import frc.team3838.core.logging.LaconicLogger
import frc.team3838.core.subsystems.Abstract3838Subsystem
import frc.team3838.core.subsystems.I3838Subsystem
import frc.team3838.core.utils.MathUtils
import frc.team3838.game.RobotMap
import kotlin.math.abs


/*

TERMS:
    Field Target Color: The color that needs to be read by the field sensor
    Robot Target Color: The color that needs to be read by the robot sensor in order for
                        the field to read the target color
    


NOTES:
    The formal name is the "Control Panel" §3.5.1 CONTROL PANEL
    A control panel spun faster than 60 revolutions per minute may cause field damage.
    SCORING:
        There are two requirements:
            1) Rotation Control: 
                Rotate the control panel at least three times, but no more than 5 time complete revolutions in the same direction
                    - if the control panel is rotated more than 5 complete revolutions, the count resets to 0
                    - the trench light turns on once Stage 2 capacity is reached indicating the control panel is ready for rotational control
            2) Position Control
                Rotate the control panel so a specified color aligns with the sensor for at least five (5) seconds.
                    - Once either ALLIANCE reaches Stage 3 CAPACITY, FMS relays a specified color (randomly selected by FMS and one (1) of 
                      the three (3) colors not currently read by the ALLIANCE’S TRENCH color sensor) to all OPERATOR CONSOLES simultaneously. 
                      The specified color may not be the same for both ALLIANCES
 */

/** 
 * The colors on the wheel. 
 * Do not change the order, as it matches the order as they appear on the wheel in a clockwise  direction. 
 * 
 * ```
 * Wheel color on TOP
 *      9:00 - 10:30   YELLOW
 *     10:30 - 12:00   BLUE
 *     12:00 -  1:30   GREEN
 *      1:30 -  3:00   RED
 *     -----
 *      3:00 -  4:30   YELLOW
 *      4:30 -  6:00   BLUE
 *      6:00 -  7:30   GREEN
 *      7:30 -  9:00   RED
 *      
 * Wheel color on BOTTOM
 *      RED, GREEN, BLUE, YELLOW
 *      
 * Turning
 *      Red     to Yellow   1 CW
 *      Red     to Blue     2 E
 *      Red     to Green    1 CCW
 *      Yellow  to Red      1 CCW
 *      Yellow  to Blue     1 CW
 *      Yellow  to Green    2 E
 *      Blue    to Red      2 E
 *      Blue    to Yellow   1 CCW
 *      Blue    to Green    1 CW
 *      Green   to Red      1 CW
 *      Green   to Yellow   2 E
 *      Green   to Blue     1 CCW
 * ```
 */
enum class WheelColor
{ 
    Yellow, Blue, Green, Red;

    fun getDirectionTo(robotTargetColor: WheelColor?) = if (robotTargetColor == null) WheelDirection.Clockwise else directionMap["$name=${robotTargetColor.name}"] ?: WheelDirection.Clockwise

    
    /**
     * Gets the corresponding target color in either direction. For example the robot target color needed
     * for the provided field target color
     */
    fun getCorrespondingTargetColor() = correspondingColorMap[this] ?: error("Missing corresponding target mapping for $this")
    
    fun getNeighbor(wheelDirection: WheelDirection):  WheelColor
    {
        val maxIndex = values().size - 1
        var index: Int
        if (wheelDirection == WheelDirection.Clockwise)
        {
            index = this.ordinal + 1
            if (index > maxIndex) index = 0
            
        }
        else
        {
            index = this.ordinal -1
            if (index < 0) index = maxIndex
        }
        return values()[index]
    }
    
    companion object
    {
        private val directionMap = mapOf(
                "${Red.name}-${Yellow.name}" to WheelDirection.Clockwise,
                "${Red.name}-${Blue.name}" to WheelDirection.Clockwise, // Either
                "${Red.name}-${Green.name}" to WheelDirection.CounterClockwise,
                "${Yellow.name}-${Red.name}" to WheelDirection.CounterClockwise,
                "${Yellow.name}-${Blue.name}" to WheelDirection.Clockwise,
                "${Yellow.name}-${Green.name}" to WheelDirection.Clockwise, // Either
                "${Blue.name}-${Red.name}" to WheelDirection.Clockwise, // Either
                "${Blue.name}-${Yellow.name}" to WheelDirection.CounterClockwise,
                "${Blue.name}-${Green.name}" to WheelDirection.Clockwise,
                "${Green.name}-${Red.name}" to WheelDirection.Clockwise,
                "${Green.name}-${Yellow.name}" to WheelDirection.Clockwise, // Either
                "${Green.name}-${Blue.name}" to WheelDirection.CounterClockwise)
        
        private val correspondingColorMap = mapOf( 
                Red to  Blue,
                Green to Yellow,
                Blue to Red,
                Yellow to Green)
    }
}

enum class WheelDirection 
{ 
    Clockwise, CounterClockwise;
    
    fun getOpposite(): WheelDirection = if (this == Clockwise) CounterClockwise else Clockwise
}

data class WheelReadingResult(val foundWheelColor: WheelColor?, val rawDetectedColor: Color?, val confidence: Double, val fieldSensorColor: WheelColor? = foundWheelColor?.getCorrespondingTargetColor())

private const val UNKNOWN = "Unknown"

object WheelSpinnerSubsystem : Abstract3838Subsystem()
{
    private var spinnerMotor: WPI_TalonSRX? = null
    
    private val speedSettingLaconicLogger = LaconicLogger(logger, TimeDuration.ofSeconds(2))

    val requiredSubsystem: ImmutableSet<I3838Subsystem> by lazy { ImmutableSet.of<I3838Subsystem>(WheelSpinnerSubsystem) }

    val tab: ShuffleboardTab = Shuffleboard.getTab("Wheel")
    
    const val MAX_SPEED: Double = 0.5
    const val MOD_SPEED: Double = 0.2
    const val MIN_SPEED: Double = 0.1

    var wheelReadingResult : WheelReadingResult = WheelReadingResult(null, null, -1.0)
        //get() = readWheelsCurrentColor()
    

    /**
     * Change the I2C port below to match the connection of your color sensor
     */
    private var i2cPort: I2C.Port? = null

    /**
     * A Rev Color Sensor V3 object is constructed with an I2C port as a
     * parameter. The device will be automatically initialized with default
     * parameters.
     */
    private var colorSensor: ColorSensorV3? = null

    /**
     * A Rev Color Match object is used to register and detect known colors. This can
     * be calibrated ahead of time or during operation.
     *
     * This object uses a simple euclidian distance to estimate the closest match
     * with given confidence range.
     */
    private var colorMatch: ColorMatch? = null
    /**
     * Note: Any example colors should be calibrated as the user needs, these
     * are here as a basic example.
     */
    private var blueTarget: Color? = null
    private var greenTarget: Color? = null
    private var redTarget: Color? = null
    private var yellowTarget: Color? = null

    override fun initSubsystem()
    {
        spinnerMotor = createWpiTalonSRX(RobotMap.CANs.WHEEL_OF_FORTUNE_SPINNER_MOTOR, invert = false, neutralMode = NeutralMode.Brake)

        i2cPort = I2C.Port.kOnboard
        colorSensor = ColorSensorV3(i2cPort)
        colorMatch = ColorMatch()
        
        blueTarget = ColorMatch.makeColor(0.143, 0.427, 0.429)
        greenTarget = ColorMatch.makeColor(0.197, 0.561, 0.240)
        // redTarget = ColorMatch.makeColor(0.561, 0.232, 0.114); // Original
        redTarget = ColorMatch.makeColor(0.432, 0.385, 0.182)
        // yellowTarget = ColorMatch.makeColor(0.361, 0.524, 0.113); // Original
        yellowTarget = ColorMatch.makeColor(0.313, 0.564, 0.121)

        colorMatch?.addColorMatch(blueTarget)
        colorMatch?.addColorMatch(greenTarget)
        colorMatch?.addColorMatch(redTarget)
        colorMatch?.addColorMatch(yellowTarget)
        
        
        tab.addNumber("RGB: Red") { wheelReadingResult.rawDetectedColor?.red ?: -1.0 }
        tab.addNumber("RGB: Green") { wheelReadingResult.rawDetectedColor?.green ?: -1.0 }
        tab.addNumber("RGB: Blue") { wheelReadingResult.rawDetectedColor?.blue ?: -1.0 }
        tab.addNumber("Confidence") { wheelReadingResult.confidence }
        tab.addString("Robot Color") { wheelReadingResult.foundWheelColor?.name ?: UNKNOWN }
        matchPlayTab.addString("Robot Color") { wheelReadingResult.foundWheelColor?.name ?: UNKNOWN }

        tab.addString("Field Color") { wheelReadingResult.fieldSensorColor?.name ?: UNKNOWN }
        matchPlayTab.addString("Field Detected Color") { wheelReadingResult.fieldSensorColor?.name ?: UNKNOWN }
    }

    override fun initDefaultCommandImpl()
    {
        defaultCommand = object : AbstractPollingCommand()
        {
            override fun initializeImpl()
            {
                super.initializeImpl()
                WheelSpinnerSubsystem
            }
            override fun doAction()
            {
                wheelReadingResult = readWheelsCurrentColor()
            }

            override fun getRequiredSubsystems(): ImmutableSet<I3838Subsystem> = requiredSubsystem

        }
    }

    fun readWheelsCurrentColor(): WheelReadingResult
    {
        /*
         * The method GetColor() returns a normalized color value from the sensor and can be
         * useful if outputting the color to an RGB LED or similar. To
         * read the raw color, use GetRawColor().
         * 
         * The color sensor works best when within a few inches from an object in
         * well lit conditions (the built in LED is a big help here!). The farther
         * an object is the more light from the surroundings will bleed into the 
         * measurements and make it difficult to accurately determine its color.
         */
            /*
         * The method GetColor() returns a normalized color value from the sensor and can be
         * useful if outputting the color to an RGB LED or similar. To
         * read the raw color, use GetRawColor().
         * 
         * The color sensor works best when within a few inches from an object in
         * well lit conditions (the built in LED is a big help here!). The farther
         * an object is the more light from the surroundings will bleed into the 
         * measurements and make it difficult to accurately determine its color.
         */
        val detectedColor = colorSensor?.color
        
        
         // Run the color match algorithm on our detected color
        
        val match = colorMatch?.matchClosestColor(detectedColor)

        val confidence = match?.confidence ?: -1.0
        val foundColor: WheelColor? = when
        {
            match?.color === blueTarget   -> WheelColor.Blue
            match?.color === redTarget    -> WheelColor.Red
            match?.color === greenTarget &&  confidence > 0.93 -> WheelColor.Green
            match?.color === yellowTarget -> WheelColor.Yellow
            else                         -> null
        }
     
        return WheelReadingResult(foundColor, detectedColor, confidence)
    }

    fun readTargetColorFromFMS(): WheelColor?
    {
        val gameData: String? = DriverStation.getInstance().gameSpecificMessage
        if (gameData.isNullOrBlank())
        {
            return null
        }
        val color: WheelColor? = when (gameData.toCharArray()[0])
        {
            'R'  -> WheelColor.Red
            'G'  -> WheelColor.Green
            'B'  -> WheelColor.Blue
            'Y'  -> WheelColor.Yellow
            else -> null
        }
        logger.debug { "For game data '$gameData' found color $color" }
        return color
    }

    fun startSpinnerMotor(direction: WheelDirection, absoluteSpeed: Double = MAX_SPEED)
    {
        var speed = MathUtils.constrainValue(abs(absoluteSpeed), MIN_SPEED, MAX_SPEED)
        if (direction == WheelDirection.Clockwise) speed = -speed
        speedSettingLaconicLogger.debug(){ "Setting wheel speed to $speed for $direction direction and requested speed of $absoluteSpeed"}
        spinnerMotor?.set(speed)
    }

    fun startSpinnerMotorClockWise(absoluteSpeed: Double = MAX_SPEED) = startSpinnerMotor(WheelDirection.Clockwise, absoluteSpeed)
    fun startSpinnerMotorCounterClockWise(absoluteSpeed: Double = MAX_SPEED) = startSpinnerMotor(WheelDirection.CounterClockwise, absoluteSpeed)
    


    fun stopSpinnerMotor()
    {
        spinnerMotor?.set(0.0)
    }
}
