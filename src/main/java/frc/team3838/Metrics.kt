package frc.team3838


import com.codahale.metrics.Histogram
import com.codahale.metrics.MetricRegistry
import com.codahale.metrics.Timer
import java.awt.Robot

val metrics = MetricRegistry()
val writeTimer: Timer = metrics.timer(MetricRegistry.name(Robot::class.java, "write"))
val readTokenTimer: Timer = metrics.timer(MetricRegistry.name(Robot::class.java, "readToken"))
val serialRead: Timer = metrics.timer(MetricRegistry.name(Robot::class.java, "serialRead"))
val serialRead2: Timer = metrics.timer(MetricRegistry.name(Robot::class.java, "serialRead2"))
val processTokenTimer: Timer = metrics.timer(MetricRegistry.name(Robot::class.java, "processToken"))
val loopCountHistogram: Histogram = metrics.histogram(MetricRegistry.name(Robot::class.java, "loopCountHistogram"))