package frc.team3838.core.pid

import edu.wpi.first.wpilibj.shuffleboard.BuiltInLayouts
import edu.wpi.first.wpilibj.shuffleboard.BuiltInWidgets
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardLayout
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab
import edu.wpi.first.wpilibj.shuffleboard.SimpleWidget
import edu.wpi.first.wpilibj.smartdashboard.SendableBuilder.BooleanConsumer
import frc.team3838.core.dashboard.addBooleanConsumer
import frc.team3838.core.dashboard.addDoubleConsumer
import frc.team3838.core.meta.API
import frc.team3838.core.utils.PIDDash
import frc.team3838.core.utils.PIDSetting
import java.util.function.DoubleConsumer

@API
class PIDTuner @JvmOverloads constructor(val pidSetting: PIDSetting,
                                         enabledConsumer: BooleanConsumer? = null,
                                         pConsumer: DoubleConsumer? = DoubleConsumer { value -> pidSetting.p = value },
                                         iConsumer: DoubleConsumer? = DoubleConsumer { value -> pidSetting.i = value },
                                         dConsumer: DoubleConsumer? = DoubleConsumer { value -> pidSetting.d = value },
                                         fConsumer: DoubleConsumer? = DoubleConsumer { value -> pidSetting.f = value }) //: Sendable
{
    companion object
    {
        const val DEFAULT_TAB_TITLE = "PID Tuning"
    }
    
    val pidDash: PIDDash = addToTab(pidSetting.name, enabledConsumer, pConsumer, iConsumer, dConsumer, fConsumer)


    private fun addToTab(layoutTitle: String,
                 enabledConsumer: BooleanConsumer? = null,
                 pConsumer: DoubleConsumer? = null,
                 iConsumer: DoubleConsumer? = null,
                 dConsumer: DoubleConsumer? = null,
                 fConsumer: DoubleConsumer? = null
                ): PIDDash
    {

        val pidTab: ShuffleboardTab = Shuffleboard.getTab(DEFAULT_TAB_TITLE)
        val pidLayout = pidTab.getLayout(layoutTitle, BuiltInLayouts.kList)
            .withSize(2, 5)
            .withPosition(0, 0)
            .withProperties(mapOf<String, Any>("Label Position" to "LEFT")) // options include "HIDDEN"
        
        val p = addWidget(pidLayout, "P", 0.00, 0, 0)

        val i = addWidget(pidLayout, "I", 0.00, 0, 1)

        val d = addWidget(pidLayout, "D", 0.00, 0, 2)

        val f = addWidget(pidLayout, "F", 0.00, 0, 3)
        
        val enabledWidget = pidLayout.add("Use PID control", false)
            .withWidget(BuiltInWidgets.kToggleSwitch)
            .withPosition(0, 4)

        p.addDoubleConsumer(pConsumer)
        i.addDoubleConsumer(iConsumer)
        d.addDoubleConsumer(dConsumer)
        f.addDoubleConsumer(fConsumer)
        enabledWidget.addBooleanConsumer(enabledConsumer)

        return PIDDash(pidTab, pidLayout, p, i, d, f, enabledWidget)
    }

    @Suppress("SameParameterValue")
    private fun addWidget(pidLayout: ShuffleboardLayout, title: String, defaultValue: Any, columnIndex: Int, rowIndex: Int): SimpleWidget
    {
        return pidLayout.add(title, defaultValue)
            .withWidget(BuiltInWidgets.kTextView)
            .withSize(2, 1)
            .withPosition(columnIndex, rowIndex)
    }
}
