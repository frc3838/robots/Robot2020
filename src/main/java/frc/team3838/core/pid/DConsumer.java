package frc.team3838.core.pid;


import com.ctre.phoenix.motorcontrol.IMotorController;

import frc.team3838.core.meta.API;



@API
public class DConsumer extends MotorControllerPidConsumer
{
    @API
    public DConsumer(IMotorController motorController) 
    {
        super(motorController);
    }
    
    
    @API
    public DConsumer(IMotorController motorController, int timeoutMs)
    {
        super(motorController, timeoutMs);
    }
    
    
    @API
    public DConsumer(IMotorController motorController, int slotIdx, int timeoutMs)
    {
        super(motorController, slotIdx, timeoutMs);
    }
    
    
    @Override
    public void accept(double value)
    {
        motorController.config_kD(slotIndex, value, timeoutMs);
    }
}
