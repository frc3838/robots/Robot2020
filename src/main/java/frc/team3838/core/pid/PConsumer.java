package frc.team3838.core.pid;


import com.ctre.phoenix.motorcontrol.IMotorController;

import frc.team3838.core.meta.API;



@API
public class PConsumer extends MotorControllerPidConsumer
{
    @API
    public PConsumer(IMotorController motorController) 
    {
        super(motorController);
    }
    
    
    @API
    public PConsumer(IMotorController motorController, int timeoutMs)
    {
        super(motorController, timeoutMs);
    }
    
    
    @API
    public PConsumer(IMotorController motorController, int slotIdx, int timeoutMs)
    {
        super(motorController, slotIdx, timeoutMs);
    }
    
    
    @Override
    public void accept(double value)
    {
        motorController.config_kP(slotIndex, value, timeoutMs);
    }
}
