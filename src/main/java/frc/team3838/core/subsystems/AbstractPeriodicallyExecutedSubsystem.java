package frc.team3838.core.subsystems;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import frc.team3838.core.config.time.TimeDuration;



/**
 * While it is generally preferred to properly implement a Subsystem and corresponding Commands to
 * use the subsystem, this class provides an alternative. It allows for a more 'procedural' 
 * implementation of a subsystem, such as that in the iterative robot, that less experienced developers 
 * might be more familiar with. 
 */
public abstract class AbstractPeriodicallyExecutedSubsystem extends Abstract3838Subsystem
{
    protected final Logger logger = LoggerFactory.getLogger(getClass());
    
    
    /**
     * The frequency at which the {@link #periodicExecute()} method is called. This
     * should never be less than 5 milliseconds to prevent CPU pegging, and in most 
     * cases no more frequent that 50 milliseconds.
     * @return TimeDuration representing the frequency at which the {@link #periodicExecute()} method is called.
     */
    public TimeDuration getPeriodicExecutionTimeDuration() { return TimeDuration.ofMillis(50);}    

    /**
     * The core/primary method that will be periodically executed (i.e. called)
     * by the thread executor in the main robot class. This method must be 
     * implmented by its subclasses.
     * The frequency at which this method is called is determined by the return 
     * value of the {@link #getPeriodicExecutionTimeDuration()}. In most cases, the 
     * default value of 50 milliseconds is sufficient.
     */
    public abstract void periodicExecute();
}
