package frc.team3838.core.subsystems

import edu.wpi.first.wpilibj.command.Subsystem
import frc.team3838.core.meta.API
import mu.KotlinLogging
import java.util.concurrent.TimeUnit

abstract class Abstract3838Subsystem : Subsystem, I3838Subsystem
{
    protected val logger = KotlinLogging.logger {}
    protected var enabled = Subsystems.checkIfEnabledInRobotMap(javaClass)
    private var hasBeenInitialized = false

    companion object
    {
        private val LOG = KotlinLogging.logger { Abstract3838Subsystem::class.java}

        init
        {
            if (!Subsystems.hasInitAllBeenCalled())
            {
                val msg = "Subsystems.initAllSubsystems() must be called before accessing/using any subsystems. It should typically be called in Robot.robotInit()"
                LOG.error("")
                LOG.error("FATAL ERROR: $msg")
                LOG.error("")
                // We want to let the logging framework flush since throwing this will result in an ExceptionInInitializerError
                // which typically dumps immediately to the console. This can cause the log message and console output to get
                // intermixed causing confusing output.
                try
                {
                    TimeUnit.MILLISECONDS.sleep(700)
                }
                catch (ignore: InterruptedException)
                {
                }
                throw IllegalStateException(msg)
            }
        }
    }

    @API
    protected constructor(name: String?) : super(name)

    @API
    protected constructor() : super()

    @Throws(Exception::class)
    protected abstract fun initDefaultCommandImpl()

    final override fun initializeTheSubsystem()
    {
        if (!hasBeenInitialized)
        {
            hasBeenInitialized = true
            val clazz: Class<out Abstract3838Subsystem?> = javaClass
            enabled = Subsystems.checkIfEnabled(clazz)
            logger.debug{"In Abstract3838Subsystem.initializeTheSubsystem(), when checking if '${clazz.simpleName}' is enabled, Subsystems.checkIfEnabled(clazz) returned $enabled"}
            if (enabled)
            {
                try
                {
                    LOG.info(){"Initializing '$name' by calling its initSubsystem() method"}
                    initSubsystem()
                }
                catch (e: Exception)
                {
                    LOG.warn(e){"An exception occurred when initializing $name. It will be DISABLED. Cause summary: $e"}
                    disableSubsystem()
                }
            }
            else
            {
                LOG.warn(){"$name is set as disabled, and will not be fully initialized."}
            }
        }
    }

    override fun initDefaultCommand()
    {
        if (isEnabled)
        {
            try
            {
                initDefaultCommandImpl()
            }
            catch (e: Exception)
            {
                logger.warn(e) {"Unable to initialize default command for $name. Cause Summary: $e"}
            }
        }
    }

    override fun isEnabled(): Boolean = enabled

    override fun disableSubsystem()
    {
        LOG.warn() {"$name is being disabled."}
        enabled = false
    }

    /**
     * Called in the constructor if the implmenting subsystem is enabled.
     * If a subsystem does not have any initialization work to do during construction, simple create
     * a no op (no operation) implementation (i.e. an empty method). Implementations can throw
     * Exceptions as the the caller in the super class, [Abstract3838Subsystem] will catch the
     * Exception, log/report it, and then set the subsystem to disabled.
     *
     * @throws Exception if any issues occur during initialization. The super constructor
     * in [Abstract3838Subsystem] will catch the Exception and then set the subsystem to
     * disabled.
     */
    @Throws(Exception::class)
    protected abstract fun initSubsystem()
    
}