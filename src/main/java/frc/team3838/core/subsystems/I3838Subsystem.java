package frc.team3838.core.subsystems;

public interface I3838Subsystem
{
    boolean isEnabled();

    void disableSubsystem();

    // Actually implemented in the WPI Subsystem class, but we want to be able to use in other contexts
    String getName();

    void initializeTheSubsystem();
}
