package frc.team3838.core.subsystems;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.wpi.first.wpilibj.Compressor;
import frc.team3838.core.meta.API;



/**
 * A base for a Compressor Subsystem. In subclasses:
 * <ol>
 * <li>When overriding {@link #initSubsystem()}, either call its super</li>
 * <li>
 * Compressor subsystems drive <em>Solenoids</em>. Solenoids are electronically controlled valves
 * that operate the Pneumatic Cylinders (which may colloquially be called 'pistons').  
 * To create solenoid objects, use either the {@link edu.wpi.first.wpilibj.Solenoid}  or the
 * {@link edu.wpi.first.wpilibj.DoubleSolenoid} class. Most often, we use the {@code DoubleSolenoid} 
 * class so we can control the forward and reverse motion. The constructors take PCM channel ID(s). 
 * You should also supply the 'moduleNumber'. You can use the {@link #getPcmCanId()} to obtain this
 * value.
 * <br/><br/>
 * 
 * Here is an example of creating and using a pair of DoubleSolenoids that are are used simultaneously
 * to control a claw.
 * 
 * </li>
 * </ol>
 *
 *
 * Create solenoids example:
 * <pre>
 * private DoubleSolenoid rightSolenoid;
 * private DoubleSolenoid leftSolenoid;
 * .  .  .
 * &#64;Override // only called if we are enabled
 * protected void initSubsystem() throws Exception
 * {
 *     super.initSubsystem();
 *     rightSolenoid = new DoubleSolenoid(getPcmCanId(), PCMs.rightSolenoidForwardChannel, PCMs.rightSolenoidReverseChannel);
 *     rightSolenoid.set(Value.kOff);
 *     leftSolenoid = new DoubleSolenoid(getPcmCanId(), PCMs.leftSolenoidForwardChannel, PCMs.leftSolenoidReverseChannel);
 *     leftSolenoid.set(Value.kOff);
 * }
 * </pre>
 * Then use code such as the following:
 * <pre>
 * private static final DoubleSolenoid.Value openValue = Value.kForward;
 * private static final DoubleSolenoid.Value closeValue = Value.kReverse;
 *
 * public void openClaw()
 * {
 *     rightSolenoid.set(openValue);
 *     leftSolenoid.set(openValue);
 *     clawStatus = "OPENED";
 *     isOpen = true;
 *     sdbPutStatus();
 * }
 *
 * public void closeClaw()
 * {
 *     rightSolenoid.set(closeValue);
 *     leftSolenoid.set(closeValue);
 *     clawStatus = "CLOSED";
 *     isOpen = false;
 *     sdbPutStatus();
 * }
 *
 * public void stopClaw()
 * {
 *     rightSolenoid.set(Value.kOff);
 *     leftSolenoid.set(Value.kOff);
 *     clawStatus = "OFF";
 *     sdbPutStatus();
 * }
 * </pre>
 */
@API
public abstract class Abstract3838CompressorSubsystem extends Abstract3838Subsystem
{
    private static final Logger logger = LoggerFactory.getLogger(Abstract3838CompressorSubsystem.class);
    
    @SuppressWarnings("WeakerAccess")
    protected Compressor compressor;
    
    
    @API
    protected Abstract3838CompressorSubsystem(String name)
    {
        // The super constructor checks if the subsystem is
        // enabled. If so, it calls initSubsystem()
        super(name);
    }
    
    
    @API
    protected Abstract3838CompressorSubsystem()
    {
        // The super constructor checks if the subsystem is
        // enabled. If so, it calls initSubsystem()
        super();
    }
    
    
    /**
     * <p>
     * <strong style="color:red">When overriding, call this via {@code super.initSubsystem()} first.</strong> 
     * Initialize any Solenoid or DoubleSolenoid fields here. See the class level documentation
     * for examples of creating and using Solenoid and DoubleSolenoid objects. 
     * </p>
     *
     * {@inheritDoc}
     *
     * @throws Exception
     */
    @Override // only called if we are enabled
    protected void initSubsystem() throws Exception
    {
        initCompressor();
    }
    
    
    @SuppressWarnings("WeakerAccess")
    protected void initCompressor()
    {
        compressor = new Compressor(getPcmCanId());
        logger.info("Compressor has been initialized");
    }
    
    
    /**
     * Stop the compressor from running in closed loop control mode.
     * Use the method in cases where you would like to manually stop and start the compressor
     * for applications such as conserving battery or making sure that the compressor motor
     * doesn't start during critical operations.
     */
    @API
    public void stopCompressor()
    {
        compressor.stop();
    }
    
    
    /**
     * Start the compressor running in closed loop control mode
     * Use the method in cases where you would like to manually stop and start the compressor
     * for applications such as conserving battery or making sure that the compressor motor
     * doesn't start during critical operations.
     */
    @API
    public void startCompressor()
    {
        compressor.start();
    }
    
    
    /**
     * The CAN ID the Pneumatic Control Module (PCM) -- i.e. the compressor --  compressor is
     * assigned via the Cross the Road Electronics (CTRE) 'Phoenix' tool.
     * That CAN ID should be set to a constant in the RobotMap (so that all CAN IDs can be
     * referenced and managed in a single location). This method then simply returns that constant.
     *
     * @return CAN ID the PCM is assigned
     */
    @API
    public abstract int getPcmCanId();
}
