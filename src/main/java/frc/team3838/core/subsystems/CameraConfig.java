package frc.team3838.core.subsystems;

import javax.annotation.Nullable;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import frc.team3838.core.meta.API;



public class CameraConfig
{
    int deviceNumber;
    @Nullable
    String name;


    @API
    public CameraConfig(int deviceNumber)
    {
        this.deviceNumber = deviceNumber;
    }


    public CameraConfig(int deviceNumber, @Nullable String name)
    {
        this.deviceNumber = deviceNumber;
        this.name = name;
    }


    public int getDeviceNumber()
    {
        return deviceNumber;
    }


    @Nullable
    public String getName()
    {
        return name;
    }


    @Override
    public String toString()
    {
        return new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE)
            .append("deviceNumber", deviceNumber)
            .append("name", name)
            .toString();
    }
}
