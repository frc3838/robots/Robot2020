package frc.team3838.core.subsystems;

import javax.annotation.Nonnull;

import frc.team3838.core.RobotProperties;



public class NavxSubsystem extends Abstract3838NavxSubsystem
{

    /**
     * This is a private constructor to ensure a single instance (i.e. singleton pattern).
     * Use the static {@link #getInstance()} method to obtain a reference to the subsystem.
     * For example, instead of doing this:
     * <pre>
     *     ${NAME} subsystem = new ${NAME}();
     * </pre>
     * do this:
     * <pre>
     *     ${NAME} subsystem = ${NAME}.getInstance();
     * </pre>
     */
    private NavxSubsystem()
    {
        //The super constructor checks if the subsystem is enabled. If so, it calls initSubsystem();
        super(RobotProperties.getNavxAdjustmentAngle());
    }


    @Override
    protected void initSubsystem() throws Exception
    {
        // The initSubsystem() method is called by the super class constructor if, and only if, the subsystem is enabled
        // The super constructor will safely handle this init method throwing an exception by disabling the subsystem

    }


    @Override
    protected void initDefaultCommandImpl() throws Exception
    {
        /* Initialize the default command, if any, here.    */
        /* A default command runs without any driver action. */
    }
    
    
    /** The Singleton instance of this NavxSubsystem. External classes should use the {@link #getInstance()} method to get the instance. */
    private static NavxSubsystem INSTANCE;
    

    
    /**
     * Returns the Singleton instance of this NavxSubsystem. This static method
     * should be used by external classes, rather than the constructor
     * to get the instance of this class.
     * <pre>
     *     NavxSubsystem navxSubsystem = new NavxSubsystem();
     * </pre>
     * do this:
     * <pre>
     *     NavxSubsystem navxSubsystem = NavxSubsystem.getInstance();
     * </pre>
     *
     * @return the single instance of this subsystem.
     */
    @Nonnull
    public static NavxSubsystem getInstance()
    {
        // "Double Checked Locking" implementation that provides quick access but with thread safe initialization,
        // and eliminates potential subtle initialization sequence bugs Eager initialization may cause
        // See Method 4 at https://www.geeksforgeeks.org/singleton-design-pattern/ 
        
        // Fast (non-synchronized) check to reduce overhead of acquiring a lock when it's not needed
        if (INSTANCE == null)
        {
            // Make thread safe 
            synchronized (NavxSubsystem.class)
            {
                // Check nullness again as multiple threads can reach above null check
                if (INSTANCE == null)
                {
                    INSTANCE = new NavxSubsystem();
                }
            }
        }
        return INSTANCE;
    }

}

