
package frc.team3838.core;

import java.util.Set;
import java.util.SortedSet;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.collect.ImmutableMap;

import edu.wpi.first.hal.HAL;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.Scheduler;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team3838.core.commands.autonomous.AutonomousDoNothingCommand;
import frc.team3838.core.config.AbstractIOAssignments;
import frc.team3838.core.config.time.TimeDuration;
import frc.team3838.core.logging.LaconicLogger;
import frc.team3838.core.logging.LogbackProgrammaticConfiguration;
import frc.team3838.core.meta.API;
import frc.team3838.core.meta.DoesNotThrowExceptions;
import frc.team3838.core.subsystems.AbstractPeriodicallyExecutedSubsystem;
import frc.team3838.core.subsystems.I3838Subsystem;
import frc.team3838.core.subsystems.Subsystems;
import frc.team3838.core.subsystems.drive.Abstract3838DriveTrainSubsystem;
import frc.team3838.core.utils.MathUtils;
import frc.team3838.core.utils.ReflectionUtils3838;



/**
 * The VM is configured to automatically run this class, and to call the
 * methods corresponding to each mode, as described in the IterativeRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the manifest file in the resource
 * directory.
 */
@SuppressWarnings({"AbstractClassExtendsConcreteClass", "UnnecessaryToStringCall"})
public abstract class Abstract3838Robot extends TimedRobot 
{
    private static final TimeDuration COMMAND_SCHEDULER_RUN_INTERVAL_TIME_DURATION = TimeDuration.ofMillis(5);


    //We have to init the logging framework programmatically before we create our logger
    protected Logger logger;

    private boolean bootCompleteMsgHasBeenLogged = false;

    @Nullable
    private Command autonomousCommand;

    @Nullable
    private ScheduledExecutorService scheduledExecutorService;

//    @Nullable
//    private Watchdog watchdogRef;


    private String robotName = RobotProperties.UNDETERMINED_ROBOT_NAME;
    
    
    @API
    protected Abstract3838Robot()
    {
        super();
//        initWatchDogHandle();
    }
    
    @API
    protected Abstract3838Robot(double period)
    {
        super(period);
//        initWatchDogHandle();
    }
    
    
    @Nullable
    protected String getRobotInitVersionInfo()
    {
        return null;
    }

    /**
     * This method is run when the robot is first started up and should be
     * used for any initialization code.
     */
    @DoesNotThrowExceptions
    @Override
    public void robotInit()
    {
        bootCompleteMsgHasBeenLogged = false;
        try
        {
            final long startTime = System.currentTimeMillis();
            initLogging();

            RobotProperties.initProperties();
            robotName = RobotProperties.getRobotName();

            logNameAndVersionInfo(null);
            // Because commands use subsystems via the `requires()` method, subsystems need
            // to be initialized before commands, and thus Joysticks since the Joystick
            // class creates command instances to assign to buttons.
            initPortAndChannelAssignments();
            initSubsystems();

            try
            {
                // We want to initialize the field by calling getDriveTrainSubsystem()
                Abstract3838DriveTrainSubsystem driveTrainSubsystem = getDriveTrainSubsystem();
                driveTrainSubsystem.feedRobotDriveMotorSafety();
                //An extra measure for absolute safety... a bit of overkill... but ya never know :)
                driveTrainSubsystem.stop();
            }
            catch (Exception e)
            {
                logger.warn("An exception occurred during robotInit() when calling getDriveTrainSubsystem().stop(). Cause Summary: {}", e.toString(), e);
            }

            initJoysticks();
            initAutonomousSelector();
            additionalRobotInitWork();
    
    
            logger.info("");
            long initDuration = System.currentTimeMillis() - startTime;
            logger.info("robotInit() completed successfully in {}ms ({} seconds)",
                        initDuration,
                        MathUtils.format(((double) initDuration) / 1_000.0));
        }
        catch (Throwable e)
        {
            logger.error(">>>FATAL<<< A fatal Exception occurred in the robotInit() method. Cause Summary: {}", e.toString(), e);
        }
    }

//    @DoesNotThrowExceptions
//    private void initWatchDogHandle()
//    {
//        try
//        {
//            final Class<?> iterativeRobotBaseClass = IterativeRobotBase.class;
//            final Field watchdogField = iterativeRobotBaseClass.getDeclaredField("m_watchdog");
//            watchdogField.setAccessible(true);
//            watchdogRef = (Watchdog) watchdogField.get(this);
//        }
//        catch (Throwable e)
//        {
//            logger.warn("Could not initialize WatchDog field handle. Cause Summary: {}", e.toString(), e);
//        }
//    }
//
//    @API
//    @DoesNotThrowExceptions
//    protected void resetWatchDog()
//    {
//        try
//        {
//            if (watchdogRef != null)
//            {
//                watchdogRef.reset();
//            }
//            feedDriveTrainMotorSafety();
//        }
//        catch (Exception e)
//        {
//            logger.debug("Could not reset Watchdog. Cause Summary: {}", e.toString(), e);
//        }
//    }


    @Override
    public void robotPeriodic()
    {
        logger.trace("robotPeriodic called");
    }


    /**
     * This method is called once each time the robot enters Disabled mode.
     * You can use it to reset any subsystem information you want to clear when
     * the robot is disabled.
     */
    @Override
    public void disabledInit()
    {
        logger.info("Robot entering disabled mode");
        stopScheduler();
        if (autonomousCommand != null) {autonomousCommand.cancel();}

        //This call really isn't needed... but we prefer to err on the side of safety
        stopDriveTrainSubsystem();
        feedDriveTrainMotorSafety();
    
        try
        {
            disableInitAdditional();
        }
        catch (Exception e)
        {
            logger.warn("An exception occured in disableInitAdditional. Cause: $e", e);
        }
    
        if (!bootCompleteMsgHasBeenLogged)
        {
            bootCompleteMsgHasBeenLogged = true;
            logger.info("");
            logNameAndVersionInfo(null);
            logger.info("");
            logger.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            logger.info("~ ROBOT BOOT UP COMPLETED ~");
            logger.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            logger.info("");
        }
    }
    
    
    /**
     * This method is called once each time the robot enters Disabled mode.
     * You can use it to reset any subsystem information you want to clear when
     * the robot is disabled.
     */
    public void disableInitAdditional()
    {
        // no op in super
    }

    @Override
    public void disabledPeriodic()
    {
        // We are using our own loop/periodic timer that calls the 
        // scheduler's run method in order have better control of 
        // the timing. See the startScheduler() method.
        // Scheduler.getInstance().run();
        feedDriveTrainMotorSafety();
    }


    /**
     * This autonomous (along with the chooser code above) shows how to select
     * between different autonomous modes using the dashboard. The sendable
     * chooser code works with the Java SmartDashboard. If you prefer the
     * LabVIEW Dashboard, remove all of the chooser code and uncomment the
     * getString code to get the auto name from the text box below the Gyro
     *
     * You can add additional auto modes by adding additional commands to the
     * chooser code above (like the commented example) or additional comparisons
     * to the switch structure below with additional strings & commands.
     */
    @Override
    public void autonomousInit()
    {
        logger.debug("autonomousInit() method called");
        startScheduler(false);

        autonomousCommand = getSelectedAutonomousCommand();
        logger.info("<<< Autonomous mode set to: {}", autonomousCommand.getName() + " >>>");

        //noinspection ConstantConditions
        if ((getDriveTrainSubsystem() != null) && getDriveTrainSubsystem().isEnabled())
        {
            preAutonomousDriveTrainPrep();
        }

        if (autonomousCommand != null)
        {
            logger.info("Running Autonomous command: {}", autonomousCommand.getName());
            autonomousCommand.start();
        }
    }


    /** This method is called periodically during autonomous. */
    @Override
    public void autonomousPeriodic()
    {
        logger.trace("autonomousPeriodic() called");
        feedDriveTrainMotorSafety();
        // We are using our own loop/periodic timer that calls the 
        // scheduler's run method in order have better control of 
        // the timing. See the startScheduler() method.
        // Scheduler.getInstance().run();
    }


    @Override
    public void teleopInit()
    {
        // This makes sure that the autonomous stops running when teleop starts running.
        // If you want the autonomous to continue until interrupted by another command,
        // remove this line or comment it out.
        if (autonomousCommand != null) { autonomousCommand.cancel(); }

        postAutonomousDriveTrainCleanup();
        startScheduler(true);
    }


    /** This method is called periodically during operator control. */
    @Override
    public void teleopPeriodic()
    {
        logger.trace("teleopPeriodic() called");
        // We are using our own loop/periodic timer that calls the 
        // scheduler's run method in order have better control of 
        // the timing. See the startScheduler() method.
        // Scheduler.getInstance().run();
    }


    /** This method is called periodically during test mode. */
    @Override
    public void testPeriodic()
    {
        logger.trace("testPeriodic() called");
    }


    @DoesNotThrowExceptions
    private void initLogging()
    {
        try
        {
            LogbackProgrammaticConfiguration.init();
            logger = LoggerFactory.getLogger(getClass());
            logger.info("Logging Initialization completed. Initializing Robot...");
        }
        catch (Throwable e)
        {
            //Since we can't initialize the logging system here, we have to use system, out to report the issue.
            //noinspection UseOfSystemOutOrSystemErr
            System.err.println(">>>CRITICAL ERROR<<< COULD NOT INITIALIZE THE LOGGING SYSTEM. CAUSE SUMMARY: " + e.toString());
            //noinspection CallToPrintStackTrace
            e.printStackTrace();
        }
    }

    @API
    public String getRobotName()
    {
        return robotName;
    }


    @SuppressWarnings("SameParameterValue")
    protected void logNameAndVersionInfo(@Nullable String prefix)
    {
        StringBuilder stringBuilder = new StringBuilder();
        if (StringUtils.isNotBlank(prefix))
        {
            stringBuilder.append(prefix).append(' ');
        }

        //noinspection CallToSimpleGetterFromWithinClass
        stringBuilder.append(getRobotName()).append(' ');
        final String initMsg = getRobotInitVersionInfo();
        if (StringUtils.isNotBlank(initMsg))
        {
            stringBuilder.append(initMsg);
        }

        stringBuilder.append("   ").append(RobotProperties.getRobotProperties());

        logger.info(stringBuilder.toString());
    }

    @DoesNotThrowExceptions
    private void initAutonomousSelector()
    {
        try
        {
            initAutonomousSelectorImpl();

        }
        catch (Throwable e)
        {
            logger.error("An exception occurred when initializing the autonomous command selector. Cause Summary: {}", e.toString(), e);
        }
    }

    @DoesNotThrowExceptions
    @Nonnull
    private Command getSelectedAutonomousCommand()
    {
        try
        {
            final Command autonomousCommand = getSelectedAutonomousCommandImpl();
            if (autonomousCommand == null)
            {
                logger.warn("The autonomous command was not determined/initialized. No autonomous mode will run.");
                return AutonomousDoNothingCommand.INSTANCE;
            }
            else
            {
                return autonomousCommand;
            }
        }
        catch (Throwable e)
        {
            logger.error("An exception occurred when determining/setting the autonomous command to use. "
                         + "No autonomous mode will run. Cause Summary: {}", e.toString(), e);
            return AutonomousDoNothingCommand.INSTANCE;
        }
    }


    @SuppressWarnings("MethodMayBeStatic")
    protected void initPortAndChannelAssignments()
    {
        AbstractIOAssignments.validateAllPortAndChannelAssignments();
    }


    protected void initSubsystems()
    {
        final ImmutableMap<Class<? extends I3838Subsystem>, Boolean> enabledSubsystemsMap = getEnabledSubsystemsMap();
        logger.info("Enabled Subsystems Map = '{}'", enabledSubsystemsMap);
        final SortedSet<Class<? extends Subsystem>> invalidSubsystems = Subsystems.initAllSubsystems(enabledSubsystemsMap);
        if (!invalidSubsystems.isEmpty())
        {
            throw new IllegalStateException("Could not initialize all Subsystems. See logs for details. "
                                            + "Problematic subsystems: " + invalidSubsystems);
        }
    }


    @DoesNotThrowExceptions
    protected void preAutonomousDriveTrainPrep()
    {
        try
        {
            getDriveTrainSubsystem().disableMotorSafetyForAutonomousMode();
        }
        catch (Exception e)
        {
            logger.warn("An exception occurred during preAutonomousDriveTrainPrep(). Cause Summary: {}", e.toString(), e);
        }
    }


    @DoesNotThrowExceptions
    protected void postAutonomousDriveTrainCleanup()
    {
        try
        {
            final Abstract3838DriveTrainSubsystem driveTrainSubsystem = getDriveTrainSubsystem();
            driveTrainSubsystem.stop();
            feedDriveTrainMotorSafety();
            driveTrainSubsystem.enableMotorSafetyPostAutonomousMode();
            driveTrainSubsystem.stop();
        }
        catch (Exception e)
        {
            logger.warn("An exception occurred during postAutonomousDriveTrainCleanup(). Cause Summary: {}", e.toString(), e);
        }
    }


    @DoesNotThrowExceptions
    protected void stopDriveTrainSubsystem()
    {
        // For stopping, we don't check if the system is enabled. We want to err on the side
        // of caution in case the enabled boolean is out of sync and thus stop the robot..
        // Runaway robots are a bad thing :)
        try
        {
            getDriveTrainSubsystem().stop();
        }
        catch (Throwable e)
        {
            logger.error("An exception was thrown when stopping the DriveTrainSubsystem. Cause Summary: {}", e.toString(), e);
        }
    }


    // @formatter:off
    @Nullable
    private Abstract3838DriveTrainSubsystem dtDoNotAccessDirectlyUseGetDriveTrainSubsystem;
    @Nonnull
    protected Abstract3838DriveTrainSubsystem getDriveTrainSubsystem()
    {
        // @formatter:on
        // We use the "Double Checked Locking" method, See "Method 4" at https://www.geeksforgeeks.org/singleton-design-pattern/ and 
        // at wikipedia https://en.wikipedia.org/wiki/Double-checked_locking 
        
        if (dtDoNotAccessDirectlyUseGetDriveTrainSubsystem == null)
        {
            // To make thread safe 
    
            synchronized (Abstract3838Robot.class)
            {
                // check for null again as multiple threads can reach above check 
                if (dtDoNotAccessDirectlyUseGetDriveTrainSubsystem == null)
                {
                    final Set<Class<? extends Abstract3838DriveTrainSubsystem>> implementations = ReflectionUtils3838.findImplementations(Abstract3838DriveTrainSubsystem.class);
                    if (implementations.isEmpty())
                    {
                        throw new IllegalStateException("Could not find an implementation of Abstract3838DriveTrainSubsystem. The Robot must have a "
                                                        + "DriveTrainSubsystem that extends the Abstract3838DriveTrainSubsystem class. For "
                                                        + "early development, consider using the NoOpDriveTrainSubsystem (by listing it in the "
                                                        + "list of subsystems or overriding the getDriveTrainSubsystem() in the Robot class)"
                                                        + " or creating a skeleton implementation and not enabling it.");
                    }
                    else if (implementations.size() > 1)
                    {
                        throw new IllegalStateException("Found multiple implementations of 'Abstract3838DriveTrainSubsystem'. You either need to "
                                                        + "have only a single instance, or override the 'getDriveTrainSubsystem()' method in "
                                                        + "your implementation of " + getClass().getSimpleName());
                    }
                    else
                    {
                        final Class<? extends Abstract3838DriveTrainSubsystem> impl = implementations.iterator().next();
                        dtDoNotAccessDirectlyUseGetDriveTrainSubsystem = Subsystems.getInstanceOf(impl);
                    }
        
                    //This ultimately should not happen, but just in case we check, mostly to make the non null return inspection in the IDE happy
                    if (dtDoNotAccessDirectlyUseGetDriveTrainSubsystem == null)
                    {
                        throw new IllegalStateException("FINAL CHECK: " +
                                                        "Could not find an implementation of Abstract3838DriveTrainSubsystem. The Robot must have a "
                                                        + "DriveTrainSubsystem that extends the Abstract3838DriveTrainSubsystem class. For "
                                                        + "early development, consider using the NoOpDriveTrainSubsystem (by listing it in the "
                                                        + "list of subsystems or overriding the getDriveTrainSubsystem() in the Robot class)"
                                                        + " or creating a skeleton implementation and not enabling it.");
                    }
                }
            }
    
        }
        return dtDoNotAccessDirectlyUseGetDriveTrainSubsystem;
    }


    protected abstract void initAutonomousSelectorImpl() throws Exception;

    @Nullable
    protected abstract Command getSelectedAutonomousCommandImpl() throws Exception;


    protected abstract void initJoysticks();

    /**
     * Called at the end of robotInit() to allow subclasses to do any
     * additional game year specific initialization work.
     */
    protected abstract void additionalRobotInitWork();


    /**
     * Gets a map of Subsystems that should be created and whether those should be enabled.
     * @return a map of Subsystems that should be created and whether those should be enabled
     */
    @Nonnull
    protected abstract  ImmutableMap<Class<? extends I3838Subsystem>, Boolean> getEnabledSubsystemsMap();


    @Override
    public final void startCompetition()
    {
        // If we do not catch an exception here, we can get into a boot/startup loop
        try
        {
            super.startCompetition();
        }
        catch (Throwable t)
        {
            logger.error("An exception occurred in the startCompetition() method. Cause Summary: {}", t.toString(), t);
        }
    }


    private void startScheduler(boolean isInTeleopMode)
    {
        final long startTime = System.currentTimeMillis();
        logger.info("startScheduler() called");
        if (scheduledExecutorService == null)
        {
            scheduledExecutorService = Executors.newScheduledThreadPool(6);
            scheduledExecutorService.scheduleAtFixedRate(() ->
                                                         {
                                                             // Schedule the Robot command scheduler
                                                             try
                                                             {
                                                                 logger.trace("Calling Scheduler.getInstance().run()");
                                                                 Scheduler.getInstance().run();
                                                             }
                                                             catch (Exception ex)
                                                             {
                                                                 logger.error(
                                                                     "CRITICAL ERROR: Scheduler.getInstance().run() threw an exception. Cause Summary: {}",
                                                                     ex.toString(),
                                                                     ex);
                                                             }

                                                         }, 0, COMMAND_SCHEDULER_RUN_INTERVAL_TIME_DURATION.toNanos(), TimeUnit.NANOSECONDS);

            if (isInTeleopMode)
            {
                //Just an extra safety insurance
                stopDriveTrainSubsystem();

                //Schedule the Drive command
                if (getDriveTrainSubsystem().isEnabled())
                {
                    scheduledExecutorService.scheduleAtFixedRate(() ->
                                                                 {
                                                                     try
                                                                     {
                                                                         logger.trace("Calling getDriveTrainSubsystem().drive()");
                                                                         getDriveTrainSubsystem().drive();
                                                                     }
                                                                     catch (Exception ex)
                                                                     {
                                                                         logger.error(
                                                                             "CRITICAL ERROR: DriveTrainSubsystem.getInstance().drive() threw an exception. Cause Summary: {}",
                                                                             ex.toString(),
                                                                             ex);
                                                                     }

                                                                 }, 0, 5, TimeUnit.MILLISECONDS);
                }
                else
                {
                    logger.warn("getDriveTrainSubsystem().isEnabled() returned false. Drive Train Scheduler Thread will not be created.");
                }
    
                for (AbstractPeriodicallyExecutedSubsystem subsystem : Subsystems.getPeriodicallyExecutedSubsystems())
                {
                    try
                    {
                        if ((subsystem != null) && subsystem.isEnabled())
                        {
                            scheduledExecutorService.scheduleAtFixedRate(() ->
                                                                         {
                                                                             LaconicLogger laconicLogger = new LaconicLogger(logger, TimeDuration.ofSeconds(15));
                                                                             try
                                                                             {
                                                                                 subsystem.periodicExecute();
                                                                             }
                                                                             catch (Throwable ex)
                                                                             {
                                                                                 laconicLogger.error(
                                                                                         "{}.periodicExecute() threw an exception. Cause Summary: {}",
                                                                                         subsystem.getName(),
                                                                                         ex.toString(),
                                                                                         ex);
                                                                             }
                                                                         },
                                                                         10,
                                                                         subsystem.getPeriodicExecutionTimeDuration().toMillis(),
                                                                         TimeUnit.MILLISECONDS);
                        }
                    }
                    catch (Exception e)
                    {
                        logger.warn("Could not schedule PeriodicallyExecutedSubsystem '{}' on the scheduledExecutorService due to an exception. Cause summary: {}",
                                    subsystem.getName(), e.toString(), e);
                    }
                }
            }
        }
        else
        {
            logger.info("scheduledExecutorService already running.");
        }
        logger.info("startScheduler() took {}ms", (System.currentTimeMillis() - startTime));
    }


    @DoesNotThrowExceptions
    private void stopScheduler()
    {
        logger.debug("stopScheduler() called");
        //Just an extra safety insurance
        stopDriveTrainSubsystem();
        try
        {
            if (scheduledExecutorService != null)
            {
                final AtomicBoolean keepRunning = new AtomicBoolean(false);
                scheduledExecutorService.shutdownNow();
                keepRunning.set(false);
            }
        }
        catch (Exception e)
        {
            logger.warn("An Exception was thrown when doing hard shutdown on the scheduledExecutorService. Cause Summary: {}", e.toString(), e);
        }
        finally
        {
            scheduledExecutorService = null;
        }

        feedDriveTrainMotorSafety();
    }


    @DoesNotThrowExceptions
    protected void feedDriveTrainMotorSafety()
    {
        try
        {
            if (Subsystems.haveSubsystemsBeenInitialized())
            {
                final Abstract3838DriveTrainSubsystem driveTrainSubsystem = getDriveTrainSubsystem();
                //noinspection ConstantConditions
                if (driveTrainSubsystem != null)
                {
                    driveTrainSubsystem.feedRobotDriveMotorSafety();
                }
            }
        }
        catch (Exception e)
        {
            logger.debug("An exception occurred when feeding motor safety. Cause Summary: {}", e.toString(), e);
        }
    }
    
    
    private enum Mode
    {
        None,
        Disabled,
        Autonomous,
        Teleop,
        Test
    }

    private Mode lastMode = Mode.None;

    // Copied from IterativeRobotBase but with the watchdog calls removed
    protected void loopFunc()
    {
        // Call the appropriate function depending upon the current robot mode
        if (isDisabled())
        {
            // Call DisabledInit() if we are now just entering disabled mode from either a different mode
            // or from power-on.
            if (lastMode != Mode.Disabled)
            {
                LiveWindow.setEnabled(false);
                Shuffleboard.disableActuatorWidgets();
                disabledInit();
                lastMode = Mode.Disabled;
            }

            HAL.observeUserProgramDisabled();
            disabledPeriodic();
        }
        else if (isAutonomous())
        {
            // Call AutonomousInit() if we are now just entering autonomous mode from either a different
            // mode or from power-on.
            if (lastMode != Mode.Autonomous)
            {
                LiveWindow.setEnabled(false);
                Shuffleboard.disableActuatorWidgets();
                autonomousInit();
                lastMode = Mode.Autonomous;
            }

            HAL.observeUserProgramAutonomous();
            autonomousPeriodic();
        }
        else if (isOperatorControl())
        {
            // Call TeleopInit() if we are now just entering teleop mode from either a different mode or
            // from power-on.
            if (lastMode != Mode.Teleop)
            {
                LiveWindow.setEnabled(false);
                Shuffleboard.disableActuatorWidgets();
                teleopInit();
                lastMode = Mode.Teleop;
            }

            HAL.observeUserProgramTeleop();
            teleopPeriodic();
        }
        else
        {
            // Call TestInit() if we are now just entering test mode from either a different mode or from
            // power-on.
            if (lastMode != Mode.Test)
            {
                LiveWindow.setEnabled(true);
                Shuffleboard.enableActuatorWidgets();
                testInit();
                lastMode = Mode.Test;
            }

            HAL.observeUserProgramTest();
            testPeriodic();
        }

        robotPeriodic();

        SmartDashboard.updateValues();
        LiveWindow.updateValues();
        Shuffleboard.update();

        if (isSimulation())
        {
            simulationPeriodic();
        }
    }
}
