package frc.team3838.core.config.time;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;



@SuppressWarnings("unused")
public enum PartialMinutes
{
    NoPartial
        {
            @Override
            public long toMills() { return 0; }
        },
    OneSixteenthMinute
        {
            @Override
            public long toMills() { return 3750; }
        },
    OneEighthMinute
        {
            @Override
            public long toMills() { return 7500; }
        },
    ThreeSixteenthMinute
        {
            @Override
            public long toMills() { return 11250; }
        },
    OneQuarterMinute
        {
            @Override
            public long toMills() { return 15000; }
        },
    FiveSixteenthMinute
        {
            @Override
            public long toMills() { return 18750; }
        },
    ThreeEightsMinute
        {
            @Override
            public long toMills() { return 18750; }
        },
    SevenSixteenthMinute
        {
            @Override
            public long toMills() { return 26250; }
        },
    HalfMinute
        {
            @Override
            public long toMills() { return 30000; }
        },
    NineSixteenthMinute
        {
            @Override
            public long toMills() { return 33750; }
        },
    FiveEightsMinute
        {
            @Override
            public long toMills() { return 37500; }
        },
    ElevenSixteenthMinute
        {
            @Override
            public long toMills() { return 41250; }
        },
    ThreeQuartersMinute
        {
            @Override
            public long toMills() { return 45000; }
        },
    ThirteenSixteenthMinute
        {
            @Override
            public long toMills() { return 48750; }
        },
    SevenEightsMinute
        {
            @Override
            public long toMills() { return 52500; }
        },
    FifteenSixteenthMinute
        {
            @Override
            public long toMills() { return 56250; }
        },
    FullMinute
        {
            @Override
            public long toMills() { return TimeUnit.MINUTES.toMillis(1); }
        }
        ;


    public abstract long toMills();
    
    
    public static long sumToMillis(PartialMinutes partialMinutes)
    {
        return sumToMillis(0, partialMinutes);
    }
    
    public static long sumToMillis(long minutes, PartialMinutes... partialMinutes)
    {
        return TimeUnit.MINUTES.toMillis(minutes) + Arrays.stream(partialMinutes).mapToLong(PartialMinutes::toMills).sum();
    }
    
}
