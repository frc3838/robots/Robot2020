package frc.team3838.core.config


@Suppress("unused")
enum class Baud(val baudRate: Int)
{
    // https://arduino.stackexchange.com/questions/296/how-high-of-a-baud-rate-can-i-go-without-errors
    BAUD_300(300),
    BAUD_600(600),
    BAUD_1200(1200),
    BAUD_2400(2400),
    BAUD_4800(4800),
    BAUD_9600(9600),
    BAUD_14400(14400),
    BAUD_19200(19200),
    BAUD_28800(28800),
    BAUD_38400(38400),
    BAUD_57600(57600),
    BAUD_115200(115200),
    BAUD_128000(128000),
    BAUD_153600(153600),
    BAUD_230400(230400),
    BAUD_256000(256000),
    BAUD_460800(460800),
    BAUD_512000(512000),
    BAUD_921600(921600),
    BAUD_1000000(1000000), // 1 Million; non standard
    BAUD_1024000(1024000),
    BAUD_1500000(1500000), // 1.5 Million; non standard
    BAUD_2000000(2000000), // 2 Million;  non standard
}
