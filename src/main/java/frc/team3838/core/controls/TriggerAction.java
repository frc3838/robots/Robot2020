package frc.team3838.core.controls;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.wpi.first.wpilibj.buttons.Trigger;
import edu.wpi.first.wpilibj.command.Command;



@SuppressWarnings("UnusedDeclaration")
public enum TriggerAction implements ControlAction<Trigger>
{
    /** Cancels a command when the trigger becomes active. */
    CancelWhenActive
        {
            @Override
            public void assign(Trigger trigger, Command command)
            {
                logAssignment(trigger, command, this);
                trigger.cancelWhenActive(command);
            }
        },
    /** 
     * Toggles (the running state of) a command when the trigger becomes active. 
     * The toggling is done by the system checking {@code command.isRunning()} 
     * and then calling either {@code command.cancel()} or {@code command.start()} 
     * as appropriate.
     */
    ToggleWhenActive
        {
            @Override
            public void assign(Trigger trigger, Command command)
            {
                logAssignment(trigger, command, this);
                trigger.toggleWhenActive(command);
            }
        },
    /** Starts the given command whenever the trigger just becomes active. */
    WhenActive
        {
            @Override
            public void assign(Trigger trigger, Command command)
            {
                logAssignment(trigger, command, this);
                trigger.whenActive(command);
            }
        },
    /** Starts the command when the trigger becomes inactive. */
    WhenInactive
        {
            @Override
            public void assign(Trigger trigger, Command command)
            {
                logAssignment(trigger, command, this);
                trigger.whenInactive(command);
            }
        },
    /** Starts the given command whenever the trigger just becomes active. */
    WhileActive
        {
            @Override
            public void assign(Trigger trigger, Command command)
            {
                logAssignment(trigger, command, this);
                trigger.whenActive(command);
            }
        };

    private static final Logger logger = LoggerFactory.getLogger(TriggerAction.class);


    private static void logAssignment(Trigger trigger, Command command, TriggerAction action)
    {
        if (logger.isDebugEnabled())
        {
            @SuppressWarnings("CastToConcreteClass")
            final String triggerName = (trigger instanceof NamedJoystickButton) ? ((NamedJoystickButton) trigger).getButtonName() : "unnamed";
            logger.debug("Assigning {} {} the command {}::{}", triggerName, action.name(), command.getName(), command.getClass().getName());
        }
    }
}