package frc.team3838.core.controls;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import frc.team3838.core.controls.ThrottleBuilder.Directionality;
import frc.team3838.core.meta.Max;
import frc.team3838.core.meta.Min;
import frc.team3838.core.utils.MathUtils;



public class BoostSpeedThrottle implements Throttle
{
    private static final Logger logger = LoggerFactory.getLogger(BoostSpeedThrottle.class);

    private final Throttle initialThrottle;
    private final Throttle boosterThrottle;

    private final double baseSpeed ;
    private final double maxSpeed ;

    private final Directionality directionality;


    public BoostSpeedThrottle(Throttle initialThrottle,
                              Throttle boosterThrottle,
                              @Min(0) @Max(1.0) double baseSpeed,
                              @Min(0) @Max(1.0) double maxSpeed,
                              Directionality directionality)
    {
        if (maxSpeed < baseSpeed)
        {
            throw new IllegalArgumentException("Max speed '" + maxSpeed + " ' must be greater than baseSpeed '" + baseSpeed + '\'');
        }
        this.directionality = directionality;
        this.initialThrottle = initialThrottle;
        this.boosterThrottle = boosterThrottle;
        this.baseSpeed = baseSpeed;
        this.maxSpeed = maxSpeed;
    }


    @Override
    public double get()
    {
        double initial = initialThrottle.get();
        double boost = boosterThrottle.get();

        double retval;
        if (boost > 0.01 && initial > 0.1)
        {
            retval = MathUtils.scaleRange(boost, 0, 1, baseSpeed, maxSpeed);
        }
        else
        {
            retval = (initial > 0.1) ? baseSpeed : 0;
        }

        switch (directionality)
        {
            case Negative:
                return -Math.abs(retval);
            case Positive:
                return Math.abs(retval);
            default:
                return retval;
        }
    }
}
