package frc.team3838.core.controls;

import edu.wpi.first.wpilibj.buttons.Trigger;
import frc.team3838.core.meta.API;


@API
public class ReversingThrottle implements Throttle
{
    private final Throttle throttle;

    private final Trigger runInReverseTrigger;


    public ReversingThrottle(Throttle throttle, Trigger runInReverseTrigger)
    {
        this.throttle = throttle;
        this.runInReverseTrigger = runInReverseTrigger;
    }


    @Override
    public double get()
    {
        return runInReverseTrigger.get() ? throttle.get() : -throttle.get();
    }
}
