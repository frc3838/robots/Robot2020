package frc.team3838.core.controls;

import edu.wpi.first.wpilibj.GenericHID;
import frc.team3838.core.meta.API;



@API
public class GamepadRightThumbArcadeAxisPair extends AbstractGamePadAxisPair
{

    public GamepadRightThumbArcadeAxisPair(GenericHID hid)
    {
        super(hid);
    }


    @Override
    public double getXorLeft()
    {
        return hid.getRawAxis(4);
    }


    @Override
    public double getYorRight()
    {
        return hid.getRawAxis(5);
    }
}
