package frc.team3838.core.controls;

import java.util.Collection;
import javax.annotation.Nonnull;

import edu.wpi.first.wpilibj.buttons.Trigger;
import frc.team3838.core.meta.API;



@API
public class AndButton extends AbstractLogicButton
{
    @API
    public AndButton(@Nonnull Trigger triggerOrButton) { super(triggerOrButton); }


    @API
    public AndButton(@Nonnull Trigger triggerOrButton, Trigger... otherTriggersOrButtons) { super(triggerOrButton, otherTriggersOrButtons); }


    @API
    public AndButton(@Nonnull Collection<Trigger> triggersOrButtons) { super(triggersOrButtons); }


    @Override
    public boolean get()
    {
        for (Trigger trigger : triggers)
        {
            if (!trigger.get())
            {
                return false;
            }
        }
        
        return !triggers.isEmpty();
    }
}
