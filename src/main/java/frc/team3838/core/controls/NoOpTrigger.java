package frc.team3838.core.controls;

import edu.wpi.first.wpilibj.buttons.Trigger;
import frc.team3838.core.meta.API;



@API
public class NoOpTrigger extends Trigger
{
    @Override
    public boolean get() { return false; }
}
