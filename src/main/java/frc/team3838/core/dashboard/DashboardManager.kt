@file:JvmName("DashboardManager")
package frc.team3838.core.dashboard

import edu.wpi.first.networktables.EntryListenerFlags
import edu.wpi.first.networktables.EntryNotification
import edu.wpi.first.wpilibj.SpeedController
import edu.wpi.first.wpilibj.shuffleboard.BuiltInLayouts
import edu.wpi.first.wpilibj.shuffleboard.BuiltInWidgets
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardContainer
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab
import edu.wpi.first.wpilibj.shuffleboard.SimpleWidget
import edu.wpi.first.wpilibj.shuffleboard.SuppliedValueWidget
import edu.wpi.first.wpilibj.smartdashboard.SendableBuilder
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard
import frc.team3838.core.meta.API
import mu.KotlinLogging
import java.util.function.BooleanSupplier
import java.util.function.DoubleConsumer
import java.util.function.IntConsumer


/*
    See ShuffleBoardSample example robot for some examples
    See the Javadoc of the BuiltInWidgets enum values for available properties for each widget type
    Property names are case- and whitespace-insensitive (capitalization and spaces do not matter).
    
    In addition to the custom properties for each widget type (listed in 
    the JavaDoc for its BuiltInWidgets enum value), all Widgets have the
    property:
        "Label position"
    with valid values of
        "TOP", "LEFT", "BOTTOM", "RIGHT", "HIDDEN"
    
    Use .withWidget(BuiltInWidgets.kNumberSlider) to define the type
         See Javadoc for the BuiltInWidgets types for a list of properties that can be set
    
    == SOME EXAMPLES ==
     
     SimpleWidget switcher = tab.add("My Switcher", true).withWidget(BuiltInWidgets.kToggleSwitch);
     
     // A boolean box with the "true" color set to blue (the property name is "green" rather than "true") 
     SimpleWidget booleanBox = tab.add(title, defaultValue).withWidget(BuiltInWidgets.kBooleanBox).withProperties("green", "#0000FF");
     
     
    
    == a boolean toggle switch with a consumer
        
        val widget: SimpleWidget = tab.add("Use PID control", false)
            .withWidget(BuiltInWidgets.kToggleSwitch)
            .withPosition(0, 4)
 
        val consumer: BooleanConsumer = ...
        
        widget.entry.addListener({ event: EntryNotification ->
                                   if (event.value.isBoolean)
                                   {
                                       SmartDashboard.postListenerTask {
                                           consumer.accept(event.value.boolean)
                                       }
                                   }
                               }, EntryListenerFlags.kImmediate or EntryListenerFlags.kNew or EntryListenerFlags.kUpdate)
                               
                               
        Add a 'max speed' widget to a tab named 'Configuration', using a number slider
        The widget will be placed in the second column and row and will be TWO columns wide
        
        NetworkTableEntry maxSpeed = Shuffleboard.getTab("Configuration")
                                                 .add("Max Speed", 1)
                                                 .withWidget("Number Slider")
                                                 .withPosition(1, 1)
                                                 .withSize(2, 1)
                                                 .getEntry();                         
                               
       public static final NetworkTableEntry elevatorCountEntry = DashboardManager.getCommandsTab().add("Elevator Count", 0.0)
                                                                                       .getEntry();
       public static final NetworkTableEntry elevatorUpDioEntry = DashboardManager.getCommandsTab().add("Up DIO", Boolean.FALSE).getEntry();
       public static final NetworkTableEntry elevatorDownDioEntry = DashboardManager.getCommandsTab().add("Down DIO", Boolean.FALSE).getEntry();
       public static final NetworkTableEntry legSpeedEntry =
    
      DashboardManager.getCommandsTab().add("Leg Speed", 0.0)
                                .withProperties(Map.of("Min", -1.0, "Max", 1.0, "Block increment", 0.05))
                                .getEntry();
 */

@Suppress("unused")
private val logger = KotlinLogging.logger {}


@API
val matchPlayTab: ShuffleboardTab = Shuffleboard.getTab("Match Play")
@API
val motorsTestTab: ShuffleboardTab = Shuffleboard.getTab("Motors Test")
@API
val commandsTab: ShuffleboardTab = Shuffleboard.getTab("Commands")
@API
val driveTab: ShuffleboardTab = Shuffleboard.getTab("Drive")
@API
val electricalTab: ShuffleboardTab = Shuffleboard.getTab("Electrical")

// 
/*
        // Add a 'max speed' widget to a tab named 'Configuration', using a number slider
        // The widget will be placed in the second column and row and will be TWO columns wide
        NetworkTableEntry maxSpeed = Shuffleboard.getTab("Configuration")
                                                 .add("Max Speed", 1)
                                                 .withWidget("Number Slider")
                                                 .withPosition(1, 1)
                                                 .withSize(2, 1)
                                                 .getEntry();
     */
//    NetworkTableEntry maxSpeed = Shuffleboard.getTab("Configuration")
//                                             .add("Max Speed", 1)
//                                             .withWidget(BuiltInWidgets.kNumberSlider)
//                                             .withPosition(1, 1)
//                                             .withSize(2, 1)
//                                             .getEntry();
val axisPairLayout = driveTab
    .getLayout("AdjustedAxisPairReading", BuiltInLayouts.kList)
    .withProperties(mapOf<String, Any>("Label position" to "LEFT"))
    .withSize(2, 2)

@API
@JvmOverloads
fun ShuffleboardContainer.addDoubleSettingTextBox(title: String, defaultValue:Double, consumer: DoubleConsumer? = null): SimpleWidget
{
    val widget =  add(title, defaultValue).withWidget(BuiltInWidgets.kTextView)
    widget.addDoubleConsumer(consumer)
    return widget
}

@API
@JvmOverloads
fun ShuffleboardContainer.addIntSettingTextBox(title: String, defaultValue:Int, consumer: IntConsumer? = null): SimpleWidget
{
    val widget =  add(title, defaultValue).withWidget(BuiltInWidgets.kTextView)
    widget.addIntConsumer(consumer)
    return widget
}

@API
/** Adds a BooleanBox to a ShuffleboardTab or ShuffleboardLayout with a default value. */
fun ShuffleboardContainer.addBooleanBox(title: String, defaultValue: Boolean): SimpleWidget
{
    return add(title, defaultValue).withWidget(BuiltInWidgets.kBooleanBox)
}

@API
/** Adds a BooleanBox to a ShuffleboardTab or ShuffleboardLayout along with a supplier to keep the value updated. */
fun ShuffleboardContainer.addBooleanBox(title: String, booleanSupplier: BooleanSupplier): SuppliedValueWidget<Boolean>
{
    return addBoolean(title, booleanSupplier).withWidget(BuiltInWidgets.kBooleanBox)
}

@API
@JvmOverloads
fun ShuffleboardContainer.addToggleSwitch(title: String, defaultValue: Boolean, booleanConsumer: SendableBuilder.BooleanConsumer? = null): SimpleWidget
{

    val widget = add(title, defaultValue).withWidget(BuiltInWidgets.kToggleSwitch)
    widget.addBooleanConsumer(booleanConsumer)
    return widget
}

@API
fun SimpleWidget.addBooleanConsumer(booleanConsumer: SendableBuilder.BooleanConsumer?)
{
    if (booleanConsumer != null)
    {
        entry.addListener({ event: EntryNotification ->
                                     if (event.value.isBoolean)
                                     {
                                         SmartDashboard.postListenerTask {
                                             booleanConsumer.accept(event.value.boolean)
                                         }
                                     }
                                 }, EntryListenerFlags.kImmediate or EntryListenerFlags.kNew or EntryListenerFlags.kUpdate)
    }
}

@API
fun SimpleWidget.addDoubleConsumer(consumer: DoubleConsumer?)
{
    @Suppress("DuplicatedCode")
    if (consumer != null)
    {
        val pEntry = entry
        pEntry.addListener({ event: EntryNotification ->
                               if (event.value.isDouble)
                               {
                                   SmartDashboard.postListenerTask {
                                       consumer.accept(event.value.double)
                                   }
                               }
                           }, EntryListenerFlags.kImmediate or EntryListenerFlags.kNew or EntryListenerFlags.kUpdate)
    }
}

@API
fun SimpleWidget.addIntConsumer(consumer: IntConsumer?)
{
    @Suppress("DuplicatedCode")
    if (consumer != null)
    {
        val pEntry = entry
        pEntry.addListener({ event: EntryNotification ->
                               if (event.value.isDouble)
                               {
                                   SmartDashboard.postListenerTask {
                                       consumer.accept(event.value.double.toInt())
                                   }
                               }
                           }, EntryListenerFlags.kImmediate or EntryListenerFlags.kNew or EntryListenerFlags.kUpdate)
    }
}





//@API
//fun SpeedController.addMotorTestWidget(name: String)
//{
//    
//    motorsTestTab.add("Start", 0.0).withWidget(BuiltInWidgets.kSpeedController)
//}

fun SpeedController?.addMotorTestWidget(name: String)
{
    if (this != null)
    {
        if (true /*isTestMode()*/)
        {
            logger.info { "Adding Motor Test Widget $name" }
            val coordinator = SpeedCoordinator(this, name)

            var col = 0
            var row = 0
            val layout = motorsTestTab.getLayout(name, BuiltInLayouts.kList)
                .withSize(2, 2)
                .withPosition(0, 0)
                .withProperties(mapOf<String, Any>("Label Position" to "LEFT"))

            val speed = layout.add("Speed", 0.0)
                .withWidget(BuiltInWidgets.kNumberSlider)
                .withSize(4, 1)
                .withPosition(col++, row++)
                .withProperties(mapOf<String, Any>("Block increment" to 0.05))

            speed.addDoubleConsumer(DoubleConsumer { value -> coordinator.modifySpeed(value) })

            val onOff = layout.add("On/Off", true)
                .withWidget(BuiltInWidgets.kToggleSwitch)
                .withSize(2, 1)
                .withPosition(col, row)

            onOff.addBooleanConsumer(SendableBuilder.BooleanConsumer { value -> coordinator.toggleOnOff(value) })
        }
    }
    else
    {
        logger.info { "SpeedController was null. Can't add Motor Test Widget $name" }
    }
}

private class SpeedCoordinator(val controller: SpeedController, val name: String) : SendableBuilder.BooleanConsumer, DoubleConsumer
{
    private var on = true
    private var speed: Double = 0.0

    fun toggleOnOff(value: Boolean)
    {
        on = value
        if (on)
        {
            controller.set(speed)
        }
        else
        {
            controller.set(0.0)
        }
    }

    override fun accept(value: Boolean) = toggleOnOff(value)

    fun modifySpeed(value: Double)
    {
        val previousSpeed = speed
        speed = value
        if (on)
        {
            controller.set(speed)
            if (speed != previousSpeed)
            {
                logger.info { "Changing speed for $name from %4.2f to %4.2f".format(previousSpeed, speed) }
            }
        }
    }

    override fun accept(value: Double) = modifySpeed(value)

}