package frc.team3838.core.commands

/**
 * A command that simply calls the provided action in the [doAction] method on each loop, and never 
 * returns `true` for isFinished, unless the [isFinishedImpl] method is overridden. An example use 
 * of this command would be keep a status updated. Note that by default the action is **not** called 
 * during command initialization (i.e. when the `initialize` method is called). This can be modified 
 * by overriding the [runActionDuringInitialize] method.
 * 
 * For an alternative, a command that takes a function to execute at construction, see the [PollingCommand]
 * class.
 * 
 * @see PollingCommand 
 */
abstract class AbstractPollingCommand : Abstract3838Command()
{
    override fun initializeImpl()
    {
        if (runActionDuringInitialize()) 
        {
            doAction()
        }
    }

    override fun executeImpl() = doAction()

    abstract fun doAction()
    
    open fun runActionDuringInitialize(): Boolean = false

    override fun isFinishedImpl(): Boolean = false

    override fun endImpl() { /* No Op */ }
}