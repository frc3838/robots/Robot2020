package frc.team3838.core.commands.drive

import frc.team3838.core.commands.Abstract3838CommandGroup
import frc.team3838.core.commands.navx.RestNavxCommand
import frc.team3838.core.config.time.TimeDuration
import frc.team3838.core.meta.API

/**
 * Resets the navX, waits the duration of time (specified in either the `RestNavxCommand` or as the `postResetWaitDuration`
 * supplied in the alternate constructor.
 * @param driveCommand the drive command to run after the navX is reset
 * @param restNavxCommand the RestNavxCommand to use (with its configured reset wait delay)
 */
@API
class DriveWithPreResetOfNavxCommand constructor(driveCommand: DriveCommand, restNavxCommand: RestNavxCommand): Abstract3838CommandGroup()
{
    /**
     * @param driveCommand the drive command to run after the navX is reset
     * @param postResetWaitDuration the duration to wait after the navX is reset before the drive command runs. Defaults to 1 second
     */
    @API
    @JvmOverloads
    constructor(driveCommand: DriveCommand, postResetWaitDuration: TimeDuration = TimeDuration.ofSeconds(1)): this(driveCommand, RestNavxCommand(postResetWaitDuration))
    
    init
    {
        addSequential(restNavxCommand)
        addSequential(driveCommand)
    }
}