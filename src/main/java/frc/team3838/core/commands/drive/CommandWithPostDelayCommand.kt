package frc.team3838.core.commands.drive

import edu.wpi.first.wpilibj.command.Command
import frc.team3838.core.commands.Abstract3838CommandGroup
import frc.team3838.core.commands.common.SleepCommand
import frc.team3838.core.meta.API


/** Executes the supplied command then the supplied delay. */
@API
class CommandWithPostDelayCommand(command: Command, sleepCommand: SleepCommand): Abstract3838CommandGroup()
{
    init
    {
        addSequential(command)
        addSequential(sleepCommand)
    }
}