package frc.team3838.core.commands.drive

import frc.team3838.core.commands.Abstract3838CommandGroup
import frc.team3838.core.commands.common.SleepCommand
import frc.team3838.core.commands.common.SleepCommand.Companion.ofSecondsReadFromDashboard
import frc.team3838.core.config.time.TimeDuration
import frc.team3838.core.units.Inches

class DriveStraightSetDistanceWithDelayCommand : Abstract3838CommandGroup
{
    @JvmOverloads
    constructor(targetDistanceInInches: Inches, delay: TimeDuration, enableTuning: Boolean = false)
    {
        addSequential(SleepCommand.of(delay))
        addSequential(DriveStraightSetDistanceCommand(targetDistanceInInches, enableTuning = enableTuning))
    }

    @JvmOverloads
    constructor(targetDistanceInInches: Inches, timeDelayDashboardKey: String?, defaultDelayValueInSeconds: Long, enableTuning: Boolean = false)
    {
        addSequential(ofSecondsReadFromDashboard(timeDelayDashboardKey!!, defaultDelayValueInSeconds.toDouble()))
        addSequential(DriveStraightSetDistanceCommand(targetDistanceInInches, enableTuning = enableTuning))
    }
}