package frc.team3838.core.commands.drive


class StopDriveTrainCommand(): AbstractBasicDriveCommand()
{
    override fun executeImpl() = driveTrainSubsystem.stop()
    override fun isFinishedImpl(): Boolean = true
}