package frc.team3838.core.commands.drive

import frc.team3838.core.commands.NO_TIMEOUT
import frc.team3838.core.config.time.TimeDuration
import frc.team3838.core.meta.API
import frc.team3838.core.subsystems.Subsystems
import frc.team3838.core.subsystems.drive.Abstract3838DriveTrainSubsystem
import frc.team3838.core.units.Angle
import frc.team3838.core.units.Inches
import frc.team3838.core.units.Speed

@API
open class DriveAtAngleForSetDistanceCommand @JvmOverloads constructor(
        targetDistance: Inches,
        targetAngle: Angle,
        speed: Speed = Speed(Subsystems.getDriveTrainSubsystem().maxSpeedForAutonomous),
        robotDirection: Abstract3838DriveTrainSubsystem.RobotDirection = Abstract3838DriveTrainSubsystem.RobotDirection.Forward,
        enableTuning: Boolean = false,
        name: String? = null,
        timeoutDuration: TimeDuration = NO_TIMEOUT): DriveForSetDistanceCommand(targetDistance,
                                                                                targetAngle,
                                                                                speed,
                                                                                robotDirection,
                                                                                enableTuning,
                                                                                name,
                                                                                timeoutDuration)
