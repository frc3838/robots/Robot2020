package frc.team3838.core.commands

import com.google.common.collect.ImmutableSet
import frc.team3838.core.meta.API
import frc.team3838.core.subsystems.I3838Subsystem

/**
 * Runs the provided action one time.
 * @see Instant3838NoSubsystemCommand
 * @see Instant3838TwoSubsystemCommand
 * @see Instant3838ThreeSubsystemCommand
 */
@API
class Instant3838OneSubsystemCommand<S : I3838Subsystem> @JvmOverloads constructor(val subsystem: S,
                                                                                   name: String? = null,
                                                                                   private val action: (S) -> Unit) :
        AbstractInstant3838Command(name)
{
    override fun executeImpl() = action.invoke(subsystem)

    override fun getRequiredSubsystems(): ImmutableSet<I3838Subsystem> = ImmutableSet.of(subsystem)
}