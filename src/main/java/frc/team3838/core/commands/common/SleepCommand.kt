package frc.team3838.core.commands.common

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard
import frc.team3838.core.commands.Abstract3838NoSubsystemsCommand
import frc.team3838.core.config.time.PartialSeconds
import frc.team3838.core.config.time.TimeDuration
import frc.team3838.core.meta.API
import frc.team3838.core.utils.NO_OP
import java.time.Duration
import java.time.LocalTime
import java.util.concurrent.TimeUnit

@API
class SleepCommand @JvmOverloads constructor(duration: TimeDuration,
                                             name: String? = "SleepCommand") : Abstract3838NoSubsystemsCommand(name, duration)
{
    @API constructor(duration: Duration, name: String? = "SleepCommand"): this(TimeDuration.ofNanos(duration.toNanos()), name)
    @API constructor(duration: String, name: String? = "SleepCommand"): this(TimeDuration.of(duration), name)
    @API constructor(durationAmt: Long, durationTimeUnit: TimeUnit, name: String? = "SleepCommand"): this(TimeDuration.of(durationAmt, durationTimeUnit), name)
    
    // isFinished in super handles the timeout
    override fun isFinishedImpl(): Boolean = false

    override fun executeImpl() = NO_OP()

    override fun initializeImpl() = NO_OP()
    
    override fun endImpl()
    {
        logger.debug("Ending {} at {}", name, LocalTime.now())
    }

    companion object
    {
        @API
        @JvmStatic
        @JvmOverloads
        fun ofSeconds(seconds: Long, name: String? = "SleepCommand") = SleepCommand(TimeDuration.ofSeconds(seconds), name)

        @API
        @JvmStatic
        @JvmOverloads
        fun ofSeconds(seconds: Long, partialSeconds: PartialSeconds, name: String? = "SleepCommand") = SleepCommand(TimeDuration.ofSeconds(seconds, partialSeconds), name)

        @API
        @JvmStatic
        @JvmOverloads
        fun ofSeconds(partialSeconds: PartialSeconds, name: String? = "SleepCommand") = SleepCommand(TimeDuration.ofSeconds(partialSeconds), name)

        @API
        @JvmStatic
        @JvmOverloads
        fun ofSeconds(seconds: Long, additionalMilliseconds: Long, name: String? = "SleepCommand") = SleepCommand(TimeDuration.ofMillis(TimeUnit.SECONDS.toMillis(seconds) + additionalMilliseconds), name)

        @API
        @JvmStatic
        fun of(timeDuration: TimeDuration, name: String? = "SleepCommand") = SleepCommand(timeDuration, name)
        
        @API
        @JvmStatic
        fun of(durationAmt: Long, durationTimeUnit: TimeUnit) = SleepCommand(TimeDuration.of(durationAmt, durationTimeUnit))

        @API
        @JvmStatic
        fun of(durationAmt: Long, durationTimeUnit: TimeUnit, name: String? = "SleepCommand") = SleepCommand(TimeDuration.of(durationAmt, durationTimeUnit), name)

        @API
        @JvmStatic
        fun of(duration1: Long, timeUnit1: TimeUnit, duration2: Long, timeUnit2: TimeUnit): SleepCommand = SleepCommand(TimeDuration.of(duration1, timeUnit1, duration2, timeUnit2))

        @API
        @JvmStatic
        fun of(duration1: Long, timeUnit1: TimeUnit, duration2: Long, timeUnit2: TimeUnit, name: String? = "SleepCommand"): SleepCommand = SleepCommand(TimeDuration.of(duration1, timeUnit1, duration2, timeUnit2), name)

        @API
        @JvmStatic
        fun of(duration1: Long, timeUnit1: TimeUnit, duration2: Long, timeUnit2: TimeUnit, duration3: Long, timeUnit3: TimeUnit): SleepCommand = SleepCommand(TimeDuration.of(duration1, timeUnit1, duration2, timeUnit2, duration3, timeUnit3))
        @API
        @JvmStatic
        fun of(duration1: Long, timeUnit1: TimeUnit, duration2: Long, timeUnit2: TimeUnit, duration3: Long, timeUnit3: TimeUnit, name: String? = "SleepCommand"): SleepCommand = SleepCommand(TimeDuration.of(duration1, timeUnit1, duration2, timeUnit2, duration3, timeUnit3), name)

        @API
        @JvmStatic
        @JvmOverloads
        fun ofMilliseconds(milliseconds: Long, name: String? = "SleepCommand"): SleepCommand = SleepCommand(TimeDuration.ofMillis(milliseconds), name)

        @API
        @JvmStatic
        fun ofSecondsReadFromDashboard(dashboardKey: String, defaultValueInSeconds: Double): SleepCommand
        {
            return ofSecondsReadFromDashboard(dashboardKey, defaultValueInSeconds, null)
        }
        @API
        @JvmStatic
        fun ofSecondsReadFromDashboard(dashboardKey: String, defaultValueInSeconds: Double, name: String? = "SleepCommand"): SleepCommand
        {
            val seconds = SmartDashboard.getNumber(dashboardKey, defaultValueInSeconds)
            val factor = TimeUnit.SECONDS.toNanos(1)
            val nanos = (seconds * factor).toLong()
            return SleepCommand(TimeDuration.ofNanos(nanos), name)
        }
    }
}
