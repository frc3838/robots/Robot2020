package frc.team3838.core.commands

import edu.wpi.first.wpilibj.command.Command
import edu.wpi.first.wpilibj.command.CommandGroup
import edu.wpi.first.wpilibj.command.Subsystem
import frc.team3838.core.commands.common.SleepCommand
import frc.team3838.core.config.time.TimeDuration
import frc.team3838.core.logging.LaconicLogger
import frc.team3838.core.meta.API
import frc.team3838.core.meta.DoesNotThrowExceptions
import frc.team3838.core.subsystems.I3838Subsystem
import frc.team3838.core.utils.NO_OP
import frc.team3838.core.utils.time.Timed
import frc.team3838.core.utils.time.Timer
import mu.KLogger
import mu.KotlinLogging
import org.slf4j.event.Level
import java.util.concurrent.TimeUnit

/**
 * Abstract base command for 3838 CommandGroups. To use, extend, then in the `constructor` (Java) or `init` block (Kotlin)
 * add the desired commands.
 * 
 * To run commands in sequential order:
 *
 * ```
 * addSequential(new Command1())
 * addSequential(new Command2())
 * ```
 *
 * To run multiple commands at the same time, use `addParallel()`. For example"
 * ```
 * addParallel(new Command1());
 * addSequential(new Command2());
 * ```
 * Command1 and Command2 will run in parallel.
 *
 * A command group will require all of the subsystems that each member would require.
 * e.g. if Command1 requires chassis, and Command2 requires arm,
 * a CommandGroup containing them would require both the chassis and the arm.
 * The `addSequential` and `addParallel` methods will automatically add the
 * requires subsystems.
 * 
 * Java example:
 * ```
 * public AutoModeJavaExampleCommand()
 * {
 *     super(
 *         List.of(
 *         new CommandGroupEntry(new LogStatementCommand("Command 1")),
 *         new CommandGroupEntry(new LogStatementCommand("Command 2"), AddAs.Sequential),
 *         new CommandGroupEntry(new LogStatementCommand("Command 3"))
 *     ));
 * }
 * ```
 * 
 */
abstract class Abstract3838CommandGroup @JvmOverloads constructor(
        /** The name of the command. Defaults to the simple class name if not set or if set to `null`. */
        name: String? = null,
        /** When the command should timeout. Default is no timeout. NOTE: setting this property after the command has started will
         * not change the timeout for the current run. The setting is read (and used) during command initialization. */
        @API private val timeoutDuration: TimeDuration = NO_TIMEOUT): CommandGroup(), Timed
{
    protected val logger: KLogger
    
    protected val subsystems: MutableSet<I3838Subsystem> = mutableSetOf()

    @API
    var hasHadException = false
        private set

    @API
    var initializedSuccessfully = true
        private set

    @API
    protected val timeoutTimer: Timer = Timer(timeoutDuration)

    @API
    protected val timeRemainingLaconicLogger: LaconicLogger

    @API
    protected open var timeoutLogLevel: Level = Level.DEBUG

    @Suppress("MemberVisibilityCanBePrivate")
    var commandTimedOut = false
        private set

    private var disableMessageLogged = false

    private var disabledMessage = createDisabledMessage()

    init
    {
        @Suppress("DEPRECATION") // setName in Sendable is deprecated, but is still valid in Command
        super.setName(name ?: this::class.java.simpleName)
        logger = KotlinLogging.logger(this::class.java.name)
        timeRemainingLaconicLogger = LaconicLogger(logger, TimeDuration.of("250 milliseconds"))
        if (timeoutDuration != NO_TIMEOUT)
        {
            // We add an extra 50 milliseconds so our internal timeout takes precedence, but the WPI timeout acts as a backup
            setTimeout(timeoutDuration.toFractionalSeconds() + 0.05)
        }
    }

    private val disabledLaconicLogger = LaconicLogger(logger, TimeDuration.of(15, TimeUnit.SECONDS))

    /** Updates the interval of the timeout logging to the supplied interval. */
    @API
    protected fun setTimeoutLoggingInterval(interval: TimeDuration) = timeRemainingLaconicLogger.setInterval(interval)

    /** Updates the interval of the timeout logging to the supplied interval. */
    @API
    protected fun setTimeoutLoggingInterval(intervalDuration: Long, intervalTimeUnit: TimeUnit) = timeRemainingLaconicLogger.setInterval(intervalDuration, intervalTimeUnit)

    /** Updates the interval of the timeout logging to the supplied interval, such that the supplied string parses to a TIme Duration. */
    @API
    protected fun setTimeoutLoggingInterval(interval: String) = timeRemainingLaconicLogger.setInterval(interval)


    @DoesNotThrowExceptions
    final override fun initialize()
    {
        hasHadException = false
        initializedSuccessfully = false

        if (areAllSubsystemsAreEnabled())
        {
            logger.debug { "$name.initialize() called" }
            try
            {
                initializeAdditional()
                commandTimedOut = false
                timeoutTimer.start()
                initializedSuccessfully = true
                logger.debug("Running {}", javaClass.simpleName)
            }
            catch (e: Exception)
            {
                hasHadException = true
                logger.error(e) { "An exception occurred when initializing the command '$name'. The command will be able to be run at all." }
            }
        }
        else
        {
            logger.warn { "Not all required subsystems for the command '$name' are enabled. Therefore the command can not be and will not be initialized or executed." }
        }
    }
    
    @API
    open fun initializeAdditional() = NO_OP()

    final override fun execute()
    {
        if (areAllSubsystemsAreEnabled())
        {
            try
            {
                super.execute()
            }
            catch (e: Exception)
            {
                logger.error(e){"An exception occurred when executing $name.execute(). Cause Summary: $e"}
                //If something has gone wrong, we do not want the command to execute any longer.
                hasHadException = true
            }
        }
        else
        {
            if (!disableMessageLogged)
            {
                disabledMessage = createDisabledMessage()
                disabledLaconicLogger.info { "In execute: $disabledMessage" }
                disableMessageLogged = true
            }
            disabledLaconicLogger.debug { "In execute: $disabledMessage" } 
        }
    }

    override fun isFinished(): Boolean
    {
        return try
        {
            // the isTimedOut() function logs that the command has timed out the first time it is called and the command timed out. 
            // We call it last so that log message does not appear for other situations. For example, if the command finished properly, 
            // or if there is a bad initialization, we do not want "command has timed out" logged later on
            !initializedSuccessfully || hasHadException || isFinishedAdditional() || super.isFinished()
        }
        catch (e: Exception)
        {
            logger.error(e) { "An exception occurred when calling $name.isFinished(). Cause Summary: $e" }
            hasHadException = true
            true
        }
    }
    
    @API
    open fun isFinishedAdditional() = false

    private val sleepCommandName = SleepCommand::class.java.name

    /**
     * Returns whether or not the command has timed out. This overridden implementation
     * in [Abstract3838Command] has millisecond precision whereas the default
     * implementation in the WpiLib [Command] class only has second precision. While
     * we do not want to try and timeout commands to the millisecond as there will always be
     * a few milliseconds of 'fluff' time, there are some use cases where we need a bit more
     * fine grained timeouts with 1/2 or 1/4 second, or possibly even 1/8 second,
     * precision rather than the a full second that is provided by the WpiLib `Command` class.
     *
     *
     * This method is called by the `isFinished` implementation within this [Abstract3838Command]
     * class.
     *
     * @return whether the time has expired
     */
    @Synchronized
    override fun isTimedOut(): Boolean
    {
        // The parent method is only accurate to the second. We want millisecond 
        // accuracy so we can have partial second, such a 1½ seconds, timeouts.
        // Thus we are fully overriding the parent method

        if (timeoutDuration == NO_TIMEOUT) return false
        if (commandTimedOut) return true

        timeRemainingLaconicLogger.logAt(timeoutLogLevel) { "Time remaining before timeout: ${timeoutTimer.timeRemaining()}" }

        commandTimedOut = timeoutTimer.isTimedOut()
        if (commandTimedOut)
        {
            logger.info { "Command $name has timed out" }
        }

        return commandTimedOut
    }

    override fun requires(subsystem: Subsystem?)
    {
        super.requires(subsystem)
        if (subsystem is I3838Subsystem) subsystems.add(subsystem)
    }

    @API
    protected open fun areAllSubsystemsAreEnabled(): Boolean
    {
        for (subsystem in subsystems)
        {
            if (!subsystem.isEnabled)
            {
                return false
            }
        }
        //All subsystems are enabled. So as long as we are initialized properly, we are good to go.
        return true
    }

    private fun createDisabledMessage() = """COMMAND NOT AVAILABLE: Not all required subsystems for the command '$name' are 
            |enabled, or there was a problem when initializing the command. Therefore the command can not be and will not be executed.""".trimMargin()

}