package frc.team3838.core.commands.log

import edu.wpi.first.wpilibj.command.Command
import java.time.Duration

class LogCommandGroupFinishedCommand @JvmOverloads constructor(commandToLog: Command, 
                                                               val startStatusLogger: AbstractLogCommandGroupStatusCommand? = null) 
    : AbstractLogCommandGroupStatusCommand(commandToLog)
{
    override val status: String
        get() = "Finished"

    override val additionalMessage: String
        get() = if (startStatusLogger != null)
        {
            val duration = Duration.between(startStatusLogger.statusTime, statusTime)
            " a duration of $duration"
        }
        else
        {
            super.additionalMessage
        }
}