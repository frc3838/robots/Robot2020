package frc.team3838.core.commands.log

import frc.team3838.core.commands.AbstractInstant3838NoSubsystemCommand

/**
 * Simple command that logs a statement that the next step in an autonomous sequence is starting.
 * Typically this is for use when "recording", via logging, the driver's actions when determining
 * actions needed to mimic a driver's actions.
 */
class NextStepLogStatementCommand(private val stepName: String = "") : AbstractInstant3838NoSubsystemCommand()
{
    @Throws(Exception::class)
    override fun executeImpl()
    {
        logger.info("================================================================================")
        logger.info("================================================================================")
        logger.info("  >>> NEXT STEP $stepName")
        logger.info("================================================================================")
        logger.info("================================================================================")
    }
}