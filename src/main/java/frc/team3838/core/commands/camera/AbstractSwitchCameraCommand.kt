package frc.team3838.core.commands.camera

import com.google.common.collect.ImmutableSet
import edu.wpi.first.wpilibj.buttons.Trigger
import frc.team3838.core.commands.Abstract3838Command
import frc.team3838.core.subsystems.Abstract3838TwoUsbCameraSubsystem
import frc.team3838.core.subsystems.I3838Subsystem
import frc.team3838.core.utils.NO_OP

abstract class AbstractSwitchCameraCommand : Abstract3838Command()
{
    protected abstract val cameraSubsystem: Abstract3838TwoUsbCameraSubsystem
    protected abstract val trigger: Trigger
    protected abstract val invert: Boolean

    override fun getRequiredSubsystems(): ImmutableSet<I3838Subsystem> = ImmutableSet.of(cameraSubsystem)

    override fun initializeImpl() = NO_OP()


    override fun executeImpl()
    {
        var triggerValue = trigger.get()
        if (invert)
        {
            triggerValue = !triggerValue
        }
        if (triggerValue)
        {
            cameraSubsystem.switchToSecondaryCamera()
        }
        else
        {
            cameraSubsystem.switchToPrimaryCamera()
        }
    }


    override fun isFinishedImpl(): Boolean = false


    override fun endImpl() = cameraSubsystem.switchToPrimaryCamera()


    override fun interruptedImpl() = end()
}