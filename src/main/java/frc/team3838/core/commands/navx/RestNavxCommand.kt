package frc.team3838.core.commands.navx

import com.google.common.collect.ImmutableSet
import frc.team3838.core.commands.Abstract3838Command
import frc.team3838.core.config.time.TimeDuration
import frc.team3838.core.subsystems.I3838Subsystem
import frc.team3838.core.subsystems.NavxSubsystem
import frc.team3838.core.utils.NO_OP

/**
 * Resets the navX system, and then waits the specified duration before ending the command.
 * @param postResetWaitDuration the delay before the command completes after the reset. Defaults to 1 second.
 */
class RestNavxCommand(postResetWaitDuration: TimeDuration = TimeDuration.ofSeconds(1)) :
        Abstract3838Command(timeoutDuration = postResetWaitDuration)
{
    override fun getRequiredSubsystems(): ImmutableSet<I3838Subsystem> =  ImmutableSet.of<I3838Subsystem>(NavxSubsystem.getInstance())

    override fun initializeImpl() 
    {
        if (NavxSubsystem.getInstance().isEnabled && NavxSubsystem.getInstance().navx != null)
        {
            NavxSubsystem.getInstance().navx?.reset()
        }
    }

    override fun executeImpl() = NO_OP()

    @Throws(Exception::class)
    override fun isFinishedImpl(): Boolean = isTimedOut

    override fun endImpl() = NO_OP()
}