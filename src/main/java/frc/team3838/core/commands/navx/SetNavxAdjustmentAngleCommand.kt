package frc.team3838.core.commands.navx

import com.google.common.collect.ImmutableSet
import frc.team3838.core.commands.AbstractInstant3838Command
import frc.team3838.core.meta.API
import frc.team3838.core.subsystems.I3838Subsystem
import frc.team3838.core.subsystems.NavxSubsystem
import frc.team3838.core.utils.NO_OP

/** Sets the navX adjustment angle. */
@API
class SetNavxAdjustmentAngleCommand(private val theAdjustmentAngle: Double) : AbstractInstant3838Command()
{
    override fun getRequiredSubsystems(): ImmutableSet<I3838Subsystem> = ImmutableSet.of<I3838Subsystem>(NavxSubsystem.getInstance())
    
    override fun initializeImpl()
    {
        if (NavxSubsystem.getInstance().isEnabled && NavxSubsystem.getInstance().navx != null)
        {
            NavxSubsystem.getInstance().navx!!.angleAdjustment = theAdjustmentAngle
        }
    }

    override fun executeImpl() = NO_OP()
}