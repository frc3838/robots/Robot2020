package frc.team3838.core.commands.autonomous

import edu.wpi.first.wpilibj.DriverStation
import edu.wpi.first.wpilibj.DriverStation.Alliance
import edu.wpi.first.wpilibj.command.Command
import frc.team3838.core.commands.common.NoOpCommand
import frc.team3838.core.meta.API
import org.apache.commons.lang3.builder.EqualsBuilder
import org.apache.commons.lang3.builder.HashCodeBuilder
import org.apache.commons.lang3.builder.ToStringBuilder
import org.apache.commons.lang3.builder.ToStringStyle
import org.slf4j.LoggerFactory

@API
class AllianceAwareAutonomousCommandConfiguration(override val description: String,
                                                  private val blueCommand: Command,
                                                  private val redCommand: Command) : AutonomousCommandConfiguration
{

    override val command: Command
        get()
        {
            return when (val alliance = DriverStation.getInstance().alliance)
            {
                Alliance.Blue    -> blueCommand
                Alliance.Red     -> redCommand
                Alliance.Invalid ->
                {
                    val msg = "DriverStation is reporting current alliance as 'Invalid'. Cannot return proper command. Will return NoOpCommand"
                    logger.error(msg)
                    DriverStation.reportError(msg, false)
                    NoOpCommand
                }
                else             ->
                {
                    val msg = "Unhandled alliance value of '$alliance'. Cannot return proper command. Will return NoOpCommand"
                    logger.error(msg)
                    DriverStation.reportError(msg, false)
                    NoOpCommand
                }
            }
        }
    

    override fun toString(): String
    {
        return ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
            .append("description", description)
            .toString()
    }

    override fun equals(other: Any?): Boolean
    {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val that = other as AllianceAwareAutonomousCommandConfiguration
        return EqualsBuilder()
            .append(blueCommand, that.blueCommand)
            .append(redCommand, that.redCommand)
            .append(description, that.description)
            .isEquals
    }

    override fun hashCode(): Int
    {
        return HashCodeBuilder(17, 37)
            .append(blueCommand)
            .append(redCommand)
            .append(description)
            .toHashCode()
    }

    companion object
    {
        private val logger = LoggerFactory.getLogger(AllianceAwareAutonomousCommandConfiguration::class.java)
    }

}