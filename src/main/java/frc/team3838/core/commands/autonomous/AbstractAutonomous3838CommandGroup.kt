package frc.team3838.core.commands.autonomous

import edu.wpi.first.wpilibj.command.Command
import frc.team3838.core.commands.Abstract3838CommandGroup
import frc.team3838.core.commands.NO_TIMEOUT
import frc.team3838.core.commands.log.LogCommandGroupFinishedCommand
import frc.team3838.core.commands.log.LogCommandGroupStartCommand
import frc.team3838.core.config.time.TimeDuration
import frc.team3838.core.meta.API

// TODO create a builder / DSL to create the List<CommandGroupEntry>, and get rid of the addCommands

/**
 * A command group that automatically logs a start and end message to the list of provided commands.
 * Read the Javadocs of the [addSequential] and [addParallel] methods for information on the usage,
 * especially on use of parallel commands as it is somewhat counter-intuitive.
 * 
 * To add commands sequentially, just add the one after another as [AddAs.Sequential]. For example:
 * 
 * ```
 * new Command1()  AddAs.Sequential
 * new Command3()  AddAs.Sequential
 * new Command3()  AddAs.Sequential
 * ```
 * 
 * To tun in commands in parallel, add the first command as paralle, and the next command as sequential:
 * ```
 * new Command1()  AddAs.Parallel
 * new Command3()  AddAs.Sequential
 * ```
 * Command1 and Command2 will run in parallel.
 * 
 * A command group will require all of the subsystems that each member would require.
 * e.g. if Command1 requires chassis, and Command2 requires arm,
 * a CommandGroup containing them would require both the chassis and the arm.
 * The `addSequential` and `addParallel` methods will automatically add the
 * requires subsystems.
 * 
 * Java example:
 * ```
 * public AutoModeJavaExampleCommand()
 * {
 *     super(
 *         List.of(
 *         new CommandGroupEntry(new LogStatementCommand("Command 1")),
 *         new CommandGroupEntry(new LogStatementCommand("Command 2"), AddAs.Sequential),
 *         new CommandGroupEntry(new LogStatementCommand("Command 3"))
 *     ));
 * }
 * ```
 * 
 * 
 * @param name The name of the command. Defaults to the simple class name if not set or if set to `null`.
 * @param timeoutDuration When the command should timeout. Default is no timeout. NOTE: setting this property after the command has started will
 * not change the timeout for the current run. The setting is read (and used) during command initialization.
 * @param sequentialCommands List of commands to be added, and how they should be added.
 */ 
@API
abstract class AbstractAutonomous3838CommandGroup @JvmOverloads constructor(
        sequentialCommands: List<CommandGroupEntry> = emptyList(),
        name: String? = null,
        timeoutDuration: TimeDuration = NO_TIMEOUT) : Abstract3838CommandGroup(name, timeoutDuration)
{
    @API protected val logStartCommand: LogCommandGroupStartCommand by lazy { LogCommandGroupStartCommand(this) }
    @API protected val logEndCommand: LogCommandGroupFinishedCommand by lazy { LogCommandGroupFinishedCommand(this, logStartCommand) }

    @API
    constructor(name: String? = null, timeoutDuration: TimeDuration = NO_TIMEOUT, vararg commandGroupEntries: CommandGroupEntry):
            this(commandGroupEntries.asList(), name, timeoutDuration)
    
    @API
    constructor(timeoutDuration: TimeDuration = NO_TIMEOUT, vararg commandGroupEntries: CommandGroupEntry):
            this(commandGroupEntries.asList(), timeoutDuration = timeoutDuration)
    @API
    constructor(name: String? = null, vararg commandGroupEntries: CommandGroupEntry): this(commandGroupEntries.asList(), name = name)

    @API
    constructor(vararg commandGroupEntries: CommandGroupEntry) : this(commandGroupEntries.asList())
    
    init
    {
        preCommands()
        sequentialCommands.forEach { 
            if (it.addAs == AddAs.Sequential)
            {
                if (it.timeoutDuration != null)
                {
                    addSequential(it.command, timeoutDuration.toFractionalSeconds())
                }
                else
                {
                    addSequential(it.command)
                }
            }
            else if (it.addAs == AddAs.Parallel)
            {
                if (it.timeoutDuration != null)
                {
                    addParallel(it.command, timeoutDuration.toFractionalSeconds())
                }
                else
                {
                    addParallel(it.command)
                }
            }
        }
        postCommands()
    }

    private fun preCommands()
    {
        addSequential(logStartCommand)
        addSequential(AutonomousDisableMotorSafetyCommand())
    }

    private fun postCommands()
    {
        addSequential(logEndCommand)
        addSequential(AutonomousEnableMotorSafetyCommand())
    }
}

class CommandGroupEntry @JvmOverloads constructor(val command: Command, val addAs: AddAs = AddAs.Sequential, val timeoutDuration: TimeDuration? = null)

enum class AddAs {Sequential, Parallel}

