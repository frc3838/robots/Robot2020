package frc.team3838.core.function

import frc.team3838.core.logging.LaconicLeveledLogger
import frc.team3838.core.meta.API
import frc.team3838.core.utils.MathUtils
import java.util.function.DoubleConsumer

@API
class LaconicLoggingDoubleConsumer(private val laconicLogger: LaconicLeveledLogger, private val msgPrefix: String?) : DoubleConsumer
{
    override fun accept(value: Double)
    {
        laconicLogger.log{ "$msgPrefix: ${MathUtils.formatNumber(value, 3)}" }
    } 
    
    // We use the default andThen implementation from the interface
}