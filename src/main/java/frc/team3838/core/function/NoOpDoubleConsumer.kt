package frc.team3838.core.function

import frc.team3838.core.meta.API
import frc.team3838.core.utils.NO_OP
import java.util.function.DoubleConsumer

@API
object NoOpDoubleConsumer : DoubleConsumer
{
    override fun accept(value: Double) = NO_OP()
    
   // We use the default andThen implementation from the interface
}