package frc.team3838.core.hardware;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.event.Level;

import edu.wpi.first.wpilibj.SpeedController;
import frc.team3838.core.config.time.TimeDuration;
import frc.team3838.core.logging.LaconicLeveledLogger;
import frc.team3838.core.meta.API;



@API
public class NoOpLoggingSpeedController implements SpeedController
{
    private static final Logger logger = LoggerFactory.getLogger(NoOpLoggingSpeedController.class);
    
    private final LaconicLeveledLogger getLaconicLogger = new LaconicLeveledLogger(logger, TimeDuration.ofSeconds(10), Level.INFO);
    private final LaconicLeveledLogger setLaconicLogger = new LaconicLeveledLogger(logger, TimeDuration.ofSeconds(3), Level.INFO);
    private double theSetSpeed = 0;
    //private double lastGetSpeed = 0;
    
    private boolean inverted = false;
    
    private final String name;
    
    
    public NoOpLoggingSpeedController()
    {
        this("Simulated Speed Controller");
    }
    
    
    public NoOpLoggingSpeedController(String name)
    {
        this.name = name;
    }
    
    
    @Override
    public double get()
    {
        //lastGetSpeed = theSetSpeed;
        getLaconicLogger.info(String.format(" %s: simulated speed is %4.2f", name, theSetSpeed));
        return 0;
    }
    
    
    @Override
    public void set(double speed)
    {
        if (speed != theSetSpeed)
        {
            setLaconicLogger.info(String.format("%s : changing speed to %4.2f from %4.2f. isInverted = %s",
                                                name, speed, theSetSpeed, inverted));
        }
        theSetSpeed = speed;
    }
    
    
    @Override
    public void setInverted(boolean isInverted) { this.inverted = isInverted; }
    
    
    @Override
    public boolean getInverted() { return inverted; }
    
    
    @Override
    public void disable() { }
    
    
    @Override
    public void stopMotor() { set(0); }
    
    
    @Override
    public void pidWrite(double output) { set(output); }
}
