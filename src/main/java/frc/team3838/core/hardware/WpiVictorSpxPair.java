package frc.team3838.core.hardware;

import javax.annotation.Nonnull;

import com.ctre.phoenix.motorcontrol.InvertType;
import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX;

import edu.wpi.first.wpilibj.SpeedController;
import frc.team3838.core.meta.API;



@API
public class WpiVictorSpxPair extends AbstractCanBusMotorPair<WPI_VictorSPX> implements SpeedController
{
    public WpiVictorSpxPair(int masterDeviceNumber,
                            int followerDeviceNumber,
                            boolean invertMaster,
                            @Nonnull InvertType followerInvertType)
    {
        super(masterDeviceNumber, followerDeviceNumber, invertMaster, followerInvertType);
    }
    
    
    @Override
    @Nonnull
    protected WPI_VictorSPX createCanController(int deviceNumber)
    {
        return new WPI_VictorSPX(deviceNumber);
    }
}
