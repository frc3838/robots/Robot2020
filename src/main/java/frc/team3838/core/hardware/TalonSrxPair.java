package frc.team3838.core.hardware;

import javax.annotation.Nonnull;

import com.ctre.phoenix.motorcontrol.InvertType;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;

import edu.wpi.first.wpilibj.SpeedController;
import frc.team3838.core.meta.API;



/**
 * See, and generally use, the {@link WpiTalonSrxPair} rather than this class.
 */
@API
public class TalonSrxPair extends AbstractCanBusMotorPair<TalonSRX> implements SpeedController
{
    public TalonSrxPair(int masterDeviceNumber,
                        int followerDeviceNumber,
                        boolean invertMaster,
                        @Nonnull InvertType followerInvertType)
    {
        super(masterDeviceNumber, followerDeviceNumber, invertMaster, followerInvertType);
    }
    
    @Nonnull
    @Override
    protected TalonSRX createCanController(int deviceNumber)
    {
        return new TalonSRX(deviceNumber);
    }
}
