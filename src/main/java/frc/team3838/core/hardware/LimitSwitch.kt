package frc.team3838.core.hardware

import edu.wpi.first.wpilibj.DigitalInput
import frc.team3838.core.meta.API

/**
 * Class to provide Limit Switch operations with meaningful method names.
 * Use the factory methods to create. Use the [.isActivated] to detect trigger.
 * Example Uses:
 * ```
 * LimitSwitch bottomLimitSwitch = LimitSwitch.createNormal(DIOs.ELEVATOR_BOTTOM_LIMIT_SWITCH);
 * LimitSwitch topLimitSwitch = LimitSwitch.createNormal(DIOs.ELEVATOR_TOP_LIMIT_SWITCH);
 *
 * LimitSwitch climberLimitSwitch = LimitSwitch.createInverted(DIOs.CLIMBER_LIMIT_SWITCH);
 * if (climberLimitSwitch.isActivated())
 * {
 *     stopClimberMotor();
 * }
 *```
 */
@API
class LimitSwitch
{
    private val digitalInput: DigitalInput?
    private val inverse: Boolean
    private val dummyValue: Boolean?

    private constructor(dio: Int, inverse: Boolean)
    {
        require(dio >= 0) { "dio specified is invalid as it is less than zero. It was: $dio" }
        digitalInput = DigitalInput(dio)
        this.inverse = inverse
        dummyValue = null
    }

    private constructor(digitalInput: DigitalInput, inverse: Boolean)
    {
        this.digitalInput = digitalInput
        this.inverse = inverse
        dummyValue = null
    }

    private constructor(dummyValue: Boolean)
    {
        digitalInput = null
        this.dummyValue = dummyValue
        inverse = false
    }

    @get:API
    val isActivated: Boolean
        get() = dummyValue ?: (inverse != digitalInput!!.get())

    @get:API
    val isNotActivated: Boolean
        get() = !isActivated

    companion object
    {
        @API
        fun createNormal(dio: Int): LimitSwitch = LimitSwitch(dio, false)

        @API
        fun createInverted(dio: Int): LimitSwitch = LimitSwitch(dio, true)

        @API
        fun create(dio: Int, inverse: Boolean): LimitSwitch = LimitSwitch(dio, inverse)

        @API
        fun createNormal(digitalInput: DigitalInput): LimitSwitch = LimitSwitch(digitalInput, false)

        @API
        fun createInverted(digitalInput: DigitalInput): LimitSwitch = LimitSwitch(digitalInput, true)

        @API
        fun create(digitalInput: DigitalInput, inverse: Boolean): LimitSwitch = LimitSwitch(digitalInput, inverse)
        
        @API
        @JvmOverloads
        fun createNoOp(dummyValue: Boolean = false): LimitSwitch = LimitSwitch(dummyValue)
    }
}