package frc.team3838.core.utils

import java.text.DecimalFormat


val DECIMAL_FORMATTER_0_PLACES = DecimalFormat("#,##0")
val DECIMAL_FORMATTER_1_PLACES = DecimalFormat("#,##0.0")
val DECIMAL_FORMATTER_2_PLACES = DecimalFormat("#,##0.00")
val DECIMAL_FORMATTER_3_PLACES = DecimalFormat("#,##0.000")
val DECIMAL_FORMATTER_4_PLACES = DecimalFormat("#,##0.0000")
val DECIMAL_FORMATTER_5_PLACES = DecimalFormat("#,##0.00000")
val DECIMAL_FORMATTER_6_PLACES = DecimalFormat("#,##0.000000")

@JvmOverloads
fun Number.format(places: Int = 2): String = selectDecimalFormatter(places).format(this)

@JvmOverloads
fun selectDecimalFormatter(places: Int = 2): DecimalFormat
{
    return when (places)
    {
        0    -> DECIMAL_FORMATTER_0_PLACES
        1    -> DECIMAL_FORMATTER_1_PLACES
        2    -> DECIMAL_FORMATTER_2_PLACES
        3    -> DECIMAL_FORMATTER_3_PLACES
        4    -> DECIMAL_FORMATTER_4_PLACES
        5    -> DECIMAL_FORMATTER_5_PLACES
        else -> DECIMAL_FORMATTER_6_PLACES
    }
}