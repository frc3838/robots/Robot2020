
package frc.team3838.core

import edu.wpi.first.wpilibj.DriverStation
import edu.wpi.first.wpilibj.RobotState
import frc.team3838.core.meta.API

// Also see the WPI Lib edu.wpi.first.wpilibj.RobotState
// although we mimic most functions here

/**
 * Returns `true` if the robot is under in autonomous control,
 * regardless if in Match play, Practice mode, or "Free" mode 
 * (i.e. direct Autonomous mode on the DriverStation).
 */
@API
fun isAutonomous(): Boolean = RobotState.isAutonomous()

/**
 * Returns `true` if the robot is under operator control, i.e. teleop,
 * regardless if in Match play, Practice mode, or "Free" mode (i.e.
 * direct "TeleOperated" mode on the DriverStation).
 */
@API
fun isTeleop(): Boolean = RobotState.isOperatorControl()

/**
 * Returns `true` if the robot is in "Test" on the Driver Station.
 */
@API
fun isTestMode():Boolean = RobotState.isTest()

/**
 * Returns the game time, in seconds including fractions of a second. It returns the time
 * remaining (and thus counts down). However, this returns the time remaining in the 
 * current Mode. So for Autonomous Mode, it counts down from 15 seconds, not the 
 * full match time. Then when Teleop starts, it starts counting down from 145 seconds.
 * When not in a competition (or practice mode) this will return -1.
 */
@API
fun getTimeRemaining(): Double = DriverStation.getInstance().matchTime


/** Returns whether or not game play is in the end game **during an actual competition or when in practice mode**. 
 * If merely in TeleOp "Free Mode", this would return false. */
@API
@JvmOverloads
// Note that getMatchTime() will return < 30 when in autonomous mode since that mode is only a 10 or 15 seconds long.
fun isInEndGame(endGameRemainingTimeStart: Int = 30) = isDuringCompOrPracticeMode() && isTeleop() && getTimeRemaining() <= endGameRemainingTimeStart


/** Returns whether or not game play is before the end game **during an actual competition or when in practice mode**. 
 * If merely in TeleOp "Free Mode", this would return false. */
@API
@JvmOverloads
fun isBeforeEndGame(endGameRemainingTimeStart: Int = 30) = isDuringCompOrPracticeMode() && getTimeRemaining() > endGameRemainingTimeStart


@API
fun isTeleopDuringCompOrPractice(): Boolean = isDuringCompOrPracticeMode() && DriverStation.getInstance().isOperatorControl


@API
fun isDuringCompOrPracticeMode(): Boolean = getTimeRemaining() != -1.0


@API
fun isNotDuringCompOrPracticeMode(): Boolean = getTimeRemaining() == -1.0

fun getAllStatusesString() = 
        """
  isTeleop() =        ${isTeleop()}
  isAutonomous() =    ${isAutonomous()}
  getMatchTime() =    ${getTimeRemaining()}
  isBeforeEndGame() = ${isBeforeEndGame()}
  isInEndGame() =     ${isInEndGame()}
  isTeleopDuringCompOrPractice() =  ${isTeleopDuringCompOrPractice()}
  isDuringCompOrPracticeMode() =    ${isDuringCompOrPracticeMode()}
  isNotDuringCompOrPracticeMode() = ${isNotDuringCompOrPracticeMode()}
  """.trimMargin()